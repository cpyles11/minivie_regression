%% Training Data Paths
% File Paths
pxiDir = 'C:\Users\pylesco1\Desktop\RegressionTrainingData';
%pxiDir = 'U:\Wearables\Baselayer\Data\2019_05_17 - Regression testing';
%pxiFilenames    = {'20190412_TrainingData_1 - 04-12-19--08-17-12.tdms';
    %'20190412_TrainingData_2 - 04-12-19--08-18-41.tdms'};
% pxiFilenames    = {'04262019_TrainingSet1.tdms'};
pxiFilenames    = {'training1.tdms'};

emgDir = 'C:\Users\pylesco1\Desktop\RegressionTrainingData';
%emgDir = 'U:\Wearables\Baselayer\Data\2019_05_17 - Regression testing';
% Note that there may be more than one EMG filename per trial, in which
% case included as a cell array within a cell array
%emgFilenames    = {{'190412_TrainingData_1_190412_133251.rhd'} ;
    %{'190412_TrainingData_2_190412_133401.rhd'}};
% emgFilenames    = {{'04262019_TrainingSet1.rhd'}};
emgFilenames = {{'training1.rhd'}};


%% Step 0: Setup VIE software
MiniVIE.configurePath();

%% Step 1: Setup Input Device
emgSignalSource = Inputs.IntanInput.getInstance;
%emgSignalSource = Inputs.SignalSimulator;
encoderSignalSource = Inputs.LabviewUdp.getInstance;

%% Step 2: Add input filters
% NOTE: IF THESE ARE CHANGED ALSO NEED TO CHANGE FILTER SETTINGS IN
% Utilities/feature_extract_slide.m
% TODO: Align this so they use the same filters
lowPassFilter = Inputs.LowPass();
highPassFilter = Inputs.HighPass();
emgSignalSource.addfilter(lowPassFilter);  % set filter with custom parameters
emgSignalSource.addfilter(highPassFilter);

%Set the number of samples to return when getting data:
emgSignalSource.Verbose = 0;
emgSignalSource.NumSamples = 2000;
encoderSignalSource.NumSamples = 1;

%% Step 3: Setup Classifier, Select Channels in use
if ~exist('SignalClassifier','var')
    SignalClassifier = SignalAnalysis.LdaAndRegression();
end
SignalClassifier.UseEncoder = 0;
SignalClassifier.ActiveChannels = [1,4,6];  % <-- Update active channels, should be same ones used in training data, and offset of 1 compare to RHD which is 0 indexed
SignalClassifier.NumMajorityVotes = 5;
SignalClassifier.FeatureList = {'mav', 'len', 'zc', 'ssc'};
SignalClassifier.WindowTimeSize = 0.2; % 200 ms

%% Step 5: Train the classifier
if isempty(SignalClassifier.regressors)
    SignalClassifier.train(pxiDir, pxiFilenames, emgDir, emgFilenames);
end
    
%% Step 6: Send data to MplUnity for visualization
h = MPL.MplUnity;
if ~emgSignalSource.IsInitialized
    emgSignalSource.initialize();
end
emgSignalSource.start();

% hSignalViewer = GUIs.guiSignalViewer(emgSignalSource);
% hSignalViewer.SelectedChannels = SignalClassifier.ActiveChannels;
% hSignalViewer.ShowFilteredData = 1;
% hSignalViewer.NumTimeDomainSamples = 2000;
% hSignalViewer.NumFeatureSamples = 250;
% hSignalViewer.NumFrequencySamples = 400;
% hSignalViewer.FeatureWindowSize = 1000*SignalClassifier.WindowTimeSize;
% hSignalViewer.ZcThreshold = SignalClassifier.ZcThresh;
% hSignalViewer.SscThreshold = SignalClassifier.SscThresh;

if SignalClassifier.UseEncoder == 1
    encoderSignalSource.initialize();
    encoderSignalSource.start();
end

h.PlotBin = 1;
h.initialize(emgSignalSource, encoderSignalSource, SignalClassifier);
h.update();
h.Verbose = 0;
h.start();

