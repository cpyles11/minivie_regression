classdef LabviewUdp < Inputs.SignalInput
    % Class for interfacing Labview via pnet.
    % This class is designed to allow Labview data to be streamed into the
    % Matlab GUI. 
    % Adam Polevoy 2019
    
    properties 
        UdpPortNum;
    end
    properties (SetAccess = private) 
        IsInitialized = 0;
        UdpSocket;
        Buffer;
        numPacketsReceived = 0;
        numValidPackets = 0;
    end
    methods (Access = private) 
        function obj = LabviewUdp 
            obj.Verbose = 0;
        end
    end
    methods
        function [ status ] = initialize(obj) 
            % Initialize network interface to NFU
            % [ status ] = initialize(obj)
            %
            % status = 0: no error
            % status < 0: Failed
            
            obj.ChannelIds = 1;
            
            status = 0;
            
            if obj.IsInitialized
                fprintf('[%s] UDP Comms already initialized\n',mfilename);
                return
            end

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%     
            % Open a udp port to receive streaming data on
            % This number must match the LabView!!!   
            % 9764 is the remote port number in the labview code 
            obj.UdpPortNum = 9764;
            obj.UdpSocket = PnetClass(obj.UdpPortNum);
            if ~obj.UdpSocket.initialize()
                fprintf(2,'[%s] Failed to initialize udp socket\n',mfilename);
                status = -1;
                return
            elseif (obj.UdpSocket ~= 0)
                fprintf(2,'[%s] Expected receive socket id == 0, got socket id == %d\n',mfilename,obj.UdpSocket.hSocket);
            end
            
            % check for data:
            [~, numReads] = obj.UdpSocket.getAllData(1e6);  %getAllData is in PnetClass, it returns all packets as a cell array
            if numReads > 0
                fprintf('[%s] UDP Data Stream Detected\n', mfilename);
            else
                fprintf('[%s] UDP Data Stream NOT Detected\n', mfilename);
            end
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
            % data is [numSamples x numChannels]
            obj.Buffer = Common.DataBuffer(5000, obj.NumChannels);
            
            obj.IsInitialized = true;
            
        end
        function update(obj) 
            
            maxRead = 1e6;
            
            %getAllData is in PnetClass, it returns all packets as a cell array
            [cellDataBytes, numReads] = obj.UdpSocket.getAllData(maxRead);
            if numReads > 0
                % convert data bytes
                [time_code, joint_angles, nValidPackets] = obj.convertPackets(cellDataBytes);
                if nValidPackets == 0
                    disp('Invalid Packets from Labview');
                    return
                end
                
                % Display Output
                if obj.Verbose > 0
                    fprintf('%d\t%d\n', time_code(1,1), joint_angles);
                end

                %%%Here we are adding data to the buffer so that it can be
                %%%plotted
                % Adam test
                obj.Buffer.addData(joint_angles, 1);
            end
        end %bottom of update
        
        %%% Called by GUI app
        function data = getData(obj,numSamples,idxChannel)
            % data = getData(obj,numSamples,idxChannel)
            % get data from buffer.  most recent sample will be at (end)
            % position.
            % dataBuffer = [NumSamples by NumChannels];
            %
            % optional arguments:
            %   numSamples, the number of samples requested from getData
            %   idxChannel, an index into the desired channels.  E.g. get the
            %   first four channels with iChannel = 1:4
            
            if nargin < 2
                numSamples = obj.NumSamples;
            end
            
            if nargin < 3
                idxChannel = 1:obj.NumChannels;
            end
            
            obj.update();

            %%% data is what will be plotted
            %%% it will have one of three numbers, which of the three is
            %%% determined by idxChannel
            data = obj.Buffer.getData(numSamples, idxChannel);
        end %bottom of getData
        
        function isReady = isReady(obj, numSamples) 
            isReady = 1;
        end
        function start(obj) 
        end
        function stop(obj) 
        end
        function close(obj) 
            Inputs.XsensUdp.getInstance(-1);
        end
    end
    methods (Static)
        function [time_code, joint_angles, nValidPackets] = ...
            convertPackets(cellDataBytes) %cellDataBytes is what is returned from getAllData, cell array of all data packets 
            % Read buffered udp packets and return results
            
            % default outputs
            %%%Deal just initializes the variables to zero
            [time_code, joint_angles, nValidPackets] = deal([]);
            
            %This is for Labview message size (6 width, 3 precision) 
            jointAnglePacketSize = 6;  
            
            %%%Check which messages have the expected size, ie are the data
            %%%we are looking for
            isJointAngleSize = cellfun(@length, cellDataBytes) == jointAnglePacketSize;
            
            % compute number of valid packets
            nValidPackets = sum(isJointAngleSize);
            
            if nValidPackets == 0
                % no new data, nothing to do
                return
            end
            
            % convert 2d array: [newPackets by num Elements]
            jointAngleOrderedBytes = reshape([cellDataBytes{isJointAngleSize}],jointAnglePacketSize,[]);
            
            % converts the binary from the udp data packets into data we 
            % want
            [time_code, joint_angles, nValidPackets] = ...
                convertJointAngles(jointAngleOrderedBytes);
        end  %bottom of convertPackets
        
        function singleObj = getInstance(cmd) %done
            persistent localObj
            if nargin < 1
                cmd = 0;
            end
            
            if cmd < 0
                fprintf('[%s] Deleting Udp comms object\n',mfilename);
                try
                    localObj.UdpSocket.close();
                catch e
                    disp(e)
                end
                %IsInitialized
                localObj = [];
                return
            end
            
            if isempty(localObj) || ~isvalid(localObj)
                fprintf('[%s] Calling constructor\n',mfilename);
                localObj = Inputs.LabviewUdp;
            else
                fprintf('[%s] Returning existing object\n',mfilename);
            end
            singleObj = localObj;
        end
    end
end

function [time_code, joint_angles, nValidPackets] = ...    
convertJointAngles(orderedBytes)% this is called by convertPackets, orderedBytes is from jointAngleOrderedBytes, each row being a packet and the columns being the bytes of that packet (cell array)  
    %%%time_code: time that sample was taken, this is provided by IMU
    %%%
    %%%joint_angles: 22 by 5 matrix, this is the 22 joints in the body
    %%%Xrotation, yrotation, Z rotation, parent ID, child ID
    %%%This is from the IMU
    %%%
    %%%nValidPackets: how many ?UDP? messages (ie packets) were received
    %%%    

    time_code = 0;%initialize variable   
   
    id_bytes = orderedBytes(1:6,:)';
    joint_angles = str2double(native2unicode(id_bytes(end,:)));%fill up every entry in the output matrix with the value that was contained inside the most recent data packet
    if obj.Verbose > 0
        fprintf('joint_angles value: %6.3f \n', joint_angles); %display output for monitoring debugging
    end

    nValidPackets = size(orderedBytes,2);
end  %bottom of convertJointAngles  