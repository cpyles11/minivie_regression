classdef XsensUdp < Inputs.SignalInput
    % Class for interfacing Xsens MVN Studio via pnet.
    % This class is designed to allow Xsens data to be streamed into the
    % MiniVIE codebase.  It can be tested through the testXsensUdp script.
    %
    %   Segment IDs:
    %   Pelvis(1), L5(2), L3(3), T12(4), T8(5), Neck(6), Head(7),
    %   R. Shoulder(8), R. Upper Arm(9), R. Forearm(10), R. Hand(11),
    %   L. Shoulder(12), L. Upper Arm(13), L. Forearm(14), L. Hand(15),
    %   R. Upper Leg(16), R. Lower Leg(17), R. Foot(18), R. Toe(19),
    %   L. Upper Leg(20), L. Lower Leg(21), L. Foot(22), L. Toe(23)
    %
    %   Information about data output: IndexofDataOutput(Information)
    %       Joint angles: (segments angle is inbetween)
    %       The three indices include x, y, and z axis angles, respectively
    %       1:3(1/2), 4:6(2/3), 7:9(3/4), 10:12(4/5), 13:15(5/6), 16:18(6/7),
    %       19:21(7/8), 22:24(8/9), 25:27(9/10), 28:30(10/11), 31:33(5/12), 
    %       34:36(12/13), 37:39(13/14), 40:42(14/15), 43:45(1/16), 
    %       46:48(16/17), 49:51(17/18), 52:54(18/19), 55:57(1/20),
    %       58:60(20/21), 61:63(21/22), 64:66(22/23)
    %
    %       Segment Orientation- Euler: (segment)
    %       The six indices include x-coord, y-coord, z-coord,
    %       x-rotation, y-rotation, z-rotation, respectively
    %       67:72(1), 73:78(2), 79:84(3), 85:90(4), 91:96(5),
    %       97:102(6), 103:108(7), 109:114(8), 115:120(9), 121:126(10),
    %       127:132(11), 133:138(12), 139:144(13), 145:150(14), 151:156(15),
    %       157:162(16), 163:168(17), 169:174(18), 175:180(19), 181:186(20),
    %       187:192(21), 193:198(22), 199:204(23)
    %
    %       Center of Mass:
    %       205(x-coord), 206(y-coord), 207(z-coord)
    %
    %   NOTE: Currently, it is reduced to only 3 channels for the elbow
    %   joint angles.
    %
    % Adam Polevoy 2018
    
    properties 
        UdpPortNum;
    end
    properties (SetAccess = private) 
        IsInitialized = 0;
        UdpSocket;
        Buffer;
        numPacketsReceived = 0;
        numValidPackets = 0;
    end
    methods (Access = private) 
        function obj = XsensUdp 
            obj.Verbose = 0;
        end
    end
    methods
        function [ status ] = initialize(obj) 
            % Initialize network interface to NFU.
            % [ status ] = initialize(obj)
            %
            % status = 0: no error
            % status < 0: Failed
            
            % Note: may have to use interpolation to upsample
            %obj.ChannelIds = 1:207;
            obj.ChannelIds = 1:3;
            
            status = 0;
            
            if obj.IsInitialized
                fprintf('[%s] UDP Comms already initialized\n',mfilename);
                return
            end
            
            % Open a udp port to receive streaming data on
            obj.UdpPortNum = 9764;
            obj.UdpSocket = PnetClass(obj.UdpPortNum);
            if ~obj.UdpSocket.initialize()
                fprintf(2,'[%s] Failed to initialize udp socket\n',mfilename);
                status = -1;
                return
            elseif (obj.UdpSocket ~= 0)
                fprintf(2,'[%s] Expected receive socket id == 0, got socket id == %d\n',mfilename,obj.UdpSocket.hSocket);
            end
            
            % check for data:
            [~, numReads] = obj.UdpSocket.getAllData(1e6);
            if numReads > 0
                fprintf('[%s] UDP Data Stream Detected\n', mfilename);
            else
                fprintf('[%s] UDP Data Stream NOT Detected\n', mfilename);
            end
            
            % data is [numSamples x numChannels]
            obj.Buffer = Common.DataBuffer(5000, obj.NumChannels);
            
            obj.IsInitialized = true;
            
        end
        function update(obj) 
            
            maxRead = 1e6;
            
            [cellDataBytes, numReads] = obj.UdpSocket.getAllData(maxRead);
            if numReads > 0
                % convert data bytes
                [time_code, joint_angles, euler_pose, center_mass, nValidPackets] = obj.convertPackets(cellDataBytes);
                if nValidPackets == 0
                    disp('Invalid Packets for Xsens');
                    return
                end
                
                % Display Output
                if obj.Verbose > 0
                    fprintf('%d\t%d\n', time_code(1,1), joint_angles(9,5,1));
                end
                
%                  % Add joint angle data
%                 for i = 1:22
%                     for j = 1:3
%                         obj.Buffer.addData(joint_angles(i,j+2), j+(i-1)*3);
%                     end
%                 end
%                 % Add euler position data
%                 for i = 1:23
%                     for j = 1:6
%                         obj.Buffer.addData(euler_pose(i,j+1), j+(i-1)*6+66);
%                     end
%                 end
%                 % Add center of mass data
%                 for j = 1:3
%                     obj.Buffer.addData(center_mass(1,j), j+204);
%                 end
                obj.Buffer.addData(joint_angles(13,1+2), 1);
                obj.Buffer.addData(joint_angles(13,2+2), 2);
                obj.Buffer.addData(joint_angles(13,3+2), 3);
            end
        end
        function data = getData(obj,numSamples,idxChannel) 
            %data = getData(obj,numSamples,idxChannel)
            % get data from buffer.  most recent sample will be at (end)
            % position.
            % dataBuffer = [NumSamples by NumChannels];
            %
            % optional arguments:
            %   numSamples, the number of samples requested from getData
            %   idxChannel, an index into the desired channels.  E.g. get the
            %   first four channels with iChannel = 1:4
            
            if nargin < 2
                numSamples = obj.NumSamples;
            end
            
            if nargin < 3
                idxChannel = 1:obj.NumChannels;
            end
            
            obj.update();

            data = obj.Buffer.getData(numSamples, idxChannel);
        end
        function isReady = isReady(obj, numSamples) 
            isReady = 1;
        end
        function start(obj) 
        end
        function stop(obj) 
        end
        function close(obj) 
            Inputs.XsensUdp.getInstance(-1);
        end
    end
    methods (Static)
        function [time_code, joint_angles, euler_pose, center_mass, nValidPackets] = ...
                convertPackets(cellDataBytes) %test
            % Read buffered udp packets and return results
            % https://xsens.com/download/usermanual/3DBM/MVN_real-time_network_streaming_protocol_specification.pdf?_ga=2.78062484.1684795999.1528115134-387213827.1527791415&_gac=1.53288666.1528133609.Cj0KCQjwxtPYBRD6ARIsAKs1XJ6Azs8yxzEzpCs3Z2qWqybNcJr7sHY8Ahw4tGDKC6Yr0BvsjBUZOA0aAp9dEALw_wcB
            % Reference Above was used 
            
            % default outputs
            [time_code, joint_angles, euler_pose, center_mass, nValidPackets] = deal([]);
            
            jointAnglePacketSize = 464;
            isJointAngleSize = cellfun(@length, cellDataBytes) == jointAnglePacketSize;
            
            eulerPosePacketSize = 668;
            isEulerPoseSize = cellfun(@length, cellDataBytes) == eulerPosePacketSize;       

            quatPosePacketSize = 760;
            isQuatPoseSize = cellfun(@length, cellDataBytes) == quatPosePacketSize;
            
            centerMassPacketSize = 36;
            isCenterMassSize = cellfun(@length, cellDataBytes) == centerMassPacketSize;
            
            % compute number of valid packets
            nValidPackets = sum(isJointAngleSize);
            
            if nValidPackets == 0
                % no new data, nothing to do
                return
            end
            
            % convert 2d array: [newPackets by num Elements]
            jointAngleOrderedBytes = reshape([cellDataBytes{isJointAngleSize}],jointAnglePacketSize,[]);
            eulerPoseOrderedBytes = reshape([cellDataBytes{isEulerPoseSize}],eulerPosePacketSize,[]);
            quatPoseOrderedBytes = reshape([cellDataBytes{isQuatPoseSize}],quatPosePacketSize,[]);
            centerMassOrderedBytes = reshape([cellDataBytes{isCenterMassSize}],centerMassPacketSize,[]);
            
            % converts the binary from the udp data packets into data we 
            % want
            [time_code, joint_angles, nValidPackets] = ...
                convertJointAngles(jointAngleOrderedBytes);
            [time_code2, euler_pose, nValidPackets2] = ...
                convertEulerPose(eulerPoseOrderedBytes);
            [time_code3, quat_pose, nValidPackets3] = ...
                convertQuatPose(quatPoseOrderedBytes);
            [time_code4, center_mass, nValidPackets4] = ...
                convertCenterMass(centerMassOrderedBytes);
        end
        function singleObj = getInstance(cmd) %done
            persistent localObj
            if nargin < 1
                cmd = 0;
            end
            
            if cmd < 0
                fprintf('[%s] Deleting Udp comms object\n',mfilename);
%                 try
%                     localObj.UdpStreamReceiveSocket.close();
%                 catch e
%                     disp(e)
%                 end
                try
                    localObj.UdpSocket.close();
                catch e
                    disp(e)
                end
                %IsInitialized
                localObj = [];
                return
            end
            
            if isempty(localObj) || ~isvalid(localObj)
                fprintf('[%s] Calling constructor\n',mfilename);
                localObj = Inputs.XsensUdp;
            else
                fprintf('[%s] Returning existing object\n',mfilename);
            end
            singleObj = localObj;
        end
    end
end

function [time_code, joint_angles, nValidPackets] = ...
    convertJointAngles(orderedBytes)
    nValidPackets = size(orderedBytes,2);

    id_bytes = orderedBytes(1:6,:);
    id = char(id_bytes);

    sample_count_bytes = flipud(orderedBytes(7:10,:));
    sample_count = typecast(sample_count_bytes(:), 'uint32');

    datagram_count = orderedBytes(11,:);

    num_items = orderedBytes(12,:);

    time_code_bytes = flipud(orderedBytes(13:16,:));
    time_code = typecast(time_code_bytes(:), 'uint32');

    character_id = orderedBytes(17,:);

    joint_angles = zeros(22,5,nValidPackets);
    for i = 0:21
        parent_id_bytes = flipud(orderedBytes(25+i*20:28+i*20,:));
        joint_angles(i+1,1,:) = typecast(parent_id_bytes(:), 'int32')./256;

        child_id_bytes = flipud(orderedBytes(29+i*20:32+i*20,:));
        joint_angles(i+1,2,:) = typecast(child_id_bytes(:), 'int32')./256;

        x_rot_bytes = flipud(orderedBytes(33+i*20:36+i*20,:));
        joint_angles(i+1,3,:) = typecast(x_rot_bytes(:), 'single');

        y_rot_bytes = flipud(orderedBytes(37+i*20:40+i*20,:));
        joint_angles(i+1,4,:) = typecast(y_rot_bytes(:), 'single');

        z_rot_bytes = flipud(orderedBytes(41+i*20:44+i*20,:));
        joint_angles(i+1,5,:) = typecast(z_rot_bytes(:), 'single');
    end
end

%seems like there is a strange offset for position
function [time_code, euler_pose, nValidPackets] = ...
    convertEulerPose(orderedBytes)
    nValidPackets = size(orderedBytes,2);

    id_bytes = orderedBytes(1:6,:);
    id = char(id_bytes);

    sample_count_bytes = flipud(orderedBytes(7:10,:));
    sample_count = typecast(sample_count_bytes(:), 'uint32');

    datagram_count = orderedBytes(11,:);

    num_items = orderedBytes(12,:);

    time_code_bytes = flipud(orderedBytes(13:16,:));
    time_code = typecast(time_code_bytes(:), 'uint32');

    character_id = orderedBytes(17,:);
    
    euler_pose = zeros(23,7,nValidPackets);
    for i = 0:22
        seg_id_bytes = flipud(orderedBytes(25+i*28:28+i*28,:));
        euler_pose(i+1,1,:) = typecast(seg_id_bytes(:), 'int32');

        x_coord_bytes = flipud(orderedBytes(29+i*28:32+i*28,:));
        euler_pose(i+1,2,:) = typecast(x_coord_bytes(:), 'single');

        y_coord_bytes = flipud(orderedBytes(33+i*28:36+i*28,:));
        euler_pose(i+1,3,:) = typecast(y_coord_bytes(:), 'single');

        z_coord_bytes = flipud(orderedBytes(37+i*28:40+i*28,:));
        euler_pose(i+1,4,:) = typecast(z_coord_bytes(:), 'single');   

        x_rot_bytes = flipud(orderedBytes(41+i*28:44+i*28,:));
        euler_pose(i+1,5,:) = typecast(x_rot_bytes(:), 'single');
        
        y_rot_bytes = flipud(orderedBytes(45+i*28:48+i*28,:));
        euler_pose(i+1,6,:) = typecast(y_rot_bytes(:), 'single');
        
        z_rot_bytes = flipud(orderedBytes(49+i*28:52+i*28,:));
        euler_pose(i+1,7,:) = typecast(z_rot_bytes(:), 'single');
    end
end

%seems like there is a strange scaling for position.  This does not
%currently work
function [time_code, quat_pose, nValidPackets] = ...
    convertQuatPose(orderedBytes)
    nValidPackets = size(orderedBytes,2);

    id_bytes = orderedBytes(1:6,:);
    id = char(id_bytes);

    sample_count_bytes = flipud(orderedBytes(7:10,:));
    sample_count = typecast(sample_count_bytes(:), 'uint32');

    datagram_count = orderedBytes(11,:);

    num_items = orderedBytes(12,:);

    time_code_bytes = flipud(orderedBytes(13:16,:));
    time_code = typecast(time_code_bytes(:), 'uint32');

    character_id = orderedBytes(17,:);
    
    quat_pose = zeros(23,8,nValidPackets);
    for i = 0:22
        seg_id_bytes = flipud(orderedBytes(25+i*32:28+i*32,:));
        quat_pose(i+1,1,:) = typecast(seg_id_bytes(:), 'int32');

        x_coord_bytes = flipud(orderedBytes(29+i*32:32+i*32,:));
        quat_pose(i+1,2,:) = typecast(x_coord_bytes(:), 'single');

        y_coord_bytes = flipud(orderedBytes(33+i*32:36+i*32,:));
        quat_pose(i+1,3,:) = typecast(y_coord_bytes(:), 'single');

        z_coord_bytes = flipud(orderedBytes(37+i*32:40+i*32,:));
        quat_pose(i+1,4,:) = typecast(z_coord_bytes(:), 'single');

        q1_bytes = flipud(orderedBytes(41+i*32:44+i*32,:));
        quat_pose(i+1,5,:) = typecast(q1_bytes(:), 'single');
        
        q2_bytes = flipud(orderedBytes(45+i*32:48+i*32,:));
        quat_pose(i+1,6,:) = typecast(q2_bytes(:), 'single');
        
        q3_bytes = flipud(orderedBytes(49+i*32:52+i*32,:));
        quat_pose(i+1,7,:) = typecast(q3_bytes(:), 'single');
        
        q4_bytes = flipud(orderedBytes(53+i*32:56+i*32,:));
        quat_pose(i+1,8,:) = typecast(q4_bytes(:), 'single');
    end
end

%seems like there is a strange scaling for position
function [time_code, center_mass, nValidPackets] = ...
    convertCenterMass(orderedBytes)
    nValidPackets = size(orderedBytes,2);

    id_bytes = orderedBytes(1:6,:);
    id = char(id_bytes);

    sample_count_bytes = flipud(orderedBytes(7:10,:));
    sample_count = typecast(sample_count_bytes(:), 'uint32');

    datagram_count = orderedBytes(11,:);

    num_items = orderedBytes(12,:);

    time_code_bytes = flipud(orderedBytes(13:16,:));
    time_code = typecast(time_code_bytes(:), 'uint32');

    character_id = orderedBytes(17,:);
    
    center_mass = zeros(1,3, nValidPackets);
    x_coord_bytes = flipud(orderedBytes(25:28,:));
    center_mass(1,1,:) = typecast(x_coord_bytes(:), 'single');
    
    y_coord_bytes = flipud(orderedBytes(29:32,:));
    center_mass(1,2,:) = typecast(y_coord_bytes(:), 'single');
    
    z_coord_bytes = flipud(orderedBytes(33:36,:));
    center_mass(1,3,:) = typecast(z_coord_bytes(:), 'single');
end