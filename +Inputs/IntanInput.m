classdef IntanInput < Inputs.SignalInput
    %INTANINPUT Class that interfaces with intan evaluation system
    
    properties (SetAccess = private)
        IsInitialized = 0;
        driver;
        board;
        datablock;
        ChipIndex = 1;
        Buffer;
        num_datablocks = 1;
        numPacketsReceived = 0;
        numValidPackets = 0;
        chunk_size;
        hTimer;
    end
    methods (Access = private)
        function obj = IntanInput
            obj.Verbose = 0;
        end
    end
    methods
        function [ status ] = initialize(obj)
            % Initialize the board and driver
            % [ status ] = initialize(obj)
            %
            % status = 0: no error
            % status < 0; Fail
            
            try
                if not(libisloaded('RHD2000m'))
                    if strcmp(computer('arch'), 'win64')
                        dll = 'C:\Program Files\MATLAB\R2017b\toolbox\local\RHD2000 Matlab Toolbox\+rhd2000\RHD2000m64.dll';
                    else
                        dll = 'C:\Program Files\MATLAB\R2017b\toolbox\local\RHD2000 Matlab Toolbox\+rhd2000\RHD2000m32.dll';
                    end
                    loadlibrary(dll, 'C:\Program Files\MATLAB\R2017b\toolbox\local\RHD2000 Matlab Toolbox\+rhd2000\RHD2000Toolbox.h', 'alias', 'RHD2000m');
                end
            catch e
                disp e
            end
            
            obj.ChannelIds = 1:16;
            
            status = 0;
            
            if obj.IsInitialized
                fprintf('[%s] Intan Board already initialized\n',mfilename);
                return
            end
            
            % Instantiate the rhd2000 driver.  Using the RHD2000 Matlab
            % Toolbox almost always starts this way.
            obj.driver = rhd2000.Driver();
            
            % Connect to a board.  This uses the default board;  if you
            % have more than one, see the two_boards example
            obj.board = obj.driver.create_board();
            obj.board.SamplingRate = rhd2000.SamplingRate.rate1000;
            
            % data is [numSamples x numChannels]
            obj.Buffer = Common.DataBuffer(5000, obj.NumChannels);
            obj.IsInitialized = true;
        end
        function update(obj)
%             % if the board's buffer is filling up too quickly, sample an
%             % additional datablock each update
%             fifoPercentage = obj.board.FIFOPercentageFull;
% %             if fifoPercentage > 1
% %                 obj.num_datablocks = obj.num_datablocks + 1;
% %             elseif (fifoPercentage <= 1) && obj.num_datablocks > 1
% %                 obj.num_datablocks = obj.num_datablocks - 1;
% %             end
%             
%             % for each datablock recieved, pull data from amplifiers and
%             % put it into the buffer
%             for i = 1:obj.chunk_size
%                 %if obj.Verbose > 0
%                     fprintf('%g ms (%2.2f%% full)\n', obj.board.FIFOLag, fifoPercentage);
%                 %end
%                 obj.datablock.read_next(obj.board);
%                 newdata = obj.datablock.Chips{obj.ChipIndex}.Amplifiers(obj.ChannelIds,:)*1000;
%                 obj.Buffer.addData(newdata.');
%             end
        end
        function data = getData(obj, numSamples, idxChannel)
            % data = getData(obj,numSamples,idxChannel)
            % get data from buffer.  most recent sample will be at (end)
            % position.
            % dataBuffer = [NumSamples by NumChannels];
            %
            % optional arguments:
            %   numSamples, the number of samples requestede from get Data
            %   idxChannel, an index into the desired channels.  E.g. get
            %   the first four channels with iChannel = 1:4
            if nargin < 2
                numSamples = obj.NumSamples;
            end
            
            if nargin < 3
                idxChannel = 1:obj.NumChannels;
            end
            
            obj.update();
            
            data = obj.Buffer.getData(numSamples, idxChannel);
        end
        function isReady = isReady(obj, numSamples)
            isReady = 1;
        end
        function start(obj)
            % create a datablock object to reuse, and start running the
            % board
            
            % Set the chunk_size; this is used in the update function
            config_params = obj.board.get_configuration_parameters();
            obj.chunk_size = config_params.Driver.NumBlocksToRead;

            % Create a datablock for reuse
            obj.datablock = rhd2000.datablock.DataBlock(obj.board);
            obj.board.run_continuously();
            
            % Need to have this always extracting data in the background so
            % it doesn't fall behind
            % Create a timer - it's used when you click Run
%             obj.hTimer = UiTools.create_timer(mfilename,@(src,evt)continuous_update(obj));
%             obj.hTimer.Period = 0.001;
            obj.hTimer = timer(...
                'ExecutionMode', 'fixedSpacing', ...       % Run timer repeatedly
                'Period', 0.01, ...
                'TimerFcn', @(src,evt)continuous_update(obj)); % Specify callback
            start(obj.hTimer);
        end
        function stop(obj)
            % stop running the board
            obj.board.stop();
        end
        function close(obj)
            % close and reset the board
            Inputs.IntanInput.getInstance(-1);
        end
    end
    methods (Static)
        function [obj, hViewer] = test
            clear all
            obj = Inputs.IntanInput.getInstance;
            obj.initialize();
            obj.start();
            hviewer = GUIs.guiXsensSignal 
            obj.stop();
            obj.close();
        end
        function singleObj = getInstance(cmd)
            persistent localObj
            if nargin < 1
                cmd = 0;
            end
            if cmd < 0
                fprintf('[%s] Reseting Intan Board\n', mfilename);
                try
                    localObj.stop();
                end
                try
                    localObj.board.flush();
                end
                try
                    localObj.board.reset();
                end
                %IsInitialized
                localObj = [];
                try
                    if libisloaded('RHD2000m')
                        unloadlibrary('RHD2000m');
                    end
                catch e
                    disp(e)
                end
                
                return
            end
            
            if isempty(localObj) || ~isvalid(localObj)
                fprintf('[%s] Calling constructor\n',mfilename);
                localObj = Inputs.IntanInput
            else
                fprintf('[%s] Returning existing object\n',mfilename);
            end
            singleObj = localObj;
        end
    end
end

function continuous_update(hObj)
fifoPercentage = hObj.board.FIFOPercentageFull;
%             if fifoPercentage > 1
%                 obj.num_datablocks = obj.num_datablocks + 1;
%             elseif (fifoPercentage <= 1) && obj.num_datablocks > 1
%                 obj.num_datablocks = obj.num_datablocks - 1;
%             end
            
            % for each datablock recieved, pull data from amplifiers and
            % put it into the buffer
            for i = 1:hObj.chunk_size
                if hObj.Verbose > 0
                    fprintf('%g ms (%2.2f%% full)\n', hObj.board.FIFOLag, fifoPercentage);
                end
                hObj.datablock.read_next(hObj.board);
                newdata = hObj.datablock.Chips{hObj.ChipIndex}.Amplifiers(hObj.ChannelIds,:)*1000000;
                hObj.Buffer.addData(newdata.');
            end
end

