classdef IntanFileInput < Inputs.SignalInput
    % This class emulates IntanInput.  However, instead of collecting data
    % real time, a data file is used.  This class mimics real time
    % streaming using data from a file.
    % Adam Polevoy 2018
    
    properties (SetAccess = private)
        IsInitialized = 0;
        ChipIndex = 1;
        Buffer;
        data;
        time;
        fs;
        index;
        running;
        mTic;
    end
    methods (Access = private) 
        function obj = IntanFileInput 
            obj.Verbose = 0;
        end
    end
    methods
        function [ status ] = initialize(obj, filename)
            % Initialize the board and driver
            % [ status ] = initialize(obj)
            %
            % status = 0: no error
            % status < 0; Fail
            obj.ChannelIds = 1:16;
            
            status = 0;
            
            if obj.IsInitialized
                fprintf('[%s] Intan File already initialized\n',mfilename);
                obj.index = 1;
                return
            end
            
            % load in the data
            load(filename);
            obj.data = emg_data;
            obj.time = emg_time;
            obj.fs = 1 / (emg_time(1,2)-emg_time(1,1));
            obj.index = 1;
            obj.running = 0;
            
            % data is [numSamples x numChannels]
            obj.Buffer = Common.DataBuffer(5000, obj.NumChannels);
            obj.IsInitialized = true;
        end
        function update(obj)
            % protection
            if obj.running
                % How long has it been since this function was last called
                timer = toc(obj.mTic);
                obj.mTic = tic;
                
                try
                    % number of samples pulled from the data file is
                    % dependant on how long since this function was last
                    % called and the sampling rate
                    num_samples = round(timer*obj.fs);
                    end_index = obj.index + num_samples - 1;
                    newData = obj.data(:,obj.index:end_index);
                    obj.index = end_index + 1;
                catch
                    obj.stop();
                    newData  = zeros(16,1);
                end
            else
                newData = zeros(16,1);
            end
                
            obj.Buffer.addData(newData.');
        end
        function data = getData(obj, numSamples, idxChannel)
            % data = getData(obj,numSamples,idxChannel)
            % get data from buffer.  most recent sample will be at (end)
            % position.
            % dataBuffer = [NumSamples by NumChannels];
            %
            % optional arguments:
            %   numSamples, the number of samples requestede from get Data
            %   idxChannel, an index into the desired channels.  E.g. get
            %   the first four channels with iChannel = 1:4
            
            if nargin < 2
                numSamples = obj.NumSamples;
            end
            
            if nargin < 3
                idxChannel = 1:obj.NumChannels;
            end
            
            obj.update();
            
            data = obj.Buffer.getData(numSamples, idxChannel);
        end
        function isReady = isReady(obj, numSamples)
            isReady = 1;
        end
        function start(obj)
            obj.mTic = tic;
            obj.running = 1;
        end
        function stop(obj)
            toc;
            obj.running = 0;
        end
        function close(obj)
            % close and reset the board
            Inputs.IntanInput.getInstance(-1);
        end
    end
    methods (Static)
        function singleObj = getInstance(cmd)
            persistent localObj
            if nargin < 1
                cmd = 0;
            end
            if cmd < 0
                fprintf('[%s] Reseting Intan Board\n', mfilename);
                try
                    localObj.stop();
                end
                %IsInitialized
                localObj = [];
                return
            end
            
            if isempty(localObj) || ~isvalid(localObj)
                fprintf('[%s] Calling constructor\n',mfilename);
                localObj = Inputs.IntanFileInput
            else
                fprintf('[%s] Returning existing object\n',mfilename);
            end
            singleObj = localObj;
        end
    end
end


