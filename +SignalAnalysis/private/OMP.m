%% OMP Implementation
function xhat=OMP(y,A,S,MI)
% y is the measurement
% A is the sensing matrix
% S is the sparsity of the signal

% xhat is the reconstructed data

etol=1e-6;
xhat=zeros(size(A,2),1);
xhats=zeros(S,1); % sparse Xhat
Sset=[]; % column indices
residue=y;
iter=1;
while norm(residue)>etol
    residue0=residue;
    ydota=A'*residue0; % array of x values
    [lst idx]=sort(abs(ydota),'descend');
    Sset=[Sset idx(1)]; % need to check case where no data available -> currently collect data using another classifer then switch?
    Sset=unique(Sset);
%     Sset=union(Sset,idx(1));
%     xhats=pinv(A(:,Sset))*y;
    xhats=A(:,Sset)\y;
    xhat(Sset)=xhats;
    residue=y-A(:,Sset)*xhats;
    iter=iter+1;
    if norm(residue) == norm(residue0) || iter > MI
        break
    end
end
if isempty(Sset)==1
    Sset=1:S; % lazy error handling if null vector is measured
end
xhat(Sset)=xhats;
end