function LabelsNew = label_convert(Labels)
    classes    = unique(Labels);
    nClasses   = numel(classes);
    nTrainData = numel(Labels);
    LabelsNew = -ones(nClasses,nTrainData,'single');

    for i = 1 : nClasses
        LabelsNew(i,Labels==classes(i)) = 1;
    end

    LabelsNew = (LabelsNew+1)/2;    
end