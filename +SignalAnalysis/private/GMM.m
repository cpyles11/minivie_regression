function [GMMs_output, GMMi_output]=GMM(Atrain, Atest, trainlabels, testlabels, numComp)

numClasses=length(unique(trainlabels));
pC=1/numClasses; % equally weighted prior
PCAtrain=pca(Atrain); % reduce dimensionality of the data
PCAweight_train=Atrain*PCAtrain(:,1:numComp); % project data onto PCA space
PCAweight_test=Atest*PCAtrain(:,1:numComp); 

classmean=zeros(numClasses,numComp); % initialize mean of each class weight for training
classcovmatrix=cell(1,numClasses); % individual covariance matrices
sharedcovmatrix=zeros(numComp);% shared covariance matrix
for j=1:numClasses
    idx=find(trainlabels==j);
    classmean(j,:)=mean(PCAweight_train(idx,:));
    classcovmatrix{j}=cov(PCAweight_train(idx,:)); % compute covariances of each class
    sharedcovmatrix=sharedcovmatrix+classcovmatrix{j}*length(idx)/size(Atrain,1); % compute 1 model for all classes
end
GMMs_output=zeros(length(testlabels),1);
GMMi_output=zeros(length(testlabels),1);
temp=zeros(numClasses,1);
temp1=zeros(numClasses,1);
for i=1:length(testlabels)
    for j=1:numClasses % compute log likelihood using either individual covariance or single covariance
        temp(j)=(PCAweight_test(i,:)-classmean(j,:))*(sharedcovmatrix)^-1*(PCAweight_test(i,:)-classmean(j,:))'; 
        temp1(j)=(PCAweight_test(i,:)-classmean(j,:))*(classcovmatrix{j})^-1*(PCAweight_test(i,:)-classmean(j,:))'-2*log(pC); 
    end
    GMMs_output(i)=find(temp==min(temp),1);
    GMMi_output(i)=find(temp1==min(temp1),1);
end

end