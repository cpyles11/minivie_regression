function [outlabel,resarray]=SRC_MiniVIE(Atrain,Atest,trainlabels,S,MI,alg)
%% Sparse Representation Classification - Harrison Nguyen

% % inputs
% Atrain - training dictionary
% Atest - collection of test vectors
% trainlabels - class of each atom in dictionary
% S - sparsity level
% MI - max number of iterations
% alg - recovery algorithm to choose {'OMP','SP','CoSaMP','LP'}

% % outputs
% outlabel - labels assigned to test vectors
% resarray - array of residuals to serve as scores for ROC 6/29/2018
%%
if nargin<6
    alg='OMP'; % default to subspace pursuit
end

Atrain=normc(Atrain); % slight improvement?

classnum=unique(trainlabels,'stable'); % training labels must be numbers
outlabel=cell(size(Atest,2),1);
resarray=zeros(size(Atest,2),length(classnum));
for i=1:size(Atest,2)
    
    switch alg
        case 'OMP'
            imhat=OMP(Atest(:,i),Atrain,S,MI);
        case 'SP'
            imhat=SP(Atest(:,i),Atrain,S,MI);
        case 'CoSaMP'
            imhat=CoSaMP(Atest(:,i),Atrain,S,MI); 
        case 'Homotopy'
%             imhat=SolveHomotopy(Atrain, Atest(:,i));
            imhat = SolveHomotopy(Atrain, Atest(:,i), 'stoppingCriterion', -2,...
                'groundTruth', Atrain\Atest(:,i), 'maxtime', 0.025, 'maxiteration', 5, 'lambda',...
                0.01,'tolerance',0.1) ; 
    end
    
    residx=zeros(1,length(classnum));
    for j=1:length(classnum)
        residx(j)=norm(Atest(:,i)-Atrain(:,strcmp(classnum{j},trainlabels))*imhat(strcmp(classnum{j},trainlabels))); % residual of image from reconstruction based only on the subspace of class
    end
    SRClabel=classnum(min(residx)==residx);
    outlabel{i}=SRClabel{:};
    resarray(i,:)=residx;
end
end