%% Betthauser - 2016 --  Compute EA-SRC (L1 w/ Closed form) accuracy from data. 
% From:  J. Cao, K. Zhang, M. Luo, C. Yin and X. Lai, "Extreme 
%        learning machine and adaptive sparse representation for 
%        image classification," Neural networks (2016).

% INPUTS: Atrain - training data (numF x N)
%         trainlabels - training data labels (1 x N)  
%         Atest - testing data (numF x P)      
%         
% OUTPUT: [predict1] - EA-SRC (L1 w/ Homotopy) predictions
function [predict1] = classifyEASRCClosedoffline(traindata, trainlabel, testdata, lambda) 
    traindata  =  traindata./( repmat(sqrt(sum(traindata.*traindata)), [size(traindata,1),1]) );
    testdata   =  testdata./(repmat(sqrt(sum(testdata.*testdata)), [size(testdata,1),1]) );
    sizeTest = size(testdata,2);
    warning('off','all');
    % [traindata,PS] = mapminmax(traindata,-1,1);%
    % testdata = mapminmax('apply',testdata,PS);

    % [traindata,PS] = mapstd(traindata);%
    % testdata = mapstd('apply',testdata,PS);

    [trainlabel1] = label_convert(trainlabel);

    %-----------Setting----------------------------------------------------
    alpha             = 0.1;
    kclass            = length(unique(trainlabel))/2;  % for adaptive class domain selection
    nn.hiddensize     = 1000;       % hidden nodes number

    method            = {'ELM','RELM'};
    nn.activefunction = 's';
    nn.inputsize      = size(traindata,1);
    nn.method         = method{2};
    nn.type           = 'classification';
    %-----------Initializzation-----------

    nn                = elm_initialization(nn);

    %--------RELM-LOO--------------------
    nn.method         = method{2};
    nn.C              = exp(-4:0.2:4);
    [nn, ~]   = elm_train(traindata, trainlabel1, nn);
    %[nn, acc_test]   = elm_test(testdata, testlabel, nn);

    predict1 = zeros(1,sizeTest);
%     IDe     = zeros(1,sizeTest);
%     lamda   = 5e-4;
%     tol     = 1e-2;
    tic;
    f = 0;

    %--------RELM-LOO---SRC--------------------
    for i = 1 : size(testdata,2)

        [nn, ~]   = elm_test(testdata(:,i), [], nn);
        O = nn.testlabel;
        [Tf, id] = max(O);
        O(id) = -inf;
        [Ts, ~] = max(O);
        Tdiff = Tf-Ts;
%         IDe(i) = id;

        if Tdiff > alpha
            predict1(i) = id;
    %         if id ~= testlabel(i)
    %             fprintf('Wrong classification for %1.0f th testing sample by ELM criterion (|T_first - T_second| = %1.2f)   \n ', i, Tdiff);
    %         end
        else
            f = f + 1;
            [~, slabel] = sort(nn.testlabel, 'descend');
            newtrainlabel = trainlabel(ismember(trainlabel,slabel(1:kclass)));            
            newtraindata  = traindata(:,ismember(trainlabel,slabel(1:kclass)));
            I = eye(size(newtraindata,2));    
            y = testdata(:,i);
            s = pinv(newtraindata'*newtraindata + lambda*I)*newtraindata'*y; % Ax = b --> x = A\b 
            
%             s = l1_ls(newtraindata, y, lamda, tol, 1);
            newlabel = unique(newtrainlabel);

    %         if ~ismember(testlabel(i), newlabel)
    %             fprintf('Wrong classification for %1.0f th testing sample by ELM criterion (|Adaptive class domain|) \n ', i);
    %         end

            gap = zeros(1,length(newlabel));
            for indClass  =  1 : length(newlabel)
                coef_c    =  s(newtrainlabel==newlabel(indClass));
                Dc        =  newtraindata(:,newtrainlabel==newlabel(indClass));
                gap(indClass) = norm(y-Dc*coef_c)^2;
            end

            wgap3  = gap ;
            index3 = find(wgap3==min(wgap3));
            id3    = index3(1);
            id     = newlabel(id3);

    %         fprintf('%1.0f / %1.0f  %1.0f   %1.3f    %1.0f    %1.2f    %1.0f \n', i, size(testdata,2), f, sum(s), find(slabel==testlabel(i)), Tdiff, id==testlabel(i));

            predict1(i) = id;
        end
    end
end