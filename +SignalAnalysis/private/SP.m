%% SP Implementation
function xhat=SP(y,A,S,MI)
% y is the measurement
% A is the sensing matrix
% S is the sparsity of the signal
% MI is the max number of iterations

% xhat is the reconstructed data

Sset=[];
xhat=zeros(size(A,2),1);
xhats=zeros(S,1);
residue=y;
etol=1e-6;
iter=1;
while norm(residue)>etol
    residue0=residue;
    ydota=A'*residue0; % array of x values
    [lst idx]=sort(abs(ydota),'descend');
    c=idx(1:S);
    suc=[Sset c];
    suc=unique(suc);
%     suc=union(Sset,c);
%     [lst idx]=sort(abs(pinv(A(:,suc))*y),'descend');
    [lst idx]=sort(abs((A(:,suc))\y),'descend');
    Sset=suc(idx(1:S));
%     xhats=pinv(A(:,Sset))*y;
    xhats=A(:,Sset)\y;
    residue=y-A(:,Sset)*xhats;
    iter=iter+1;
    if norm(residue) == norm(residue0) || iter > MI
        break
    end
end
if isempty(Sset)==1
    Sset=1:S; % lazy error handling if null vector is measured
end
xhat(Sset)=xhats;
end