function xhat=linprog(y,A)
% y is the measurement
% A is the sensing matrix

% xhat is the reconstructed data

options = optimoptions('linprog','Algorithm','dual-simplex','Display','none','OptimalityTolerance',1.0000e-07);

f=ones(2*size(A,2),1);
A_eq=[A -A];
beq=y;
lb=zeros(size(f));
z = linprog(f,[],[],A_eq,beq,lb,[],options);
xhat=z(1:end/2,:)-z(end/2+1:end,:);
end