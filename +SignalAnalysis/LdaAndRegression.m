%classdef LdaAndRegression < SignalAnalysis.Classifier
classdef LdaAndRegression < handle
    % Joint velocity regression for APL elbow 1DOF
    properties
        classifier = []; % matlab output from fitcdiscr() used to classify emg signals
        regressors = []; % a separate neural net object trained for each class
        ClassNames = {'No Movement', 'Elbow Flexion' 'Elbow Extension'};
        %              0                1                  2
        NumClasses = 3;
        %NumSamplesPerWindow = 200;
        WindowTimeSize = 0.2; % 200 ms
        NumMajorityVotes = 5;
        ZcThresh = 0.15;
        SscThresh = 0.15;
        ActiveChannels = [1:3];
        FeatureList = {'mav', 'len', 'zc', 'ssc'};
        UseEncoder = 1;
        
        prSpeedBuffer = [];
        prSpeedVoteBuffer = [];
        
    end
    methods
        function obj = LdaAndRegression
            % Constructor
        end
        function train(obj, pxiDir, pxiFilenames, emgDir, emgFilenames)
   
            % Constants
            isPlot = false;
            
            % Loop through all of the files
            numTrials = length(emgFilenames);
            features_cell = cell(numTrials,1);
            labels_cell = cell(numTrials, 1);
            velocity_cell = cell(numTrials, 1);
            position_cell = cell(numTrials, 1);
            for iTrial=1:numTrials
                
                % Load data and extract the time alignm,ent parameters
                %% TODO, bring this function into a method
                [pxiData, emgVoltage, params] = obj.Baselayer_TimeAlignCheck(pxiDir, pxiFilenames{iTrial}, emgDir, emgFilenames{iTrial}, isPlot);
                
                % Create time aligned EMG  dataset
                emgData.voltage  = emgVoltage;
                emgData.time_sec =  params.t_amplifier - params.shiftdelta;
                
                % Create common time basis
                % TODO: Make sure this sample rate doesn't effect online
                % regression, as it might be at different rate.
                fs = 1000; % 1/s
                tCommon = get_common_time(pxiData.time_sec, emgData.time_sec, fs);
                
                % Interpolate signals to this common time basis
                Time       = tCommon;
                Position   = interp1(pxiData.time_sec, pxiData.position, Time, 'linear', 'extrap');
                Velocity   = interp1(pxiData.time_sec, pxiData.velocity, Time, 'linear', 'extrap');
                Torque     = interp1(pxiData.time_sec, pxiData.torque,   Time, 'linear', 'extrap');
                numEMGChannels  = size(emgData.voltage,2);
                EMG       = zeros(length(Time),  numEMGChannels);
                for i = 1:numEMGChannels
                    EMG(:,i) = interp1(emgData.time_sec, emgData.voltage(:,i), Time, 'linear', 'extrap');
                end
                % Filter the velocity
                order = 2;
                FNyquist = fs/2;
                cutoff = 10;
                [z,p,k] = butter(order,cutoff/FNyquist,'low');
                [sos, g] = zp2sos(z,p,k);
                Velocity = filtfilt(sos, g, Velocity);
                
                label_fname = fullfile(emgDir, strrep(emgFilenames{iTrial},'.rhd','_labels.mat'));
                if iscell(label_fname)
                    label_fname = label_fname{1};
                end
                if ~exist(label_fname, 'file')
                    % Label the data based on the encoder joint angle
                    Labels = label_flexion_extension(Time, Position);
                    
                    % Save label data
                    save(label_fname,'Labels');
                else
                    load(label_fname);
                end
                
                % Format as timetable
                Data = timetable(Labels, Position, Velocity, Torque, EMG, 'SampleRate', fs, 'StartTime', seconds(Time(1)));
                
                % Truncate the dataset so that it doesn't include non-labeled data
                idxStart = find(~isnan(Data.Labels),1);
                idxEnd   = find(~isnan(Data.Labels),1, 'last');
                Data = Data(idxStart:idxEnd, :);
                
                % Extract features
                % TODO: Make sure these align with windows from online
                % MiniVIE
                window_time_size = obj.WindowTimeSize; % 200 ms
                window_time_slide = 0.02; % 20 ms
                %obj.NumSamplesPerWindow = round(window_time_size * fs);
                zc_thresh = obj.ZcThresh;
                ssc_thresh = obj.SscThresh;
                feature_list = obj.FeatureList;
                
                % PLOT
                f = figure();
                hold on
                plot(Data.Time, Data.EMG(:,1));
                plot(Data.Time, Data.EMG(:,2));
                plot(Data.Time, Data.EMG(:,3));
                hold off
                xlabel('Time (s)');
                ylabel('Voltage');
                legend('Ch0','Ch3','Ch5');
                
                
                [features, labels, time] = feature_extract_slide(Data.EMG', Data.Labels', fs, window_time_size, window_time_slide, zc_thresh, ssc_thresh, feature_list);

                features_cell{iTrial} = features;
                labels_cell{iTrial} = labels';
                
                % Get velocity at point that each classification is made
                slide_size = round(window_time_slide * fs); %converts slide from time
                window_size = round(window_time_size * fs);
                velocity = Data.Velocity(1:slide_size:end-window_size);
                velocity_cell{iTrial} = velocity;
                position = Data.Position(1:slide_size:end-window_size);
                
                % Add position to feature list
                if obj.UseEncoder
                    features_cell{iTrial} = [features_cell{iTrial}, position];
                end
                
            end
            
            
            % Train and evaluate model
            [nets, classifiers, R2, Performance, CrossCorr, errorRate, R2Lumped, PerformanceLumped] = obj.emg_regression(features_cell, labels_cell, velocity_cell);
            
            %% Set object properties
            [r2, idxBest] = max(R2Lumped);
            bestNets = nets(:,idxBest);
            obj.regressors = bestNets;
            [perf, idxBest] = min(errorRate);
            obj.classifier = classifiers{idxBest};     
            
        end
        function [classOut, voteDecision, velocityOut, voteVelocityOut] = classify(obj,featuresColumns)
            % Feature must be sent in same order that was used to train
            % obj.classifier and obj.regressors
            
            % Perform classification
            classOut = predict(obj.classifier,featuresColumns);
            numDecisions = length(classOut);
            
            % Majority vote
            if nargout > 1
                voteDecision = zeros(numDecisions,1);
                for i = 1:numDecisions
                    [voteDecision(i), recentDecisions] = obj.majority_vote(classOut(i), obj.NumMajorityVotes, obj.NumClasses, 1, 1);
                end
            end
            
            % Perform regression
            if voteDecision == 0 % No movement
                velocityOut = 0;
                voteVelocityOut = 0;
            else
                thisNet = obj.regressors{voteDecision + 1};
                velocityOut = thisNet(featuresColumns');
                idxSameVotes = find(recentDecisions == voteDecision);
                recentVelocities = flipud(obj.prSpeedBuffer(end-obj.NumMajorityVotes + 1:end));
                voteVelocityOut = mean(recentVelocities(idxSameVotes));
            end
            
        end
        % Classifies using the emg features and labels, outputs results
        % Input: Cell array (numFolds) of feature matricis (Samples x Features), Cell array (numFolds) of Label vectors (samples x 1)
        % Output: ErrorRate (1 x numFolds) (error from each cross validation fold)
        function [nets, classifiers, r2, performance, crossCorr, errorRate, r2Lumped, performanceLumped] = emg_regression(obj,features, labels, velocities)
            
            % Train a separate regressor for each class, across all folds, assuming
            % perfect classification
            % TODO: Input position and train different regressors depending on
            % position
            allClasses = unique(labels{1});
            numClasses = length(allClasses);
            assert(numClasses == obj.NumClasses);
            numNetsToTrain = 10;
            nets = cell(numClasses, numNetsToTrain);
            
            all_features = vertcat(features{:});
            all_labels = vertcat(labels{:});
            all_velocities = vertcat(velocities{:});
            
            performance = NaN(numClasses, numNetsToTrain);
            r2 = performance;
            crossCorr= performance;
            tTest = cell(numClasses,1); % true values
            yTest = cell(numClasses,1); % predicted values
            for iClass = 1:numClasses
                thisClass = allClasses(iClass);
                
                idx = [all_labels == thisClass];
                
                all_feat = all_features(idx,:);
                all_vel = all_velocities(idx);
                
                tTestClass = [];
                yTestClass = [];
                
                % Train multiple nets
                for i = 1:numNetsToTrain
                    [nets{iClass,i}, r2(iClass,i), performance(iClass,i), t,y]  = train_fitnet(all_feat', all_vel');
                    tTestClass = [tTestClass,t'];
                    yTestClass = [yTestClass,y'];
                end
                tTest{iClass} = tTestClass;
                yTest{iClass} = yTestClass;
            end
            
            %Calc lumped R2 values across flexion and extension
            r2Lumped = NaN(1, numNetsToTrain);
            performanceLumped = NaN(1, numNetsToTrain);
            for i = 1:numNetsToTrain
                t = vertcat(tTest{2}(:,i), tTest{3}(:,i));
                y = vertcat(yTest{2}(:,i), yTest{3}(:,i));
                r = corrcoef(t,y);
                r = r(2,1);
                r2Lumped(i) = r*r;
                performanceLumped(i) = mean((t-y).^2);
                
                
                fR = figure();
                hold on
                scatter(t,y);
                xlabel('True Angular Velocity');
                ylabel('Predicted Angular Velocity');
                title(sprintf('Testing Set: R^2 = %0.2f',r2Lumped(i)));
                hold off
%                 PlotUtils.setPlotEBK();
            end
            
            
            % Train a classifier for each fold
            numFolds = length(features);
            errorRate = zeros(1,numFolds);
            classifiers = cell(1,numFolds);
            for i = 1:numFolds
                all_idx = [1:1:numFolds];
                
                % get indices of testing set and training set
                testing_idx = i;
                %training_idx = all_idx(all_idx~=i);
                training_idx = all_idx(all_idx);
                
                % use incdices to split testing and training sets
                test_feat = vertcat(features{testing_idx});
                test_lab = vertcat(labels{testing_idx});
                test_vel = vertcat(velocities{testing_idx});
                train_feat = features{training_idx};
                train_lab = labels{training_idx};
                train_vel = vertcat(velocities{training_idx});
                
                % Perform classification
                Mdl = fitcdiscr(train_feat, train_lab);
                classifiers{i} = Mdl;
                out_labels = predict(Mdl,test_feat);
                vote_out_labels = SignalAnalysis.LdaAndRegression.majority_vote_static(out_labels, obj.NumMajorityVotes);
                % evaluate class performance
                if length(unique(test_lab)) == 1
                    differences = 0;
                    for j = 1:length(test_lab)
                        if (test_lab(j,1) ~= vote_out_labels(j,1))
                            differences = differences + 1;
                        end
                    end
                    errorRate(1,i) = differences/length(test_lab);
                else
                    CP = classperf(test_lab, vote_out_labels);
                    errorRate(1,i) = CP.ErrorRate;
                end
                
                % Now run the run the regressor on classified data
                velocity_predictions = NaN(size(test_vel,1), numNetsToTrain);
                velocity_predictions_class = NaN(size(test_vel,1), numNetsToTrain);
                residuals = velocity_predictions;
                residuals_class = velocity_predictions;
                for iClass = 1:numClasses
                    thisClass = allClasses(iClass);
                    
                    % Use ground truth labels
                    idx_test = [test_lab == thisClass];
                    reg_test_feat = test_feat(idx_test,:)';
                    reg_test_vel = test_vel(idx_test,:)';
                    
                    % Use classified ones
                    idx_test_class = [vote_out_labels == thisClass];
                    reg_test_feat_class = test_feat(idx_test_class,:)';
                    reg_test_vel_class = test_vel(idx_test_class,:)';
                    
                    % Run regression on each trained net
                    % Run one assuming perfect classification, one using actual
                    % classification
                    
                    for i = 1:numNetsToTrain
                        thisNet = nets{iClass, i};
                        
                        % Predict and stitch data back together
                        % Calculate residual
                        if thisClass == 0
                            velocity_prediction = NaN(1, length(reg_test_vel));;
                            velocity_prediction_class = NaN(1, length(reg_test_vel_class));
                            residual = NaN(1, length(reg_test_vel));
                            residual_class = NaN(1, length(reg_test_vel_class));
                        else
                            
                            velocity_prediction = thisNet(reg_test_feat);
                            velocity_prediction_class = thisNet(reg_test_feat_class);
                            residual = gsubtract(reg_test_vel, velocity_prediction);
                            residual_class = gsubtract(reg_test_vel_class, velocity_prediction_class);
                        end
                        velocity_predictions(idx_test, i) = velocity_prediction;
                        velocity_predictions_class(idx_test_class, i) = velocity_prediction_class;
                        residuals(idx_test,i) = residual;
                        residuals_class(idx_test_class,i) = residual_class;
                        
                        % Calculate the cross-corr between actual and predicted
                        % velocity
                        N = length(velocity_prediction);
                        x = velocity_prediction;
                        y = reg_test_vel;
                        cc = 0;
                        for j = 1:N
                            cc = cc + ((x(j) - mean(x)) * (y(j) - mean(y)));
                        end
                        cc = cc/(N*sqrt(var(x)*var(y)));
                        crossCorr(iClass,i) = cc;
                    end
                end
                
                % Calculate Overall R2
                yresid = residuals(~isnan(residuals(:,1)),:);
                yresid_class = residuals_class(~isnan(residuals_class(:,1)),:);
                SSresid = sum(yresid.^2);
                SSresid_class = sum(yresid_class.^2);
                
                y = test_vel;
                y_class = test_vel;
                SStotal = (size(y,1)-1) * var(y,0,1);
                SStotal_class = (size(y_class,1)-1) * var(y_class,0,1);
                
                rsq = 1 - SSresid./SStotal;
                rsq_class = 1 - SSresid_class./SStotal_class;
                
                % Calculate R2 For Each Class
                rsq_by_label = NaN(numClasses, numNetsToTrain)
                rsq_by_label_class = NaN(numClasses, numNetsToTrain)
                for iClass = 1:numClasses
                    thisClass = allClasses(iClass);
                    if thisClass == 0
                        continue
                    else
                        yresid = residuals([test_lab == thisClass],:);
                        yresid_class = residuals_class([vote_out_labels == thisClass],:);
                        SSresid = sum(yresid.^2);
                        SSresid_class = sum(yresid_class.^2);
                        
                        y = velocity_predictions(~isnan(velocity_predictions(:,1)),:);
                        y_class = velocity_predictions_class(~isnan(velocity_predictions_class(:,1)),:);
                        SStotal = (size(y,1)-1) * var(y,0,1);
                        SStotal_class = (size(y_class,1)-1) * var(y_class,0,1);
                        
                        rsq_by_label(iClass,:) = 1 - SSresid./SStotal;
                        rsq_by_label_class(iClass,:) = 1 - SSresid_class./SStotal_class;
                    end
                end
                
                time = [1:1:length(test_vel)]*0.02;
%                 % Plot the best one based on R2
%                 [m,idx] = max(mean(rsq_by_label_class(2:3,:)));
%                 f = figure();
%                 plot(time,test_vel,'DisplayName', 'Target');
%                 hold on
%                 v_predict = velocity_predictions(:,idx);
%                 v_predict(isnan(v_predict)) = 0;
%                 plot(time,v_predict, 'DisplayName', 'Predicted (True Labels)');
%                 v_predict_class = velocity_predictions_class(:,idx);
%                 v_predict_class(isnan(v_predict_class)) = 0;
%                 %plot(time,v_predict_class, 'DisplayName', 'Predicted');
%                 hold off
%                 xlabel('Time (s)');
%                 ylabel('Elbow Velocity (deg/s)');
%                 legend('location','best');
%                 PlotUtils.setPlotEBK();
                
                % Plot position
%                 f = figure();
%                 test_pos = cumtrapz(time,test_vel);
%                 plot(time,test_pos,'DisplayName', 'Target');
%                 hold on
%                 pos_predict = cumtrapz(time,v_predict);
%                 %plot(time,pos_predict, 'DisplayName', 'Predicted (True Labels)');
%                 pos_predict_class = cumtrapz(time,v_predict_class);
%                 plot(time,pos_predict_class, 'DisplayName', 'Predicted');
%                 hold off
%                 xlabel('Time (s)');
%                 ylabel('Elbow Position (deg)');
%                 legend('location','best');
%                 PlotUtils.setPlotEBK();
                
                
            end
        end
        function classNames = getClassNames(obj)
            classNames = obj.ClassNames;
        end
        function activeChannels = getActiveChannels(obj)
            activeChannels = obj.ActiveChannels;
        end
        function features2D = extractfeatures(obj,filteredDataWindowAllChannels, fs)
            % features2D = feature_extract(filteredDataWindowAllChannels(:,obj.getActiveChannels)',obj.NumSamplesPerWindow);
            features2D = feature_extract(filteredDataWindowAllChannels',obj.WindowTimeSize*fs,obj.ZcThresh,obj.SscThresh);
        end
        function close(obj)
        end
    end
    methods (Static = true)
        function [PXI_data, EMG_data, params] = Baselayer_TimeAlignCheck(PXI_folder_name, PXI_filename, EMG_folder_name, EMG_filename, isPlot)
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %%%Load the Labview/PXI data
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            PXI_data =  SignalAnalysis.LdaAndRegression.PXI_dataload(PXI_filename,PXI_folder_name);
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %%%Load the EMG data
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            if iscell(EMG_filename)
                board_dig_in_data = [];
                amplifier_data = [];
                t_amplifier_all = [];
                for i = 1:length(EMG_filename)
                    [this_board_dig_in_data, this_sample_rate, this_record_time, this_amplifier_data] = read_Intan_RHD2000_file_v2(EMG_filename{i}, EMG_folder_name);%#ok
                    board_dig_in_data = [board_dig_in_data, this_board_dig_in_data];
                    amplifier_data = [amplifier_data, this_amplifier_data];
                    t_amplifier_all = [t_amplifier_all, t_amplifier];
                end
                t_amplifier = t_amplifier_all;
            else
                [board_dig_in_data, sample_rate, record_time, amplifier_data] = read_Intan_RHD2000_file_v2(EMG_filename, EMG_folder_name);%#ok
            end
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
            
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %%%Look at the sync data, check for drift
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %%%
            %%%Identify the transition from low to high for each set of sync data
            syncUp_PXI = find(diff(PXI_data.sync)>0);
            %%%
            syncUp_EMG = find(diff(board_dig_in_data)>0);
            %%%
            Qty_mutualpulses = min([length(syncUp_PXI) length(syncUp_EMG)]);
            %%%
            %%%Check for drift in the detection of sync pulses
            A = t_amplifier(syncUp_EMG(1:Qty_mutualpulses))';%EMG
            B = PXI_data.time_sec(syncUp_PXI(1:Qty_mutualpulses));%labview
            %%%
            %%%You don't want an overall offset to skew things, so normalize them both
            %%%to start at zero
            A = A - A(1);
            B = B - B(1);
            %%%
            driftcheck = A - B;
            %%%
            
            if isPlot
                figure; hold on; box off; grid on;
                plot(1000*driftcheck,'k*')
                xlabel('Sync Pulse Index');
                ylabel('Time difference (milliseconds)');
                title('Checking for drift in the sync pulse timing');
            end
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %%%Determine what your time-allignment parameter is going to be (use the
            %%%first sync pulse, an alternative could be the mean/median of all the
            %%%sync pulses)
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %%%
            %%%Index of first sync pulse in both files
            check1 = find(board_dig_in_data>0.5);
            check2 = find(PXI_data.sync>0.5);
            %%%
            %%%Time offset between the two data files
            shiftdelta = t_amplifier(check1(1)) - PXI_data.time_sec(check2(1));
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %%%For each EMG data
            %%%
            %%%Plot the EMG
            %%%Overlay with the Encoder data
            %%%Overlay with the Time sync data
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %%%
            EMG_labels = {['EMG-1'];['EMG-2'];['EMG-3'];['EMG-4'];['EMG-5'];['EMG-6'];['EMG-7'];['EMG-8'];['EMG-9'];['EMG-10'];['EMG-11'];['EMG-12'];['EMG-13'];['EMG-14'];['EMG-15'];['EMG-16']};
            %%%
            EMG_data = amplifier_data';
            %%%
            qty_EEG = size(EMG_data,2);
            %%%
            sync_scale = 150;
            %%%
            
            params.t_amplifier = t_amplifier;
            params.shiftdelta = shiftdelta;
            
            if isPlot
                for k=1:qty_EEG
                    %%%
                    figure; hold on; box off; grid on;
                    %%%Plot the EMG data
                    %%%Normalize to +/-180 (nicer plotting with the encoder data)
                    plot([t_amplifier - shiftdelta], 90+90*EMG_data(:,k)./max(abs(EMG_data(:,k))))%EMG
                    %%%
                    %%%Encoder data
                    plot(PXI_data.time_sec, PXI_data.position,'g', 'linewidth', 2)%encoder
                    %%%
                    %%%Labview sync
                    plot(PXI_data.time_sec, sync_scale*PXI_data.sync,'k','linewidth',2)
                    %%%EMG sync
                    plot(t_amplifier - shiftdelta, sync_scale*board_dig_in_data,'--r','linewidth',2)
                    %%%
                    ylabel('EMG is normalized, with an offset')
                    xlabel('Time (seconds)');
                    %%%
                    legend(EMG_labels{k},'Encoder','Sync:PXI','Sync:EMG');
                    %%%
                    axis([-inf inf -25 200])
                end
                
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                %%%Plot all the time-aligned data
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                %%%
                figure; hold on; box off; grid on;
                %%%
                %%%Plot the EMG data
                plot([t_amplifier - shiftdelta],amplifier_data')%EMG
                %%%
                %%%Labview sync
                plot(PXI_data.time_sec, sync_scale*PXI_data.sync,'k','linewidth',2)
                %%%EMG sync
                plot(t_amplifier - shiftdelta, sync_scale*board_dig_in_data,'--r','linewidth',2)
                %%%
                %%%Encoder data
                plot(PXI_data.time_sec, PXI_data.position,'g', 'linewidth', 2)%encoder
                %%%
                %legend('Sync:PXI','Sync:EMG','Encoder');
                %%%
                xlabel('Time (seconds)');
                ylabel('Encoder has a factor of 10');
                %%%
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                
            end
        end
        function PXI_data = PXI_dataload(filename,folder_name)
            
            if false
                %%%directory containing convertTDMS.m
                addpath C:\Cdrive\MATLAB\R2013a\work\baselayer\DataFiles
            end
            
            
            if nargin < 2
                folder_name='';
                folder_name=[];
                folder_name='C:\Cdrive\MATLAB\R2013a\work\baselayer\DataFiles\PXIdata_20181017';
            end
            
            if nargin < 1
                filename='Test - 10-17-18--05-20-46.tdms';
                [filename, older_name, filterindex] =  uigetfile('*.tdms', 'Select an EMG Data File', 'MultiSelect', 'off');
            end
            
            
            %%%Convert the labview TDMS file
            raw_data = convertTDMS(1,fullfile(folder_name,filename));
            raw_data = raw_data.Data.MeasuredData(3).Data;
            
            %%%
            %%%Pull out the values of each parameter from the raw data
            %%%
            index_increment = 9;
            %%%
            PXI_data.time       = raw_data((1+0):index_increment:end);
            PXI_data.stop_time  = raw_data((1+1):index_increment:end);
            PXI_data.cur_cmd    = raw_data((1+2):index_increment:end);
            PXI_data.cur        = raw_data((1+3):index_increment:end);
            PXI_data.torque     = raw_data((1+4):index_increment:end);
            PXI_data.position   = raw_data((1+5):index_increment:end);
            PXI_data.velocity   = raw_data((1+6):index_increment:end);
            PXI_data.sync       = raw_data((1+7):index_increment:end);
            PXI_data.strain_gauge = raw_data((1+8):index_increment:end);

            
            % %%%
            % %%%Revised data log in which 8 values are stored
            % index = 1;
            % for i=1:8:length(raw_data)
            %     PXI_data.time(index) = raw_data(i);
            %     PXI_data.stop_time(index) = raw_data(i+1);
            %     PXI_data.cur_cmd(index) = raw_data(i+2);
            %     PXI_data.cur(index) = raw_data(i+3);
            %     PXI_data.torque(index) = raw_data(i+4);
            %     PXI_data.position(index) = raw_data(i+5);
            %     PXI_data.velocity(index) = raw_data(i+6);
            %     PXI_data.sync(index) = raw_data(i+7);
            %     index = index + 1;
            % end
            
            %%%Original version (in which only 7 variables were stored)
            % index = 1;
            % for i=1:7:length(raw_data)
            %     time(index) = raw_data(i);
            %     stop_time(index) = raw_data(i+1);
            %     cur_cmd(index) = raw_data(i+2);
            %     cur(index) = raw_data(i+3);
            %     torque(index) = raw_data(i+4);
            %     position(index) = raw_data(i+5);
            %     velocity(index) = raw_data(i+6);
            %     index = index + 1;
            % end
            
            %%%
            %%%Get time in seconds
            %stop_time = (stop_time - time(1))/1e9;
            %stop_time(1) = 0;
            PXI_data.time_sec = (PXI_data.time - PXI_data.time(1))/1e9;
        end
        function [vote_out_labels] = majority_vote_static(out_labels, numVotes)
            %MAJORITY_VOTE Smooths out out_labels based upon a majority vote of the
            %past 'vote_size'.  If there is a tie, it takes the most recent vote that
            %tied as the winner
            % Adam Polevoy 2018
            
            % number of voting labels, including current
            vote_size = numVotes;
            
            % preallocate
            vote_out_labels = zeros(length(out_labels),1);
            
            % For first 5, can't vote, not enough points
            vote_out_labels(1:vote_size-1) = out_labels(1:vote_size-1);
            
            % for each out label
            for i = vote_size:length(out_labels)
                % three possible out labels
                votes = zeros(3,1);
                
                % keep track of how many times each one is done
                for j = i-vote_size+1:i
                    votes(out_labels(j,1)+1,1) = votes(out_labels(j,1)+1,1)+1;
                end
                
                % find label with most votes
                maxval = max(votes);
                idx = find(votes == maxval);
                
                % if tied
                if length(idx) > 1
                    % find most recent one
                    for j = 0:4
                        if ismember(idx,out_labels(i-j)+1)
                            vote_out_labels(i) = idx-1;
                            break;
                        end
                    end
                    % otherwise
                else
                    vote_out_labels(i) = idx-1;
                end
            end
        end
        function [voteDecision, recentDecisions] = majority_vote(classDecision, numVotes, ...
                numClasses, numClassifiers, currentClassifier)
            % perform majority voting
            % Inputs:
            %   uint8   classDecision[1];
            %   uint8   numVotes[1];
            %   uint8   numClasses[1];
            % Outputs:
            %   uint8   voteDecision[1];
            %
            % Note Code contains an internal buffer for determining winner.
            % Also there is an assumption about the no movement class being
            % the last class which causes an immediate stop if only two NM
            % classes are detected.
            %
            % R. Armiger: Created
            
            persistent decisionHistory
            
            % Maintain a buffer of the latest decisions
            maxVotes = uint8(255);  % CONST
            %voteDecision = uint8(numClasses);  % init to No movement (last class)
            voteDecision = uint8(1);  % init to No movement
            
            %validate classDecision input range
            % SHIFT UP BY 1 !!!!
            classDecision = classDecision + 1;
            classDecision = uint8(classDecision);
            classDecision = max(classDecision,1);
            classDecision = min(classDecision,numClasses);
            
            %validate numVotes input range
            numVotes = uint8(numVotes);
            numVotes = max(numVotes,1);
            numVotes = min(numVotes,maxVotes);
            
            numClasses = uint8(numClasses);
            
            % log decisions
            if isempty(decisionHistory)
                % init buffer
                decisionHistory = zeros(maxVotes,numClassifiers,'uint8');
            else
                % shift and put newer samples at position 1
                %     decisionHistory = circshift(decisionHistory,1);
                for iValue = size(decisionHistory,1)-1:-1:1
                    decisionHistory(iValue+1,currentClassifier) = decisionHistory(iValue,currentClassifier);
                end
                decisionHistory(1,currentClassifier) = classDecision;
            end
            
            % tally votes
            tally = zeros(numClasses,1,'uint8');
            for iVote = 1:numVotes
                myVote = decisionHistory(iVote,currentClassifier);
                if myVote > 0
                    tally(myVote) = tally(myVote) + 1;
                end
            end
            
            % find winner
            [maxTally, maxTallyId] = eml_max(tally);
            
            % check for ties
            isTie = sum((tally == maxTally)) > 1;
            
            if ~isTie
                voteDecision = uint8(maxTallyId);
            else
                % if we have a tie, the tied leader that occurred most recently wins
                listOfWinners = ((1:numClasses)' .* uint8(tally == maxTally)); % alternative to find, find is invalid in eml
                for i = 1:numVotes  % order is important here
                    if any(listOfWinners == decisionHistory(i,currentClassifier))
                        voteDecision = decisionHistory(i);
                        break
                    end
                end
            end
            
            % RSA: Add an immediate stop if 2 no movements are detected
            % at the end of the majorty vote buffer
           % noMovementClass = numClasses;  % NM must be last class!
           noMovementClass = 1; 
            
            if (numVotes > 1) && ...
                    (decisionHistory(1) == noMovementClass) && ...
                    (decisionHistory(1) == noMovementClass)
                voteDecision = noMovementClass;
            end
            
            % Correct for the initial increment at beginning of function
            voteDecision = voteDecision - 1;
            recentDecisions = decisionHistory(1:numVotes) - 1;
            
        end
    end
end
