classdef GMM < SignalAnalysis.Classifier
    % Gaussian Mixture Model as a classifier
    % computes two covariance matrices (either shared or for each class)
    % and use the one that leads to best accuracy based on training
    %
    % 23-07-18 Harrison Nguyen
    % TODO: for training, create a loop that iterates through number of PCA
    % components until optimal accuracy is achieved
    properties
        % Note store model in Wg
    end
    methods
        
        function confuseMat = train(obj)
            
            printStatus(obj);
            td = obj.TrainingData;
            
            [predictors, response] = getDataTable(td);
            
            if isempty(response)
                fprintf('[%s] No Training Data\n',mfilename);
                obj.Wg = [];
                obj.Cg = [];
                obj.IsTrained = false;
                return
            end
            
            tic
            fprintf('Training %s...',mfilename);
            numComp=6; % guess to be updated
            numComp=min(numComp,size(predictors,2)); % handle case where less features than 10 components
            numClasses=length(unique(response));
            numConverted = obj.cellArrayToNum(response,obj.getClassNames);
            pC=1/numClasses; % equally weighted prior
            PCAtrain=pca(predictors); % reduce dimensionality of the data
            PCAweight_train=predictors*PCAtrain(:,1:numComp); % project data onto PCA space
            PCAweight_test=predictors*PCAtrain(:,1:numComp); 

            classmean=zeros(numClasses,numComp); % initialize mean of each class weight for training
            classcovmatrix=cell(1,numClasses); % individual covariance matrices
            sharedcovmatrix=zeros(numComp);% shared covariance matrix
            for j=1:numClasses
                idx=find(numConverted==j);
                classmean(j,:)=mean(PCAweight_train(idx,:));
                classcovmatrix{j}=cov(PCAweight_train(idx,:)); % compute covariances of each class
                sharedcovmatrix=sharedcovmatrix+classcovmatrix{j}*length(idx)/size(predictors,1); % compute 1 model for all classes
            end
            fprintf('Done. T = %f s\n',toc)
            
            % Predict original model
            GMM_output=zeros(2,length(response));
            temp=zeros(numClasses,1);
            temp1=zeros(numClasses,1);
            for i=1:length(response)
                for j=1:numClasses % compute log likelihood using either individual covariance or single covariance
                    temp(j)=(PCAweight_test(i,:)-classmean(j,:))*(sharedcovmatrix)^-1*(PCAweight_test(i,:)-classmean(j,:))'; 
                    temp1(j)=(PCAweight_test(i,:)-classmean(j,:))*(classcovmatrix{j})^-1*(PCAweight_test(i,:)-classmean(j,:))'; 
                end
                GMM_output(1,i)=find(temp==min(temp),1); % shared covariance
                GMM_output(2,i)=find(temp1==min(temp1),1); % individual class covariance
            end
            GMM_acc(1)=sum(GMM_output(1,:)==numConverted)/length(numConverted); % doesn't seem to vary depending on training set
            GMM_acc(2)=sum(GMM_output(2,:)==numConverted)/length(numConverted);
            
            [~,GMMtype]=max(GMM_acc);
            p=GMM_output(GMMtype,:);
            
            confuseMat = confusionmat({td.ClassNames{p}},response,'order',td.ClassNames);
            
            obj.IsTrained = true;
            obj.Wg{1} = numComp;
            obj.Wg{2} = PCAtrain;
            obj.Wg{3} = classmean;
            if GMMtype==1
                obj.Wg{4} = sharedcovmatrix;
            else
                obj.Wg{4} = classcovmatrix;
            end
        end
        function [classOut, voteDecision] = classify(obj,featuresColumns)
%             assert(size(featuresColumns,1) == obj.NumActiveChannels*obj.NumFeatures,...
%             assert(size(featuresColumns,1) == obj.NumActiveChannels*length(obj.TrainingData.ActiveFeatures),... % hack
%                 'Expected first dimension of featuredata [%d]to be equal to numActiveChannels*numFeatures [%d]',...
%                 size(featuresColumns,1),obj.NumActiveChannels*length(obj.TrainingData.ActiveFeatures));
%                 size(featuresColumns,1),obj.NumActiveChannels*obj.NumFeatures);
            if isempty(obj.Wg)
                warning('Classifier not trained');
                [classOut, voteDecision] = deal(length(obj.getClassNames));
                return
            end
            
            trainedClassifier = obj.Wg;
            numComp=trainedClassifier{1};
            PCAtrain=trainedClassifier{2};
            classmean=trainedClassifier{3};
            covmatrix=trainedClassifier{4};
            numClasses=size(classmean,1);
            pC=1/numClasses;
            
            predictors = featuresColumns';
            
            PCAweight_test=predictors*PCAtrain(:,1:numComp); 
            
            % Predict original model
            for i=1:size(predictors,1)
                for j=1:numClasses % compute log likelihood using either individual covariance or single covariance
                    if iscell(covmatrix)
                        temp(j)=(PCAweight_test(i,:)-classmean(j,:))*(covmatrix{j})^-1*(PCAweight_test(i,:)-classmean(j,:))'; 
                    else
                        temp(j)=(PCAweight_test(i,:)-classmean(j,:))*(covmatrix)^-1*(PCAweight_test(i,:)-classmean(j,:))'; 
                    end
                end
                p(i)=find(temp==min(temp),1); 
            end
            
            numConverted = p;
            
            classOut = numConverted;
            voteDecision = numConverted;
        end
        function close(obj)
        end
    end
end
