classdef EASRC < SignalAnalysis.Classifier
    % EASRC < SignalAnalysis.Classifier
    % Class file for interfacing MATLAB Signal Processing machine Learning
    % Algorithms within MiniVIE
    %
    % 26-June-2018 Nguyen: Created based on JLB functions 2017
%     TODO: Modify code to extract neural net for real time training + test
%     rather than train again each time
%     This code also only works offline or with an initial data set already -> need to figure out labels 
    properties
        % Note store model in Wg
    end
    methods
        
        function confuseMat = train(obj)
            
            printStatus(obj);
            td = obj.TrainingData;
            
            [predictors, response] = getDataTable(td);
            
            if isempty(response)
                fprintf('[%s] No Training Data\n',mfilename);
                obj.Wg = [];
                obj.Cg = [];
                obj.IsTrained = false;
                return
            end
            
            fprintf('Training %s...',mfilename);
            
            warning('off','all');
            classlabels=obj.getClassNames;
            predictorsNum=obj.cellArrayToNum(response,classlabels);
            lambda=0.01; % bad
            p=classifyEASRCClosedoffline(predictors', predictorsNum', predictors',lambda);
    
            confuseMat = confusionmat({classlabels{p}},response,'order',td.ClassNames);
            
            obj.IsTrained = true;
%             obj.Wg = trainedClassifier;
            obj.Wg = lambda;
        end
        function [classOut, voteDecision] = classify(obj,featuresColumns)
            assert(size(featuresColumns,1) == obj.NumActiveChannels*obj.NumFeatures,...
                'Expected first dimension of featuredata [%d]to be equal to numActiveChannels*numFeatures [%d]',...
                size(featuresColumns,1),obj.NumActiveChannels*obj.NumFeatures);
            if isempty(obj.Wg)
                warning('Classifier not trained');
                [classOut, voteDecision] = deal(length(obj.getClassNames));
                return
            end
            td = obj.TrainingData;
%             [~, response] = getDataTable(td);
            [predictors, response] = getDataTable(td);
            classlabels=obj.getClassNames;
            predictorsNum=obj.cellArrayToNum(response,classlabels);
            
%             trainedClassifier = obj.Wg;
%             predictors = featuresColumns';
            lambda=obj.Wg;
            
            p=classifyEASRCClosedoffline(predictors', predictorsNum', featuresColumns,lambda);
            
            numConverted = p;
            
            classOut = numConverted;
            voteDecision = numConverted;
        end
        function close(obj)
        end
    end
end