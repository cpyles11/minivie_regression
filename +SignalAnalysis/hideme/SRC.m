classdef SRC < SignalAnalysis.Classifier
    % SRC < SignalAnalysis.Classifier
    % Class file for interfacing MATLAB Signal Processing machine Learning
    % Algorithms within MiniVIE
    %
    % 08-June-2018 Nguyen: Created
% %     TODO: make SRC robust for strings or numbers as labels
% %         Exploit OOP for SRC parameters? Sparsity S, Max Iter MI, l1 alg
% %         Run in real time since there's no model/prior data to use to
% train (MiniVIE will return an error!)
    properties
        % Note store model in Wg -> there is no explicit model here! In
        % SRC, we just attempt to solve y=Ax s.t. x has a few nonzero
        % entries, expressing y as a linear combination of redundant vecs
    end
    methods
        
        function confuseMat = train(obj)
            
            printStatus(obj);
            td = obj.TrainingData;
            
            [predictors, response] = getDataTable(td);
            
            if isempty(response)
                fprintf('[%s] No Training Data\n',mfilename);
                obj.Wg = [];
                obj.Cg = [];
                obj.IsTrained = false;
                return
            end
            
            % SRC parameters
            S=1; % S=1 perfect recovery on own data...
            MI=S; % do this for OMP only
%             MI=10; % do this for OMP only
            p = SRC_HN(predictors',predictors',response,S,MI);
%             p = SRC_HN(predictors',predictors',response,S,MI,'OMP');
            
            confuseMat = confusionmat(p,response,'order',td.ClassNames);
            
%             obj.IsTrained = true;
%             obj.Wg = trainedClassifier;
            obj.IsTrained = true;
        end
        function [classOut, voteDecision] = classify(obj,featuresColumns)
            assert(size(featuresColumns,1) == obj.NumActiveChannels*obj.NumFeatures,...
                'Expected first dimension of featuredata [%d]to be equal to numActiveChannels*numFeatures [%d]',...
                size(featuresColumns,1),obj.NumActiveChannels*obj.NumFeatures);
%             if isempty(obj.Wg)
%                 warning('Classifier not trained');
%                 [classOut, voteDecision] = deal(length(obj.getClassNames));
%                 return
%             end
            
%             trainedClassifier = obj.Wg;
%             predictors = featuresColumns';

            td = obj.TrainingData;
            
            [predictors, response] = getDataTable(td);
            S=1;
            MI=S; % do this for OMP only
            p = SRC_HN(predictors',featuresColumns,response,S,MI);
%             p = predict(trainedClassifier,predictors);
            
            numConverted = obj.cellArrayToNum(p,obj.getClassNames);
            
            classOut = numConverted;
            voteDecision = numConverted;
        end
        function close(obj)
        end
    end
end
