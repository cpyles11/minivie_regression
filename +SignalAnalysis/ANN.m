classdef ANN < SignalAnalysis.Classifier
    % ANN < SignalAnalysis.Classifier
    % Class file for interfacing MATLAB Signal Processing machine Learning
    % Algorithms within MiniVIE
    %
    % 08-June-2018 Nguyen: Created based on JLB 2017
%     NEED TO CHECK LICENSE - updated 6/28/2018 and verified to work in real time
    properties
        % Note store model in Wg
    end
    methods
        
        function confuseMat = train(obj)
            
            printStatus(obj);
            td = obj.TrainingData;
            
            [predictors, response] = getDataTable(td);
            
            if isempty(response)
                fprintf('[%s] No Training Data\n',mfilename);
                obj.Wg = [];
                obj.Cg = [];
                obj.IsTrained = false;
                return
            end
            classlabels=obj.getClassNames;
            predictorsNum=obj.cellArrayToNum(response,classlabels);

%             numClasses = length(unique(response)); % ?
            numClasses = length(classlabels);
%             sizeTrain = size(predictorsNum',2); 
            sizeTrain = length(predictorsNum); % check this still works with Myo data 7/11/2018
            targets = eye(numClasses);
            hiddenLayerSize = 15;

            trainTargets = zeros(numClasses,sizeTrain);
            for i = 1:sizeTrain     
                trainTargets(:,i) = targets(predictorsNum(i),:)';
            end
            net = patternnet(hiddenLayerSize); 
            
            net.divideParam.trainRatio = .70; % training set [%]
            net.divideParam.valRatio = .15; % validation set [%]
            net.divideParam.testRatio = .15; % test set [%]
            net.trainParam.showWindow = 0;

            tic
            fprintf('Training %s...',mfilename);

            [net,~,~,~] = train(net,predictors',trainTargets);
            trainedClassifier = net;
            
            fprintf('Done. T = %f s\n',toc)
            
            % Predict original model
            pmat = net(predictors');
            [~,p] = max(pmat); % predicted class
            confuseMat = confusionmat({classlabels{p}},response,'order',td.ClassNames); % hack
            
            obj.IsTrained = true;
            obj.Wg = trainedClassifier;
        end
        function [classOut, voteDecision] = classify(obj,featuresColumns)
%             assert(size(featuresColumns,1) == obj.NumActiveChannels*obj.NumFeatures,...
%                 'Expected first dimension of featuredata [%d]to be equal to numActiveChannels*numFeatures [%d]',...
%                 size(featuresColumns,1),obj.NumActiveChannels*obj.NumFeatures);
            if isempty(obj.Wg)
                warning('Classifier not trained');
                [classOut, voteDecision] = deal(length(obj.getClassNames));
                return
            end
            
            net = obj.Wg;
            predictors = featuresColumns';
            pmat = net(predictors');
            [~,numConverted] = max(pmat); % predicted class is in numbers, no need to convert class labels to indices
                        
            classOut = numConverted;
            voteDecision = numConverted;
        end
        function close(obj)
        end
    end
end