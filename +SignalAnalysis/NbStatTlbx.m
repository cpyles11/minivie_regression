classdef NbStatTlbx < SignalAnalysis.Classifier
    % NbStatTlbx < SignalAnalysis.Classifier
    % Class file for interfacing MATLAB Signal Processing machine Learning
    % Algorithms within MiniVIE
    %
    % 28-June-2018 Nguyen: Created
    properties
        % Note store model in Wg
    end
    methods
        
        function confuseMat = train(obj)
            
            printStatus(obj);
            td = obj.TrainingData;
            
            [predictors, response] = getDataTable(td);
            
            if isempty(response)
                fprintf('[%s] No Training Data\n',mfilename);
                obj.Wg = [];
                obj.Cg = [];
                obj.IsTrained = false;
                return
            end
            
            tic
            fprintf('Training %s...',mfilename);
            
            trainedClassifier = fitcnb(predictors, response,'Distribution','kernel','ClassNames',td.ClassNames); % mvmn or kernel(default to normal), kernel seems to be more "reasonable" per crossval
%             trainedClassifier = fitcnb(predictors, response,'Distribution','kernel','Kernel','triangle'); % kernel opts include 'normal','epanechnikov','box','triangle'
            
            fprintf('Done. T = %f s\n',toc)
            
            % Predict original model
            p = predict(trainedClassifier,predictors);
            
            confuseMat = confusionmat(p,response,'order',td.ClassNames);
            
            obj.IsTrained = true;
            obj.Wg = trainedClassifier;
        end
        function [classOut, voteDecision] = classify(obj,featuresColumns)
% %             assert(size(featuresColumns,1) == obj.NumActiveChannels*obj.NumFeatures,...
%             assert(size(featuresColumns,1) == obj.NumActiveChannels*length(obj.TrainingData.ActiveFeatures),... % hack
%                 'Expected first dimension of featuredata [%d]to be equal to numActiveChannels*numFeatures [%d]',...
%                 size(featuresColumns,1),obj.NumActiveChannels*length(obj.TrainingData.ActiveFeatures));
% %                 size(featuresColumns,1),obj.NumActiveChannels*obj.NumFeatures);
            if isempty(obj.Wg)
                warning('Classifier not trained');
                [classOut, voteDecision] = deal(length(obj.getClassNames));
                return
            end
            
            trainedClassifier = obj.Wg;
            predictors = featuresColumns';
            p = predict(trainedClassifier,predictors);
            
            numConverted = obj.cellArrayToNum(p,obj.getClassNames);
            
            classOut = numConverted;
            voteDecision = numConverted;
        end
        function close(obj)
        end
    end
end
