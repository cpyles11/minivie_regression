classdef SRC < SignalAnalysis.Classifier
    % SRC < SignalAnalysis.Classifier
    %
    % 08-June-2018 Nguyen: Created
% %     TODO: make SRC robust for strings or numbers as labels
% %         Exploit OOP for SRC parameters? Sparsity S, Max Iter MI, l1 alg
% %         Add SRC functions as part of classdef rather than private fn
    properties
        % Note store model in Wg -> there is no explicit model here! In
        % SRC, we just attempt to solve y=Ax s.t. x has a few nonzero
        % entries, expressing y as a linear combination of redundant vecs
        % There are several solvers available for the sparse recovery part.
        % OMP is fastest, with SP and CoSaMP available. A\y and l1 homotopy
        % also work in the SRC function
    end
    methods
        
        function confuseMat = train(obj)
            
            printStatus(obj);
            td = obj.TrainingData;
            
            [predictors, response] = getDataTable(td);
            
            if isempty(response)
                fprintf('[%s] No Training Data\n',mfilename);
                obj.Wg = [];
                obj.Cg = [];
                obj.IsTrained = false;
                return
            end
            
            fprintf('Initializing %s...',mfilename);
            
            % SRC parameters
            S=1; % S=1 perfect recovery on own data...
            MI=2*S; % do this for OMP only
%             MI=10; % do this for OMP only
            p = SRC_MiniVIE(predictors',predictors',response,S,MI);
%             p = SRC_MiniVIE(predictors',predictors',response,S,MI,'Homotopy');
            
            confuseMat = confusionmat(p,response,'order',td.ClassNames);
            
%             obj.IsTrained = true;
%             obj.Wg = trainedClassifier;
            obj.IsTrained = true;
            obj.Wg(1)=S;
            obj.Wg(2)=MI;
        end
        function [classOut, voteDecision,residual] = classify(obj,featuresColumns)
% %             assert(size(featuresColumns,1) == obj.NumActiveChannels*obj.NumFeatures,...
%             assert(size(featuresColumns,1) == obj.NumActiveChannels*length(obj.TrainingData.ActiveFeatures),... % hack
%                 'Expected first dimension of featuredata [%d]to be equal to numActiveChannels*numFeatures [%d]',...
%                 size(featuresColumns,1),obj.NumActiveChannels*length(obj.TrainingData.ActiveFeatures));%                 size(featuresColumns,1),obj.NumActiveChannels*obj.NumFeatures);
            if isempty(obj.Wg)
                warning('Classifier not trained');
                [classOut, voteDecision] = deal(length(obj.getClassNames));
                return
            end
            
%             trainedClassifier = obj.Wg;
%             predictors = featuresColumns';

            td = obj.TrainingData;
            
            [predictors, response] = getDataTable(td);

            S=obj.Wg(1);
            MI=obj.Wg(2); % do this for OMP only
            [p,residual] = SRC_MiniVIE(predictors',featuresColumns,response,S,MI);
%             p = SRC_MiniVIE(predictors',featuresColumns,response,S,MI,'Homotopy');
%             p = predict(trainedClassifier,predictors);
            
            numConverted = obj.cellArrayToNum(p,obj.getClassNames);
            
            classOut = numConverted;
            voteDecision = numConverted;
        end
        function close(obj)
        end
    end
end
