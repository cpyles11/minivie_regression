function [labels] = label_flexion_extension(time, angle)
    
    % Plot the data
    f = figure();
    ax = gca();
    hL = line(time, angle);
    hXLabel = xlabel('Time (s)');
    hYLabel = ylabel('Elbow Angle (degrees)');
    set(hL, 'LineWidth',2);
    set(ax, 'FontName','Helvetica');
    set([hXLabel, hYLabel], 'FontName', 'AvanteGarde');
    set([ax], 'FontSize', 12);
    set(ax,...
        'Box', 'off',...
        'TickDir', 'out',...
        'TickLength', [0.02,0.02],...
        'XMinorTick','on',...
        'YMinorTick', 'on',...
        'YGrid','on',...
        'XColor'      , [.3 .3 .3], ...
        'YColor'      , [.3 .3 .3], ...
        'LineWidth'   , 1         );
    set(f,'Color',[1 1 1], 'Position',[277.0000  121.0000  902.4000  608.0000]);
    set(f, 'PaperPositionMode', 'auto');

    % Ask user to select points
    fprintf('SELECT THE START OF EACH FLEXION TRIAL, THEN PRESS ENTER\n');
    [x,y] = ginput;
    idx_flexion_start = zeros(length(x),1);
    for i = 1:length(x)
        [trash, idx_flexion_start(i)] = min(hypot(time - x(i), angle - y(i)));
    end
    idx_flexion_start = sort(idx_flexion_start);
    
    fprintf('SELECT THE END OF EACH FLEXION TRIAL, THEN PRESS ENTER\n');
    [x,y] = ginput;
    idx_flexion_end = zeros(length(x),1);
    for i = 1:length(x)
        [trash, idx_flexion_end(i)] = min(hypot(time - x(i), angle - y(i)));
    end
    idx_flexion_end = sort(idx_flexion_end);
    
    fprintf('SELECT THE START OF EACH EXTENSION TRIAL, THEN PRESS ENTER\n');
    [x,y] = ginput;
    idx_extension_start = zeros(length(x),1);
    for i = 1:length(x)
        [trash, idx_extension_start(i)] = min(hypot(time - x(i), angle - y(i)));
    end
    idx_extension_start = sort(idx_extension_start);
    
    fprintf('SELECT THE END OF EACH EXTENSION TRIAL, THEN PRESS ENTER\n');
    [x,y] = ginput;
    idx_extension_end = zeros(length(x),1);
    for i = 1:length(x)
        [trash, idx_extension_end(i)] = min(hypot(time - x(i), angle - y(i)));
    end
    idx_extension_end = sort(idx_extension_end);
    
    assert(length(idx_flexion_start)==length(idx_flexion_end));
    assert(length(idx_extension_start)==length(idx_extension_end));
    
    % Create labeling intervals
    labels = zeros(length(time), 1); % 0 is for no movement
    for i = 1:length(idx_flexion_start)
        labels(idx_flexion_start(i):idx_flexion_end(i)) = 1; % Flexion
    end
    for i = 1:length(idx_extension_start)
        labels(idx_extension_start(i):idx_extension_end(i)) = 2; % Extension
    end
    labels(1:idx_flexion_start(1)-1) = nan;
    labels(idx_flexion_end(end)+1:end) = nan;
    
    % Find transition points
    transitions = diff(labels);
    idxTransition = find(transitions>0);
    
    % Number of transitions should be equal to number of intervals - 1
    assert(length(idxTransition)==(length(idx_flexion_start) + length(idx_extension_start) - 1),'Warning, wrong number of transitions');
 
    % Add shading to plot
    hold on
    for i = 1:length(idx_flexion_start)
        pFlex = patch( [time(idx_flexion_start(i)), time(idx_flexion_end(i)), time(idx_flexion_end(i)), time(idx_flexion_start(i))],...
            [ax.YLim(1), ax.YLim(1), ax.YLim(2), ax.YLim(2)], 'b');
        pFlex.LineStyle = 'none';
        pFlex.FaceAlpha = '0.3';
        %text(time(idx_flexion_start(i)), ax.YLim(2)-3, 'Flexion', 'FontSize', 10, 'FontName', 'AvanteGarde');
    end
    for i = 1:length(idx_extension_start)
        pExt = patch( [time(idx_extension_start(i)), time(idx_extension_end(i)), time(idx_extension_end(i)), time(idx_extension_start(i))],...
            [ax.YLim(1), ax.YLim(1), ax.YLim(2), ax.YLim(2)], 'g');
        pExt.LineStyle = 'none';
        pExt.FaceAlpha = '0.3';
        %text(time(idx_extension_start(i)), ax.YLim(2)-3, 'Extension', 'FontSize', 10, 'FontName', 'AvanteGarde');
    end
    hold off
    uistack(hL,'top');
    hLegend = legend([hL, pFlex, pExt], 'Encoder Data', 'Flexion', 'Extension');
    set(hLegend, 'FontName', 'AvanteGarde', 'FontSize', 10);
    
    good = input('Is labeling good? (0 for no, 1 for yes): ');
    if ~good
        [labels] = label_flexion_extension(time, angle);
    end
end
