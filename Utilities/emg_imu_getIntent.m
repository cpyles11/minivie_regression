function [classDecision,voteDecision,className,prSpeed,rawEmgSignals, ...
    filteredEmgSignals,rawImuSignals,filteredImuSignals,features2D, featuresIMU] ...
    = emg_imu_getIntent(hEmgSignalSource,hImuSignalSource,hSignalClassifier)
% Given an emg signal source, an imu signal source and a classifier, derive
% intent based on the current signal state

% Initialize output
[classDecision,voteDecision,className,prSpeed] = deal([]);

% Verify inputs
if isempty(hEmgSignalSource)
    disp('No Emg Signal Source');
    return
elseif isempty(hImuSignalSource)
    disp('No Imu Signal Source');
    return
elseif isempty(hSignalClassifier)
    disp('No Signal Classifier');
    return    
end

% Get the appropriate number of samples
hEmgSignalSource.NumSamples = hSignalClassifier.NumSamplesPerWindow;
hImuSignalSource.NumSamples = hSignalClassifier.NumSamplesPerWindowIMU;

% pad the signal to avoid filter artifact.  this should be a non-issue
% with the filter save state
numSamples = hEmgSignalSource.NumSamples;
numPad = numSamples;
rawEmgSignals = hEmgSignalSource.getData(numSamples+numPad);
filteredEmgSignals = hEmgSignalSource.applyAllFilters(rawEmgSignals);
filteredEmgSignals = filteredEmgSignals(end-numSamples+1:end,:);
rawEmgSignals = rawEmgSignals(end-numSamples+1:end,:);

% Extract features
features2D = hSignalClassifier.extractfeatures(filteredEmgSignals);
activeChannelFeatures = features2D(hSignalClassifier.getActiveChannels,:);

% pad the signal to avoid filter artifact.  this should be a non-issue
% with the filter save state
numSamples = hImuSignalSource.NumSamples;
numPad = numSamples;
rawImuSignals = hImuSignalSource.getData(numSamples+numPad);
filteredImuSignals = rawImuSignals;
filteredImuSignals = filteredImuSignals(end-numSamples+1:end,:);
rawImuSignals = rawImuSignals(end-numSamples+1:end,:);
% separate feature extracts for each one? 

% Extract imu features
featuresIMU = hSignalClassifier.extractimufeatures(filteredImuSignals);
activeIMUChannelFeatures = featuresIMU(hSignalClassifier.getActiveIMUChannels,:);

% put entire thing into classifier
combinedfeatures = [reshape(activeChannelFeatures',[],1); reshape(activeIMUChannelFeatures',[],1)];
[classDecision, voteDecision] = hSignalClassifier.classify(combinedfeatures);

if hSignalClassifier.NumMajorityVotes > 1
    cursorMoveClass = voteDecision;
else
    cursorMoveClass = classDecision;
end

virtualChannels = hSignalClassifier.virtual_channels(features2D,cursorMoveClass);   %multi input change?
prSpeed = max(virtualChannels);

% fprintf('Class=%2d; Vote=%2d; Class = %16s; S=%6.4f',...
%     classOut,voteDecision,hSignalClassifier.ClassNames{cursorMoveClass},prSpeed);
classNames = hSignalClassifier.getClassNames;
className = classNames{cursorMoveClass};
end

