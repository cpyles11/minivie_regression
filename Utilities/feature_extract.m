function features = feature_extract(windowData, windowSize, zc_thresh, ssc_thresh ,Fs, feature_list)
% based on code by RSA

if nargin < 6
    feature_list = {'mav', 'len', 'zc', 'ssc'};
end
if nargin < 5
    Fs = 1000;
end
if nargin < 4
    ssc_thresh = 0.15;
end
if nargin < 3
    zc_thresh = 0.15;
end

[nChannels, nSamples] = size(windowData);
windowSize = max(windowSize,10);
windowSize = min(windowSize,nSamples);
n = windowSize; %  Normalize features so they are independant of the window size
idStart = (1+nSamples-windowSize);
y = windowData(1:nChannels,idStart:end)';
features = [];


% time domain features

%  MAV shouldn't be normalized
if ismember('mav', feature_list)
    MAV = mean( abs( y ) );
    features = [features; MAV];
end

%  VAR
if ismember('var', feature_list)
    VAR = std(y,[],1).^2;
    features = [features; VAR];
end

%  Curve length is the sum of the absolute value of the derivative of the
%  signal, normalized by the sample rate
if ismember('len', feature_list)
    LEN = sum( abs( diff(y) ) ) * Fs / n;
    features = [features; LEN];
end

% Root Mean Square (RMS) or Integrated EMG
if ismember('rms', feature_list)
    RMS = sqrt(sum(y.^2,1)/n);
    features = [features; RMS];
end

%  Zero Crossing (zc)
%  if zeroCross and overThreshold: count a zero cross
% Value to compute 'zero-crossing' around
if ismember('zc', feature_list)
    t = 0.0;
    ZC = sum(...
        ((y(1:n-1,:) - t > 0) & (y(2:n,:) - t < 0) | ...
        (y(1:n-1,:) - t < 0) & (y(2:n,:) - t > 0)) & ...
        (abs(y(1:n-1,:) - t - y(2:n,:) - t) > zc_thresh) ...
        ) * Fs / n;
    features = [features; ZC];
end

%  Slope Sign Change (ssc)
%  if signChange and overThreshold: count a slope change
if ismember('ssc', feature_list)
    SSC = sum( ...
        ((y(2:n-1,:) > y(1:n-2,:)) & (y(2:n-1,:) > y(3:n,:)) |  ...
        (y(2:n-1,:) < y(1:n-2,:)) & (y(2:n-1,:) < y(3:n,:))) &  ...
        ((abs(y(2:n-1,:) - y(3:n,:)) > ssc_thresh) | (abs(y(2:n-1,:) - y(1:n-2,:)) > ssc_thresh))...
        ) * Fs / n;
    features = [features; SSC];
end


% frequency domain features

% Estimate Power Spectrum Density
% https://www.mathworks.com/help/signal/ug/power-spectral-density-estimates-using-fft.html
if sum(ismember('mp', feature_list) || ismember('mf', feature_list))
    n = windowSize;
    xdft = fft(y);
    xdft = xdft(1:n/2+1);
    psdx = (1/(Fs*n)) * abs(xdft).^2;
    psdx(2:end-1) = 2*psdx(2:end-1);
    freq = (0:Fs/n:Fs/2).';
end

% % mean frequency
% https://www.sciencedirect.com/science/article/pii/S0957417412001200
if ismember('mf', feature_list)
    MF = sum(psdx.*freq)/sum(psdx);
    features = [features; MF];
end

% % median power
% https://www.sciencedirect.com/science/article/pii/S0957417412001200
if ismember('mp', feature_list)
    MP = 0.5 * sum(psdx);
    features = [features; MP];
end
end
