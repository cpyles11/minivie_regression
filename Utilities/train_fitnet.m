function [net, r2, performance, tTest,yTest] = train_fitnet(features, targets)
% Solve an Input-Output Fitting problem with a Neural Network
% Connor Pyles
%
%   features - input data.
%   targets - target data.

x = features;
t = targets;
% Choose a Training Function
% For a list of all training functions type: help nntrain
% 'trainlm' is usually fastest.
% 'trainbr' takes longer but may be better for challenging problems.
% 'trainscg' uses less memory. Suitable in low memory situations.
trainFcn = 'trainlm';  % Levenberg-Marquardt backpropagation.

% Create a Fitting Network
hiddenLayerSize = 10;
net = fitnet(hiddenLayerSize,trainFcn);

% Setup Division of Data for Training, Validation, Testing
net.divideParam.trainRatio = 70/100;
net.divideParam.valRatio = 10/100;
net.divideParam.testRatio = 20/100;

% Train the Network
[net,tr, data] = train_custom(net,x,t); % Note that this is an updated version of train that includes more output data

% Get testing set
xTest = data.X{1}(:,data.test.indices);
tTest = data.T{1}(1, data.test.indices);

% Test the Network
yTest = net(xTest);
e = gsubtract(tTest,yTest);
performance = perform(net,tTest,yTest);
r = corrcoef(tTest,yTest);
r = r(1,2);
r2 = r*r;


%figure, plotregression(tTest,yTest);

% Test the Network
% y = net(x);
% e = gsubtract(t,y);
% performance = perform(net,t,y);

% View the Network
% view(net)

% Plots
% Uncomment these lines to enable various plots.
%figure, plotperform(tr)
%figure, plottrainstate(tr)
%figure, ploterrhist(e)
%figure, plotregression(t,y)
%figure, plotfit(net,x,t)
end
