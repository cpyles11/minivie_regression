% Adam Polevoy 2018
% Extracts the features from the emg signals
% Inputs:
%   data: 2D matrix, number of channels by number of samples (mV)
%   fs: sampling frequency of emg data
%   window_time_size: size of emg windows (seconds)
%   window_time_slide: amount that window moves by (seconds)
%   zc_thresh: threshold for zc features (mV)
%   ssc_thresh: threshold for ssc features(mV)
%   feature_list: 1xN string cell array, contains features to extract
%       'mav', 'len', 'zc', 'ssc', 'var', 'rms', 'mf', 'mp'
function [features, time_window_labels, time] = feature_extract_slide(emg_data, labels, fs, window_time_size, window_time_slide, zc_thresh, ssc_thresh, feature_list)

if nargin < 8
    feature_list = {'mav', 'len', 'zc', 'ssc'};
end

% % filter the data
% filter_order = 4;
% bandpass_low = 15;
% bandpass_high = 450;
% 
% %% TODO: align this with MiniVIE filtering in utilities, getIntentNew.m
% filtered_data = bandpass_filter(emg_data, filter_order, bandpass_low, bandpass_high, fs);

lowPassFilter = Inputs.LowPass(450,8,fs);
highPassFilter = Inputs.HighPass(15,8,fs);
filtered_data = emg_data';
filtered_data = lowPassFilter.apply(filtered_data);
filtered_data = highPassFilter.apply(filtered_data);
filtered_data = filtered_data';

% PLOT
f = figure();
all_time = [1:1:length(filtered_data)] ./ fs;
hold on
plot(all_time, filtered_data(1,:));
plot(all_time, filtered_data(2,:));
plot(all_time, filtered_data(3,:));
hold off
xlabel('Time (s)');
ylabel('Voltage');
legend('Ch0','Ch3','Ch5');

% how many features are being extracted
num_features = length(feature_list);

window_size = round(window_time_size * fs); %converts size from time to samples
slide_size = round(window_time_slide * fs); %converts slide from time to samples
data_size = size(filtered_data, 2); %size of the data set
output_size = round((data_size-window_size)/slide_size); %size of the outputs
features = zeros(output_size, num_features*size(filtered_data,1));
time_window_labels = [];

% for each channel
for j = 1:size(filtered_data,1)
    % slide the window
    for a = 1:slide_size:data_size-window_size
        % NOTE: Index used is (i+slide_size-1)/slide_size. This will convert
        %   from i, the index that the sliding window is at in the emg signal
        %    to k, the index of the output.
        i = floor(a);
        k = floor((a+slide_size-1)/slide_size);
        windowData = filtered_data(j,i:(i+window_size-1));
        
        % f = figure();
        % plot([1:1:length(windowData)]./fs, windowData);
        
        feats = feature_extract(windowData,window_size,zc_thresh,ssc_thresh,fs);
        features(k,(j-1)*num_features+1:(j)*num_features) = feats;
        
        label_inds = k*slide_size+1:(k*slide_size+window_size-1);
        if label_inds(end) > length(labels)
            label_inds = label_inds(label_inds < length(labels));
        end
        time_window_labels(k) = mode(labels(label_inds));
    end
end

time = [1:1:size(features,1)] *window_time_slide;

% PLOT
f = figure();
title('MAV');
all_time = [1:1:size(features,1)] *window_time_slide;
hold on
plot(all_time, features(:,1));
plot(all_time, features(:,5));
plot(all_time, features(:,9));
hold off
xlabel('Time (s)');
ylabel('Voltage');
legend('Ch0','Ch3','Ch5');

f = figure();
title('LEN');
all_time = [1:1:size(features,1)] *window_time_slide;
hold on
plot(all_time, features(:,2));
plot(all_time, features(:,6));
plot(all_time, features(:,10));
hold off
xlabel('Time (s)');
ylabel('Voltage');
legend('Ch0','Ch3','Ch5');

f = figure();
title('ZC');
all_time = [1:1:size(features,1)] *window_time_slide;
hold on
plot(all_time, features(:,3));
plot(all_time, features(:,7));
plot(all_time, features(:,11));
hold off
xlabel('Time (s)');
ylabel('Voltage');
legend('Ch0','Ch3','Ch5');

f = figure();
title('SSC');
all_time = [1:1:size(features,1)] *window_time_slide;
hold on
plot(all_time, features(:,4));
plot(all_time, features(:,8));
plot(all_time, features(:,12));
hold off
xlabel('Time (s)');
ylabel('Voltage');
legend('Ch0','Ch3','Ch5');

end
 
% Filters signal using a butterworth bandpass filter
function filtered = bandpass_filter(data, order, high_cutoff, low_cutoff, fs)
cutoffs = [high_cutoff low_cutoff] * (2/fs);
[b,a] = butter(order,cutoffs, 'Bandpass');
filtered = zeros(size(data,1), size(data,2));
for i=1:size(data,1)
    filtered(i,:) = filter(b,a,data(i,:));
end
end
