function [classDecision,voteDecision,className,prSpeed,rawSignals,filteredSignals,features2D] ...
    = getIntentNew( hEMGSignalSource, hEncoderSignalSource, hSignalClassifier)
% Given a signal source and a classifier, derive intent based on the
% current signal state

% Initialize output
[classDecision,voteDecision,className,prSpeed] = deal([]);

% Verify inputs
if isempty(hEMGSignalSource)
    disp('No Signal Source');
    return
elseif isempty(hSignalClassifier)
    disp('No Signal Classifier');
    return    
end


%%%%% DEBUGGING PLOTS MAKE SURE TO UNCOMMENT

% hEMGSignalSource.NumSamples = 5000;
% rawSignals = hEMGSignalSource.getData(5000);
% filteredSignals = hEMGSignalSource.applyAllFilters(rawSignals);
% 
% f = figure();
% hold on
% all_time = [1:1:size(rawSignals,1)]./hEMGSignalSource.SampleFrequency;
% activeChannels = hSignalClassifier.getActiveChannels;
% plot(all_time, filteredSignals(:,activeChannels(1)));
% plot(all_time, filteredSignals(:,activeChannels(2)));
% plot(all_time, filteredSignals(:,activeChannels(3)));
% hold off
% xlabel('Time (s)');
% ylabel('Voltage');
% legend('Ch0','Ch3','Ch5');

% f = figure();
% hold on
% all_time = [1:1:size(rawSignals,1)]./hEMGSignalSource.SampleFrequency;
% activeChannels = hSignalClassifier.getActiveChannels;
% plot(all_time, rawSignals(:,activeChannels(1)));
% plot(all_time, rawSignals(:,activeChannels(2)));
% plot(all_time, rawSignals(:,activeChannels(3)));
% hold off
% xlabel('Time (s)');
% ylabel('Voltage');
% legend('Ch0','Ch3','Ch5');

%%%%%

% Get the appropriate number of samples
numSamples = round(hEMGSignalSource.SampleFrequency * hSignalClassifier.WindowTimeSize);
hEMGSignalSource.NumSamples = numSamples;

% pad the signal to avoid filter artifact.  this should be a non-issue
% with the filter save state
numPad = 250;
rawSignals = hEMGSignalSource.getData(numSamples+numPad);
filteredSignals = hEMGSignalSource.applyAllFilters(rawSignals);
filteredSignals = filteredSignals(end-numSamples+1:end,:);
rawSignals = rawSignals(end-numSamples+1:end,:);

% Extract features and classify
features2D = hSignalClassifier.extractfeatures(filteredSignals, hEMGSignalSource.SampleFrequency);
features2D = features2D';
activeChannelFeatures = features2D(hSignalClassifier.getActiveChannels,:);
features1D = activeChannelFeatures';
features1D = features1D(:)';

if hSignalClassifier.UseEncoder
    position = hEncoderSignalSource.getData();
    features1D = [features1D,position];
end

%%%%%%%%%%%%%
% WRITE OUT FEATURES FOR DEBUGGING
%fid = fopen('features.txt','w');


%%%%%%%%%%%

%f = figure()
%plot([1:1:numSamples] /hEMGSignalSource.SampleFrequency, filteredSignals(:,1));

%% TODO: Determine how to properly concatenate position and feature data
[classDecision, voteDecision, velocityOut] = hSignalClassifier.classify(features1D);
fprintf(strcat('Class: ', num2str(classDecision),'\t|\t','Velocity: ',num2str(velocityOut), ' deg/s?\r\n'));


if hSignalClassifier.NumMajorityVotes > 1
    cursorMoveClass = voteDecision;
else
    cursorMoveClass = classDecision;
end
if cursorMoveClass == 0
    cursorMoveClass = 1; % no movement
end

% virtualChannels = hSignalClassifier.virtual_channels(features2D,cursorMoveClass);
% prSpeed = max(virtualChannels);

prSpeed = velocityOut;

% fprintf('Class=%2d; Vote=%2d; Class = %16s; S=%6.4f',...
%     classOut,voteDecision,hSignalClassifier.ClassNames{cursorMoveClass},prSpeed);

%% TODO: get correct class names, should align with cases in ScenarioBase.generateUpperArmCommand
classNames = hSignalClassifier.getClassNames;
className = classNames{cursorMoveClass};
