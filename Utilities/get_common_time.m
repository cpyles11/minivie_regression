function tCommon = get_common_time(t1,t2, sampleRateCommon)
    
    if nargin < 3
        % Choose faster rate for common baseline:
        sampleRate1 = 1/min(diff(t1));
        sampleRate2 = 1/min(diff(t2));
        sampleRateCommon = max(sampleRate1,sampleRate2);
    end
    
    % Get common time range
    tRange1 = [min(t1) max(t1)];
    tRange2 = [min(t2) max(t2)];
    tStartCommon = max(tRange1(1),tRange2(1));
    tEndCommon   = min(tRange1(2),tRange2(2));

    if tEndCommon < tStartCommon
        error("Non overlapping time range: tRange1 = [" + ...
            tRange1(1) + " " + tRange1(2) + ...
            "], tRange2 = [" + tRange2(1) + " " + tRange2(2) + "]")
    end

    % New ideal time basis:
    tCommon = (tStartCommon:(1/sampleRateCommon):tEndCommon)';

%     % Final common time basis
%     tCommon1 = t1(tStartCommon <= t1 & t1 <= tEndCommon);
%     tCommon2 = t2(tStartCommon <= t2 & t2 <= tEndCommon);
% 
%     mustResample1 = false;
%     mustResample2 = false;
% 
%     % check that time basis matches this:
%     if isequal(size(tCommon1),size(tCommon))
%         isBad = any( abs(tCommon1 - tCommon) > 0.1/sampleRate1 );
%         if isBad
%             mustResample1 = true;
%         end
%     else
%         % size mismatch A
%         mustResample1 = true;
%     end
%     if isequal(size(tCommon2),size(tCommon))
%         isBad = any( abs(tCommon2 - tCommon) > 0.1/sampleRate2 );
%         if isBad
%             mustResample2 = true;
%         end
%     else
%         % size mismatch B
%         mustResample2 = true;
%     end
% 
%     % merge signals over common time:
%     % use time index to locate the common signal range:
%     isCommonA = (tStartCommon <= t1 & t1 <= tEndCommon);
%     isCommonB = (tStartCommon <= t2 & t2 <= tEndCommon);
% 
%     sA = objA.Signals(isCommonA,:);
%     sB = objB.Signals(isCommonB,:);
% 
%     if mustResample1
%         warning('New Data set timebase is not exact match of existing timebase.  Resampling implemented');
%         sAResamp = interp1(t1(isCommonA),sA,tCommon,'linear','extrap');
%     else
%         sAResamp = sA;
%     end
%     if mustResample2
%         warning('New Data set timebase is not exact match of existing timebase.  Resampling implemented');
%         sBResamp = interp1(t2(isCommonB),sB,tCommon,'linear','extrap');
%     else
%         sBResamp = sB;
%     end
end