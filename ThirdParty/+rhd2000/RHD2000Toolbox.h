//----------------------------------------------------------------------------------
// Intan Technologies RHD2000 RHD2000_Automation API
//
// Copyright (c) 2014-2018 Intan Technologies LLC.  All rights reserved.
//
// See http://www.intantech.com for documentation and product information.
//----------------------------------------------------------------------------------


#pragma once

typedef unsigned short API_BOOL;
typedef unsigned int BOARD_DATA_SOURCE;
typedef unsigned int DATABLOCKREF;
typedef unsigned int BOARDREF;
typedef unsigned int SAVEDATATYPE;

#ifdef __cplusplus
extern "C" {
#endif
#pragma region Initialize
    // Initialize
    __declspec(dllexport) long __cdecl Open(const char* serialNumber, BOARDREF* boardRef);
#pragma endregion Initialize
    
#pragma region Close
    // Close
    __declspec(dllexport) long __cdecl Close(BOARDREF boardRef);
#pragma endregion Close

#pragma region Configuration
    // Configuration
    __declspec(dllexport) long __cdecl Configure16DigitalOutputs(BOARDREF boardRef);
    __declspec(dllexport) long __cdecl Configure8DigitalOutputs8Comparators(BOARDREF boardRef, double* thresholdVoltages, API_BOOL* risingEdges);
    __declspec(dllexport) long __cdecl GetDigitalOutputConfiguration(BOARDREF boardRef, API_BOOL* comparatorsEnabled, double* thresholdVoltages, API_BOOL* risingEdges);

    __declspec(dllexport) long __cdecl ConfigureAnalogOutputCommon(BOARDREF boardRef, API_BOOL dspSettle, API_BOOL highpassFilterEnabled, double highpassFilterFrequency, double noiseSuppress, unsigned int dacGain);
    __declspec(dllexport) long __cdecl GetAnalogOutputCommon(BOARDREF boardRef, API_BOOL* dspSettle, API_BOOL* highpassFilterEnabled, double* highpassFilterFrequency, double* noiseSuppress, unsigned int* dacGain);
    __declspec(dllexport) long __cdecl SetDACManual(BOARDREF boardRef, double dacManualVoltage);
    __declspec(dllexport) long __cdecl ConfigureAnalogOutputSource(BOARDREF boardRef, unsigned int dac, API_BOOL enabled, unsigned int datasource, unsigned int channel);
    __declspec(dllexport) long __cdecl GetAnalogOutputSource(BOARDREF boardRef, unsigned int dac, API_BOOL* enabled, unsigned int* datasource, unsigned int* channel);

    __declspec(dllexport) long __cdecl GetSamplingRate(BOARDREF boardRef, unsigned int* pSamplingRateEnum, double* pSamplingRate);
    __declspec(dllexport) long __cdecl SetSamplingRate(BOARDREF boardRef, unsigned int samplingRateEnum);

    __declspec(dllexport) long __cdecl GetBandwidthParameters(BOARDREF boardRef, double* desiredUpperBandwidth, double* actualUpperBandwidth, double* desiredLowerBandwidth, double* actualLowerBandwidth, double* desiredDspCutoffFreq, double* actualDspCutoffFreq, API_BOOL* dspEnabled);
    __declspec(dllexport) long __cdecl SetBandwidthParameters(BOARDREF boardRef, double desiredUpperBandwidth, double desiredLowerBandwidth, double desiredDspCutoffFreq, API_BOOL dspEnabled);

    __declspec(dllexport) long __cdecl GetFastSettle(BOARDREF boardRef, API_BOOL* enabled, API_BOOL* external, int* channel);
    __declspec(dllexport) long __cdecl SetFastSettle(BOARDREF boardRef, API_BOOL enabled, API_BOOL external, int channel);

    __declspec(dllexport) long __cdecl GetEvalBoardMode(BOARDREF boardRef, int* value);

    // Data streams
    __declspec(dllexport) long __cdecl ConfigureDataStreams(BOARDREF boardRef, API_BOOL* allowDataSource);

    // Functionality related to ports
    __declspec(dllexport) long __cdecl GetAuxDigitalOutputConfiguration(BOARDREF boardRef, unsigned short port, API_BOOL* enabled, int* channel);
    __declspec(dllexport) long __cdecl SetAuxDigitalOutputConfiguration(BOARDREF boardRef, unsigned short port, API_BOOL enabled, int channel);

    __declspec(dllexport) long __cdecl GetCableDelays(BOARDREF boardRef, unsigned short port, API_BOOL* manualDelayEnabled, int* manualDelay, double* lengthInMeters);
    __declspec(dllexport) long __cdecl SetCableDelays(BOARDREF boardRef, unsigned short port, API_BOOL manualDelayEnabled, int manualDelay, double lengthInMeters);


    __declspec(dllexport) long __cdecl GetNumBlocksToRead(BOARDREF boardRef, unsigned int* numBlocks);
    __declspec(dllexport) long __cdecl SetNumBlocksToRead(BOARDREF boardRef, unsigned int numBlocks);

    // General register settings
    __declspec(dllexport) long __cdecl GetPowerSettings(BOARDREF boardRef, API_BOOL* ampVRef, API_BOOL* adcComparator, API_BOOL* vddSense, API_BOOL* auxIn1, API_BOOL* auxIn2, API_BOOL* auxIn3, API_BOOL* amplifiers);
    __declspec(dllexport) long __cdecl SetPowerSettings(BOARDREF boardRef, API_BOOL ampVRef, API_BOOL adcComparator, API_BOOL vddSense, API_BOOL auxIn1, API_BOOL auxIn2, API_BOOL auxIn3, API_BOOL* amplifiers);

    __declspec(dllexport) long __cdecl GetChipNumberFormat(BOARDREF boardRef, API_BOOL* absMode, API_BOOL* twosComplement);
    __declspec(dllexport) long __cdecl SetChipNumberFormat(BOARDREF boardRef, API_BOOL absMode, API_BOOL twosComplement);

    __declspec(dllexport) long __cdecl GetWeakMISO(BOARDREF boardRef, API_BOOL* value);
    __declspec(dllexport) long __cdecl SetWeakMISO(BOARDREF boardRef, API_BOOL value);

    // Impedance & Plating
    __declspec(dllexport) long __cdecl GetImpedanceFreq(BOARDREF boardRef, double* desiredFrequency, double* actualFrequency);
    __declspec(dllexport) long __cdecl SetImpedanceFreq(BOARDREF boardRef, double desiredFrequency);
    __declspec(dllexport) long __cdecl BeginImpedanceMeasurement(BOARDREF boardRef, int *numBlocks);
    __declspec(dllexport) long __cdecl BeginPlating(BOARDREF boardRef, short channel);
    __declspec(dllexport) long __cdecl EndImpedanceMeasurement(BOARDREF boardRef);
    __declspec(dllexport) long __cdecl SetImpedanceParams(BOARDREF boardRef, unsigned short capValue, unsigned short channel, unsigned short polarity);


    // Impedance - waveform
    // Impedance - cap & channel enable
#pragma endregion Configuration

#pragma region Action_Status
    // Action/Status
    __declspec(dllexport) long __cdecl Stop(BOARDREF boardRef);
    __declspec(dllexport) long __cdecl RunFixed(BOARDREF boardRef, int numDatablocks);
    __declspec(dllexport) long __cdecl RunContinuously(BOARDREF boardRef);
    __declspec(dllexport) long __cdecl IsRunning(BOARDREF boardRef, API_BOOL* result);
    __declspec(dllexport) long __cdecl Flush(BOARDREF boardRef);
    __declspec(dllexport) long __cdecl ResetBoard(BOARDREF boardRef);
#pragma endregion Action_Status

#pragma region Input
    // Input
    __declspec(dllexport) long __cdecl ReadDigitalInputs(BOARDREF boardRef, int* values);

    __declspec(dllexport) long __cdecl ReadNextDataBlock(BOARDREF boardRef, DATABLOCKREF* datablockRef);
    __declspec(dllexport) long __cdecl FreeDataBlock(DATABLOCKREF datablockRef);
    __declspec(dllexport) long __cdecl GetLag(BOARDREF boardRef, double* lag);
    __declspec(dllexport) long __cdecl GetFIFOPercentageFull(BOARDREF boardRef, double* fifoPercentageFull);

    // Per-board
    __declspec(dllexport) long __cdecl GetDatablockTimestamps(DATABLOCKREF datablockRef, unsigned int* timestamps);
    __declspec(dllexport) long __cdecl GetDatablockADCs(DATABLOCKREF datablockRef, unsigned int index, double* adcs);
    __declspec(dllexport) long __cdecl GetDatablockAllADCsColumnMajor(DATABLOCKREF datablockRef, double* adcs);
    __declspec(dllexport) long __cdecl GetDatablockAllADCsRowMajor(DATABLOCKREF datablockRef, double* adcs);
    __declspec(dllexport) long __cdecl GetDatablockDigitalIn(DATABLOCKREF datablockRef, unsigned short* digitalIn);
    __declspec(dllexport) long __cdecl GetDatablockDigitalOut(DATABLOCKREF datablockRef, unsigned short* digitalOut);

    // Per-datasource
    __declspec(dllexport) long __cdecl GetDatablockAmplifierChannel(DATABLOCKREF datablockRef, BOARD_DATA_SOURCE datasource, unsigned int channel, double* amplifierData);
    __declspec(dllexport) long __cdecl GetDatablockAllAmplifierChannelsColumnMajor(DATABLOCKREF datablockRef, BOARD_DATA_SOURCE datasource, double* amplifierData);
    __declspec(dllexport) long __cdecl GetDatablockAllAmplifierChannelsRowMajor(DATABLOCKREF datablockRef, BOARD_DATA_SOURCE datasource, double* amplifierData);
    __declspec(dllexport) long __cdecl GetDatablockAuxInput(DATABLOCKREF datablockRef, BOARD_DATA_SOURCE datasource, unsigned int auxInputNum, double* valueArray);
    __declspec(dllexport) long __cdecl GetDatablockAllAuxInputsColumnMajor(DATABLOCKREF datablockRef, BOARD_DATA_SOURCE datasource, double* valueArray);
    __declspec(dllexport) long __cdecl GetDatablockAllAuxInputsRowMajor(DATABLOCKREF datablockRef, BOARD_DATA_SOURCE datasource, double* valueArray);
    __declspec(dllexport) long __cdecl GetDatablockTemperature(DATABLOCKREF datablockRef, BOARD_DATA_SOURCE datasource, double* temperature);
    __declspec(dllexport) long __cdecl GetDatablockSupplyVoltage(DATABLOCKREF datablockRef, BOARD_DATA_SOURCE datasource, double* voltage);
    __declspec(dllexport) long __cdecl GetDatablockROM(DATABLOCKREF datablockRef, BOARD_DATA_SOURCE datasource, char* company, char* chipName,
                                                       unsigned short* misoABMarker, unsigned short* dieRevision, unsigned short* unipolar, 
                                                       unsigned short* numAmplifiers, unsigned short* chipId);
#pragma endregion Input

#pragma region Output
    // Output
    __declspec(dllexport) long __cdecl SetLEDs(BOARDREF boardRef, API_BOOL* ledArray);
    __declspec(dllexport) long __cdecl Write16DigitalOutputs(BOARDREF boardRef, API_BOOL* outputs);
    __declspec(dllexport) long __cdecl Write8DigitalOutputs(BOARDREF boardRef, API_BOOL* outputs);
#pragma endregion Output

#pragma region Utility
    // Utility
    __declspec(dllexport) long __cdecl GetChipTypes(BOARDREF boardRef, API_BOOL forceScan, unsigned int* pVal);
    __declspec(dllexport) long __cdecl EstimateCableLengthsInMeters(BOARDREF boardRef, double *estimates);
    __declspec(dllexport) long __cdecl CalculateBestImpedance(BOARDREF boardRef, double *realIn, double *imaginaryIn, double *realOut, double *imaginaryOut);
    __declspec(dllexport) long __cdecl AmplitudeOfFrequencyComponents(BOARDREF boardRef, double *waveform, int length, double *realOut, double *imaginaryOut);
    __declspec(dllexport) long __cdecl DiscoverBoards1(int* numChars);
    __declspec(dllexport) long __cdecl DiscoverBoards2(char* serialNumberString, int numChars);

    __declspec(dllexport) long __cdecl RevisionQuery(int* major, int* minor, int* submajor);
    __declspec(dllexport) long __cdecl SelfTest(BOARDREF boardRef);
    __declspec(dllexport) long __cdecl Reset();
    __declspec(dllexport) long __cdecl MeasureAllImpedances(BOARDREF boardRef);
    __declspec(dllexport) long __cdecl MeasureOneImpedance(BOARDREF boardRef, BOARD_DATA_SOURCE datasource, unsigned int channel, double *magnitudeOut, double *phaseOut);
    __declspec(dllexport) long __cdecl GetImpedance(BOARDREF boardRef, BOARD_DATA_SOURCE datasource, unsigned int channel, double *magnitudeOut, double *phaseOut);

#pragma endregion Utility

#pragma region SaveFile
    __declspec(dllexport) long __cdecl SetSaveDataEnabled(BOARDREF boardRef, SAVEDATATYPE dataType, const char* channelName, API_BOOL enabled);
    __declspec(dllexport) long __cdecl RenameChannel(BOARDREF boardRef, SAVEDATATYPE dataType, const char* oldChannelName, const char* newChannelName);
    __declspec(dllexport) long __cdecl ConfigureSaveFile(BOARDREF boardRef, API_BOOL saveTemperatures, const char* note1, const char* note2, const char* note3);
    __declspec(dllexport) long __cdecl OpenSaveFile(BOARDREF boardRef, int format, const char* path); // also writes header
    __declspec(dllexport) long __cdecl WriteDatablockToSaveFile(DATABLOCKREF datablockRef, int timestampOffset);
    __declspec(dllexport) long __cdecl CloseSaveFile(BOARDREF boardRef);
    __declspec(dllexport) long __cdecl GetSaveFileNotes(BOARDREF boardRef, int allocatedLen, char* note1, char* note2, char* note3);
    __declspec(dllexport) long __cdecl GetNumSignalGroups(BOARDREF boardRef, int* numSignalGroups);
    __declspec(dllexport) long __cdecl GetSignalGroupInfo(BOARDREF boardRef, SAVEDATATYPE dataType, int allocatedLen, char* signalGroupName, int* numChannels);
    __declspec(dllexport) long __cdecl GetChannelInfo(BOARDREF boardRef, SAVEDATATYPE dataType, int signalChannel, int allocatedLen, char* channelName, API_BOOL* enabled);
#pragma endregion SaveFile

#pragma region Other
    //---------------------------------------------------------------------------------------------------------
    // Other functions

    __declspec(dllexport) long __cdecl ErrorMessage(long errorCode, unsigned int allocatedLen, char* out);
#pragma endregion Other

#ifdef __cplusplus
}
#endif
