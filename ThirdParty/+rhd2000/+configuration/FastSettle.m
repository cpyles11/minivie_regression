classdef FastSettle
    %FASTSETTLE Configure a chip's fast Settle functionality.
    %
    % All RHD2000 series chips have a hardware 'fast settle' function that 
    % rapidly resets the analog signal path of each amplifier channel to zero 
    % to prevent (or recover from) saturation caused by large transient input 
    % signals such as those due to nearby stimulation. Recovery from amplifier 
    % saturation can be slow when the lower bandwidth is set to a low frequency 
    % (e.g., 1 Hz).
    %
    % Fast settle can be configured in one of three ways:
    %      Disabled      No fast settling; normal amplifier operation. 
    %      Manual        Fast settling will begin immediately and will continue 
    %                    until manual mode is exited. 
    %      Real-time     Fast setting is linked to one of the digital inputs. 
    %                    Fast settling will occur whenever the selected digital 
    %                    input goes high and will continue until the selected 
    %                    digital input goes low.
    %
    % Fast settle can be configured any of these three ways using this
    % class.  For quicker response, 'manual' fast settle and 'disabled' can
    % be toggled using the rhd2000.Board's manual_fast_settle method.
    %
    % Note: Fast settle settings apply to all RHD2000 series chips attached 
    % to all ports.  
    %
    % Example:
    %       params = board.get_configuration_parameters();
    %
    %       % Configure real-time fast settle from digital input '0'
    %       params.Chip.FastSettle.Enabled = true;
    %       params.Chip.FastSettle.External = true;
    %       params.Chip.FastSettle.Channel = 0;
    %
    %       board.set_configuration_parameters(params);
    %
    % See also rhd2000.configuration.Configuration, rhd2000.Board.manual_fast_settle.
    
    properties
        %ENABLED Enables/disables fast settling
        %
        % Enabled = false => 'Disabled' mode
        % Enabled = true  => 'Manual' or 'Real-time' mode, depending
        %                    on the External setting.
        %
        % See also External.
        Enabled
        
        %EXTERNAL Enables/disables Real-time(AKA External) fast settle.
        %
        % Requires Enabled = true.
        %
        % External = false => 'Manual' mode.  Fast settling will begin 
        %                     immediately and will continue until manual 
        %                     mode is exited.
        % External = true  => 'Real-time' or 'External' mode. Fast setting 
        %                     is linked to one of the digital inputs on the
        %                     evaluation board.  Fast settling will occur 
        %                     whenever the selected digital input goes high 
        %                     and will continue until the selected digital 
        %                     input goes low.
        %
        %                     Which digital input is controlled by Channel.
        %
        % See also Enabled, Channel.
        External
        
        %CHANNEL Which digital input controls fast settling.
        %
        % Requires Enabled = true, External = true
        %
        % A logic high signal on the selected digital input will enable 
        % amplifier fast settling.
        %
        % Note that Channel is 0-indexed, i.e., valid values are 0-15.
        %
        % See also Enabled, External.
        Channel
    end
    
    methods
        function obj = FastSettle(board)
        %Call rhd2000.Board/get_configuration_parameters instead.
        %
        % See also rhd2000.Board.get_configuration_parameters, 
            if (nargin == 1)
                [retVal, enabled_int, external_int, obj.Channel] = ...
                    calllib('RHD2000m', 'GetFastSettle', board.handle, ...
                            0, 0, 0);

                rhd2000.report_error(retVal);

                obj.Enabled = logical(enabled_int);
                obj.External = logical(external_int);
            end
        end
    end
    
    methods (Hidden = true)
        function save(obj, board)
            retVal = calllib('RHD2000m', 'SetFastSettle', ...
                board.handle, obj.Enabled, obj.External, obj.Channel);

            rhd2000.report_error(retVal);            
        end
    end
    
end

