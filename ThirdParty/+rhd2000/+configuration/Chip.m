classdef Chip
    %CHIP Settings related to individual chips.
    %
    % Contains settings specific to individual chips: bandwidth,
    % power, fast settling, and auxililiary digital output
    % configuration.
    %
    % See <a href="matlab:web(fullfile(fileparts(fileparts(which('rhd2000.configuration.Chip'))),'Documentation','html','Chips.html'))">RHD2000 Series Chip</a>
    %
    % See also rhd2000.configuration.Configuration.
    
    properties
        %POWER Power settings for the chips.
        %
        % Various parts of the RHD2000 series chip can be turned off when 
        % not in use to save power. See the data sheet for more information.
        %
        % Note: Power settings apply to all RHD2000 series chips attached to all ports.  
        %
        % See also rhd2000.configuration.Power.
        Power
        
        %BANDWIDTH Bandwidth settings for the on-chip filters.
        %
        % Note: Bandwidth settings apply to all RHD2000 series chips attached to all ports.  
        %
        % See also rhd2000.configuration.Bandwidth.
        Bandwidth
        
        %AUXDIGITALOUTPUTS Configure realtime control for chips' auxiliary digital outputs.
        %
        % Each RHD2000 series chip has an auxiliary digital output pin auxout.
        %
        % The RHD2000 Matlab toolbox lets you configure this pin to follow one 
        % of the RHD2000 evaluation board's digital inputs. So by connecting a 
        % digital signal to the digital input, that signal is routed to the 
        % individual chips attached to ports A, B, C, and D.
        %
        % Note: This setting applies to all RHD2000 series chips attached 
        % to a single port, and each of the four ports (A, B, C, D) are
        % controlled independently.
        %
        % AuxDigitalOutputs{1} controls Port A, AuxDigitalOutputs{2} 
        % controls Port B, AuxDigitalOutputs{3} controls Port C, and 
        % AuxDigitalOutputs{4} controls Port D.
        %
        % See also rhd2000.configuration.AuxDigitalOutput.
        AuxDigitalOutputs

        %FASTSETTLE Configure a chip's fast Settle functionality.
        %
        % All RHD2000 series chips have a hardware 'fast settle' function that 
        % rapidly resets the analog signal path of each amplifier channel to zero 
        % to prevent (or recover from) saturation caused by large transient input 
        % signals such as those due to nearby stimulation. 
        %
        % Note: Fast settle settings apply to all RHD2000 series chips attached 
        % to all ports.  
        %
        % See also rhd2000.configuration.FastSettle.
        FastSettle
    end
    
    methods
        function obj = Chip(board)
        %Call rhd2000.Board/get_configuration_parameters instead.
        %
        % See also rhd2000.Board.get_configuration_parameters, 
            if (nargin == 1)
                obj.Power = rhd2000.configuration.Power(board);
                obj.Bandwidth = rhd2000.configuration.Bandwidth(board);

                ado = cell(4,1);
                for port = 1:4
                    ado{port} = rhd2000.configuration.AuxDigitalOutput(board, port-1);
                end
                obj.AuxDigitalOutputs = ado;

                obj.FastSettle =  rhd2000.configuration.FastSettle(board);
            end
        end
    end
        
    methods (Hidden = true)
        function save(obj, board)
            obj.Power.save(board);
            obj.Bandwidth.save(board);

            ado = obj.AuxDigitalOutputs;
            for port = 1:4
                ado{port}.save(board);
            end

            obj.FastSettle.save(board);
        end
    end

    
end

