classdef DigitalOutputs
    %DIGITALOUTPUTS Settings related to digital outputs on the evaluation board.
    %
    % The RHD2000 Evaluation Board contains 16 digital outputs.
    %
    % These 16 digital outputs can be set in one of two modes:
    %       * 16 user-controllable digital outputs 
    %       * 8 user-controllable digital outputs and 8 digital outputs 
    %         acting as threshold comparators
    %
    % The ComparatorsEnabled property switches between these two modes.  If
    % comparators are enabled, the Comparators property contains a list of
    % settings for each of the eight comparators.  The 8 or 16
    % user-controlled digital outputs are set via rhd2000.Board's
    % DigitalOutputs property.
    %
    % Tip: '8 user-controlled digital outputs and 8 comparators' is the 
    % default configuration for the board when the driver is initialized.
    %
    % Caution: This class configures digital outputs on the RHD2000 
    % evaluation board. Additional auxiliary digital outputs on the 
    % individual RHD2000 series chips are controlled by the 
    % rhd2000.configuration.AuxDigitalOutput class, not by the current class.  
    %
    % Tip: The digital outputs controlled by comparators are only updated 
    % when the RHD2000 evaluation board is running, either via run_fixed or 
    % run_continuously. The digital outputs controlled directly are updated 
    % immediately when board.DigitalOutputs is changed, whether or not the 
    % RHD2000 evaluation board is running. 
    %
    % Example:
    %       params = board.get_configuration_parameters();
    %
    %       % Enable comparators
    %       params.Board.DigitalOutpus.ComparatorsEnabled = true;
    %
    %       % Configure comparator for output 1 to 1 mV, rising edge
    %       params.Board.DigitalOutpus.Comparators{1}.Threshold = 1e-3;
    %       params.Board.DigitalOutpus.Comparators{1}.RisingEdge = true;
    %
    %       board.set_configuration_parameters(params);
    %
    % See <a href="matlab:web(fullfile(fileparts(fileparts(which('rhd2000.configuration.DigitalOutputs'))),'Documentation','html','DigitalOutputs.html'))">Digital Outputs</a>
    %
    % See also rhd2000.configuration.Configuration,
    % rhd2000.configuration.Comparator, rhd2000.Board.DigitalOutputs, 
    % rhd2000.configuration.AuxDigitalOutput.
    
    properties
        %COMPARATORSENABLED Enables or disables threshold comparators.
        %
        % If true, the board is configured to have 8 digital outputs acting
        % as threshold comparators, and the Comparators member is used to
        % configure those.  (The remaining 8 digital outputs are 
        % user-controlled.)
        %
        % If false, the board is configured to have 0 comparators, the
        % Comparators member is unused, and all 16 digital outputs are
        % user-controlled.
        %
        % See also Comparators.
        ComparatorsEnabled
        
        %COMPARATORS Configuration of the threshold comparators
        %
        % When the RHD2000 evaluation board is configured with 
        % ComparatorsEnabled = true, the first 8 digital outputs (0-7) are 
        % threshold comparators, and the second 8 digital outputs (8-15) 
        % are controlled directly.
        %
        % In this mode, digital outputs 0-7 are the result of a threshold 
        % comparison of the evaluation board's analog outputs 0-7. There is 
        % no remapping: digital output X always corresponds to analog 
        % output X.
        %
        % The threshold of each comparator can be set independently, as can 
        % the polarity (i.e., is it a rising-edge threshold or a 
        % falling-edge threshold).
        %
        % See rhd2000.configuration.AnalogOutputs for a more complete 
        % description of analog output configuration.
        %
        % Note: The comparator operates after the raw analog value has been 
        % passed through a high-pass filter, but before any subsequent 
        % processing (digital gain or noise slicing).
        %
        % See also rhd2000.configuration.Comparator, ComparatorsEnabled, 
        % rhd2000.configuration.AnalogOutputs.
        Comparators
    end
    
    methods
        function obj = DigitalOutputs(board)
        %Call rhd2000.Board/get_configuration_parameters instead.
        %
        % See also rhd2000.Board.get_configuration_parameters, 
            if (nargin == 1)
                % API call returns thresholds and rising_edges as arrays
                thresholds = zeros(8, 1);
                rising_edges = zeros(8, 1);
                [retVal, comparators_enabled_int, thresholds, ...
                 rising_edges] = calllib('RHD2000m', ...
                    'GetDigitalOutputConfiguration', board.handle, 0, ...
                    thresholds, rising_edges);
                rhd2000.report_error(retVal);

                obj.ComparatorsEnabled = logical(comparators_enabled_int);
                if (obj.ComparatorsEnabled)
                    % Convert to array of objects
                    obj.Comparators = cell(8,1);
                    for c = 1:8
                        obj.Comparators{c} = ...
                            rhd2000.configuration.Comparator(...
                                    thresholds(c), rising_edges(c));
                    end
                end
            end
        end
    end
       
    methods (Hidden = true)
        function save(obj, board)
            if (obj.ComparatorsEnabled)
                thresholds = zeros(8, 1);
                rising_edges = zeros(8, 1);
                for c = 1:8
                    thresholds(c) = obj.Comparators{c}.Threshold;
                    rising_edges(c) = obj.Comparators{c}.RisingEdge;
                end
                [retVal, ~, ~] = calllib('RHD2000m', ...
                            'Configure8DigitalOutputs8Comparators', ...
                            board.handle, thresholds, rising_edges);
                rhd2000.report_error(retVal);
            else
                retVal = calllib('RHD2000m', ...
                            'Configure16DigitalOutputs', board.handle);
                rhd2000.report_error(retVal);
            end
        end
    end

    
end

