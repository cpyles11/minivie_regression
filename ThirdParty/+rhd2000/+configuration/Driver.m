classdef Driver
    %DRIVER Settings related to the driver.
    %
    % Contains settings not specific to the evaluation board or 
    % individual chips: cable delays and read buffer size.
    %
    % See also rhd2000.configuration.Configuration.
    
    properties
        %CABLEDELAYS Configures cable delays on Port A - Port D.
        %
        % See also rhd2000.configuration.CableDelay.
        CableDelays
        
        %NUMBLOCKSTOREAD Number of data blocks to read at once across the USB interface.
        %
        % This value is set automatically when an rhd2000.Board is created
        % and when the SamplingRate is set. The default value depends on 
        % sampling rate, and sets this value so that roughly 30 times per 
        % second, a group of data blocks will be read.
        %
        % Caution: setting this value too low will result in very 
        % inefficient usage of the USB, and may cause the FPGA's FIFO to 
        % fill up faster than the data can be read out. Setting the value 
        % too high will cause an unnecessary lag between events and when 
        % the data is read into the computer.  
        %
        % Tip: If your control .m file isn't processing data fast enough 
        % (and so the FIFO is filling up), use this property to increase 
        % the number of blocks to be read at once. 
        %
        % Example:
        %       params = board.get_configuration_parameters();
        %       params.Driver.NumBlocksToRead = 2 * params.Driver.NumBlocksToRead;
        %       board.set_configuration_parameters(params);
        %
        % See also rhd2000.configuration.Configuration, rhd2000.Board.read_next_data_block.
        NumBlocksToRead
    end
    
    methods
        function obj = Driver(board)
        %Call rhd2000.Board/get_configuration_parameters instead.
        %
        % See also rhd2000.Board.get_configuration_parameters, 
            if (nargin == 1)
                % Get the cable delays
                obj.CableDelays = cell(4,1);
                for port = 1:4
                    obj.CableDelays{port} = ...
                        rhd2000.configuration.CableDelay(board, port - 1);
                end

                % Get the number of blocks to read
                [retVal, obj.NumBlocksToRead] = ...
                    calllib('RHD2000m', 'GetNumBlocksToRead', ...
                            board.handle, 0);
                rhd2000.report_error(retVal);
            end
        end
    end
        
    methods (Hidden = true)
        function save(obj, board)
            for port = 1:4
                obj.CableDelays{port}.save(board);
            end

            retVal = calllib('RHD2000m', 'SetNumBlocksToRead', ...
                                board.handle, obj.NumBlocksToRead);
            rhd2000.report_error(retVal);            
        end
    end

    
end

