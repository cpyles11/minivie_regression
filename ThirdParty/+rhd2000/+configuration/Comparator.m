classdef Comparator
    %COMPARATOR Configuration of one threshold comparator
    %
    % When the RHD2000 evaluation board is configured with 
    % ComparatorsEnabled = true, the first 8 digital outputs (0-7) are 
    % threshold comparators, and the second 8 digital outputs (8-15) 
    % are controlled directly.
    %
    % In this mode, digital outputs 0-7 are the result of a threshold 
    % comparison of the evaluation board's analog outputs 0-7. There is 
    % no remapping: digital output X always corresponds to analog 
    % output X.
    %
    % The threshold of each comparator can be set independently, as can 
    % the polarity (i.e., is it a rising-edge threshold or a 
    % falling-edge threshold).
    %
    % Note: The comparator operates after the raw analog value has been 
    % passed through a high-pass filter, but before any subsequent 
    % processing (digital gain or noise slicing).
    %
    % Example:
    %       params = board.get_configuration_parameters();
    %
    %       % Enable comparators
    %       params.Board.DigitalOutpus.ComparatorsEnabled = true;
    %
    %       % Configure comparator for output 1 to 1 mV, rising edge
    %       params.Board.DigitalOutpus.Comparators{1}.Threshold = 1e-3;
    %       params.Board.DigitalOutpus.Comparators{1}.RisingEdge = true;
    %
    %       board.set_configuration_parameters(params);
    %
    % See also rhd2000.configuration.DigitalOutputs.
    
    properties
        %THRESHOLD Threshold for the comparator, in Volts.
        Threshold
        
        %RISINGEDGE Polarity of the comparator.
        %
        % True if the comparator should be rising edge triggered.
        % False if the comparator should be falling edge triggered.
        RisingEdge
    end
    
    methods
        function obj = Comparator(threshold, rising_edge)
        %Call rhd2000.Board/get_configuration_parameters instead.
        %
        % See also rhd2000.Board.get_configuration_parameters, 
            if (nargin == 2)
                obj.Threshold = threshold;
                obj.RisingEdge = rising_edge;
            end
        end
    end
    
end

