classdef Bandwidth
    %BANDWIDTH Bandwidth settings for the on-chip filters.
    %
    % Note: Bandwidth settings apply to all RHD2000 series chips attached to all ports.  
    %
    % Example:
    %       params = board.get_configuration_parameters();
    %
    %       % Set to default values
    %       params.Chip.Bandwidth.DesiredUpper = 7500;
    %       params.Chip.Bandwidth.DesiredLower = 0.1;
    %       params.Chip.Bandwidth.DspEnabled = true;
    %       params.Chip.Bandwidth.DesiredDsp = 1;
    %
    %       board.set_configuration_parameters(params);
    %
    % See <a href="matlab:web(fullfile(fileparts(fileparts(which('rhd2000.configuration.Bandwidth'))),'Documentation','html','Filtering.html'))">On-chip Filtering</a>
    %
    % See also rhd2000.configuration.Configuration.
    
    properties
        %DESIREDUPPER Desired upper cutoff frequency of the on-chip analog filter, in Hz. 
        %
        % See also ActualUpper.
        DesiredUpper
        
        %DESIREDLOWER Desired lower cutoff frequency of the on-chip analog filter, in Hz. 
        %
        % See also ActualLower.
        DesiredLower
        
        %DESIREDDSP Desired cutoff frequency of the on-chip digital filter, in Hz.
        %
        % See also ActualDsp, DspEnabled.
        DesiredDsp
        
        %DSPENABLED Whether the optional on-chip DSP is enabled or not.
        %
        % See also DesiredDsp.
        DspEnabled
    end
    
    properties (SetAccess = immutable)
        %ACTUALUPPER Best achievable upper cutoff frequency of the on-chip analog filter, in Hz.
        %
        % Due to on-chip limitations, only certain cutoff frequencies are
        % achievable.  For example, if you request 999 Hz, you might get
        % 1000 Hz instead.
        %
        % This value is set when get_configuration_parameters is called.
        % So if you change DesiredUpper, ActualUpper will not automatically change;
        % you'll need to call set_configuration_parameters and then
        % get_configuration_parameters.
        %
        % Example:
        %       params = board.get_configuration_parameters();
        %
        %       % DesiredUpper = 7500; ActualUpper = 7604.
        %
        %       params.Chip.Bandwidth.DesiredUpper = 10000;
        %       % DesiredUpper = 10000; ActualUpper = 7604 (unchanged).
        %
        %       board.set_configuration_parameters(params);
        %       params = board.get_configuration_parameters();
        %       % DesiredUpper = 10000; ActualUpper = 9987.
        %
        % See also DesiredUpper.
        ActualUpper
        
        %ACTUALLOWER Best achievable lower cutoff frequency of the on-chip analog filter, in Hz.
        %
        % Due to on-chip limitations, only certain cutoff frequencies are
        % achievable.  For example, if you request 0.999 Hz, you might get
        % 1.000 Hz instead.
        %
        % This value is set when get_configuration_parameters is called.
        % So if you change DesiredLower, ActualLower will not automatically change;
        % you'll need to call set_configuration_parameters and then
        % get_configuration_parameters.
        %
        % Example:
        %       params = board.get_configuration_parameters();
        %
        %       % DesiredLower = 0.1; ActualLower = 0.0945.
        %
        %       params.Chip.Bandwidth.DesiredLower = 1;
        %       % DesiredLower = 1; ActualLower = 0.0945 (unchanged).
        %
        %       board.set_configuration_parameters(params);
        %       params = board.get_configuration_parameters();
        %       % DesiredLower = 1; ActualLower = 1.0977.
        %
        % See also DesiredLower.
        ActualLower
        
        %ACTUALDSP Best achievable cutoff frequency of the on-chip digital filter, in Hz.
        %
        % Due to on-chip limitations, only certain cutoff frequencies are
        % achievable.  For example, if you request 0.999 Hz, you might get
        % 1.000 Hz instead.
        %
        % This value is set when get_configuration_parameters is called.
        % So if you change DesiredDsp, ActualDsp will not automatically change;
        % you'll need to call set_configuration_parameters and then
        % get_configuration_parameters.
        %
        % Example:
        %       params = board.get_configuration_parameters();
        %
        %       % DesiredDsp = 1; ActualDsp = 0.7772.
        %
        %       params.Chip.Bandwidth.DesiredDsp = 10000;
        %       % DesiredDsp = 2; ActualDsp = 0.7772 (unchanged).
        %
        %       board.set_configuration_parameters(params);
        %       params = board.get_configuration_parameters();
        %       % DesiredDsp = 2; ActualDsp = 1.5546.
        %
        % See also DesiredDsp, DspEnabled.
        ActualDsp
    end
    
    methods
        function obj = Bandwidth(board)
        %Call rhd2000.Board/get_configuration_parameters instead.
        %
        % See also rhd2000.Board.get_configuration_parameters, 
            if (nargin == 1)
                [retVal, obj.DesiredUpper, obj.ActualUpper, ...
                    obj.DesiredLower, obj.ActualLower, ...
                    obj.DesiredDsp, obj.ActualDsp, obj.DspEnabled ] = ...
                        calllib('RHD2000m', 'GetBandwidthParameters', ...
                                board.handle, 0, 0, 0, 0, 0, 0, 0);
                rhd2000.report_error(retVal);
            end
        end
    end
    
    methods (Hidden = true)
        function obj = save(obj, board)
            retVal = calllib('RHD2000m', 'SetBandwidthParameters', ...
                                board.handle, ...
                                obj.DesiredUpper, obj.DesiredLower, ...
                                obj.DesiredDsp, obj.DspEnabled);
            rhd2000.report_error(retVal);
        end
    end
end

