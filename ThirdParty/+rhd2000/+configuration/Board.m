classdef Board
    %BOARD Settings related to the RHD2000 evaluation board.
    %
    % Contains settings specific to the evaluation board: configuration
    % of analog and digital outputs on the board.
    %
    % See <a href="matlab:web(fullfile(fileparts(fileparts(which('rhd2000.Board'))),'Documentation','html','EvaluationBoard.html'))">RHD2000 Evaluation Board</a>
    %
    % See also rhd2000.configuration.Configuration.
    
    properties
        %DIGITALOUTPUTS Settings related to digital outputs on the evaluation board.
        %
        % See also rhd2000.configuration.DigitalOutputs.
        DigitalOutputs
        

        %ANALOGOUTPUTS Settings related to analog outputs on the evaluation board.
        %
        % Note: Configuration of the digital high-pass filter, noise slicing, 
        % and digital gain parameters affect all analog outputs identically;
        % they are not per-output.
        %
        % See also rhd2000.configuration.AnalogOutputs.
        AnalogOutputs
    end
    
    methods
        function obj = Board(board)
        %Call rhd2000.Board/get_configuration_parameters instead.
        %
        % See also rhd2000.Board.get_configuration_parameters, 
            if (nargin == 1)
                obj.DigitalOutputs = rhd2000.configuration.DigitalOutputs(board);
                obj.AnalogOutputs = rhd2000.configuration.AnalogOutputs(board);
            end
        end
    end
        
    methods (Hidden = true)
        function save(obj, board)
            obj.DigitalOutputs.save(board);
            obj.AnalogOutputs.save(board);
        end
    end

    
end

