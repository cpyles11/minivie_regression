classdef CableDelay
    %CABLEDELAY Settings related to SPI cable-read delay.
    %
    % The SPI protocol, used for communication between the RHD2000 evaluation 
    % board and RHD2000 series chips, consists of:
    %     * a bit being sent from the master (i.e., the evaluation board) 
    %       to the slave (i.e., the chip) - this is called Master Output 
    %       Slave Input (MOSI) 
    %     * a bit being sent from the slave (i.e., the chip) to the master 
    %       (i.e., the evaluation board) - this is called Master Input 
    %       Slave Output (MISO)
    %
    % Typically, both bits are read at the same time. However, with the 
    % RHD2000 evaluation board and the RHD2000 series chip physically 
    % separated by a significant distance (several feet or meters), there 
    % may be a noticeable lag before the MISO input to the evaluation board 
    % stabilizes. This class lets you to set the delay between when the 
    % evaluation board writes the MOSI output and reads the MISO input to 
    % compensate for that.
    %
    % Cable delays can be configured either:
    %      Manually      - delay in integer clock steps, where each clock step 
    %                      is 1/2800 of a per-channel sampling period. 
    %      Automatically - delay is specified in meters 
    %
    % Note: The get_chip_types function sets the cable delays to optimum 
    % values, based on a quick per-chip read test.  It is called internally
    % when a board is created, or can be called manually later.
    %
    % Tip: The optimum delay changes when the board sampling rate is 
    % changed. If the delays are configured in meters, the board will be 
    % reconfigured when the board sampling rate is changed. If the delays 
    % are configured manually, you'll need to make any adjustments to the 
    % delays by hand.  
    %
    % Example:
    %       params = board.get_configuration_parameters();
    %
    %       % Set Port A to a 1.5m delay
    %       params.Driver.CableDelays{1}.DelayInMeters = 1.500;
    %
    %       % Set Port B to a manual delay
    %       params.Driver.CableDelays{2}.UseManual = true;
    %       params.Driver.CableDelays{2}.ManualDelay = 8;
    %
    %       board.set_configuration_parameters(params);
    %
    % See also rhd2000.configuration.Driver, rhd2000.Board.estimate_cable_lengths_in_meters.

    
    properties
        %USEMANUAL Specifies whether to use ManualDelay or DelayInMeters.
        %
        % UseManual = true uses the ManualDelay value
        % UseManual = false uses the DelayInMeters value
        %
        % See also ManualDelay, DelayInMeters.
        UseManual
        
        %MANUALDELAY Cable delay in clock steps.
        %
        % Each clock step is 1/2800 of a per-channel sampling period. 
        % Valid values are 0-15. 
        %
        % Only used with UseManual = true.
        %
        % See also UseManual, DelayInMeters.
        ManualDelay
        
        %DELAYINMETERS Configured length of the cable, in meters. 
        %
        % Only used with UseManual = false.
        %
        % See also UseManual, ManualDelay.
        DelayInMeters
    end
    
    properties (Access = private, Hidden = true)
        port_index
    end
    
    
    methods
        function obj = CableDelay(board, port_index)
        %Call rhd2000.Board/get_configuration_parameters instead.
        %
        % See also rhd2000.Board.get_configuration_parameters, 
            if (nargin == 2)
                obj.port_index = port_index;
                [retVal, use_manual_int, obj.ManualDelay, ...
                 obj.DelayInMeters] = ...
                    calllib('RHD2000m', 'GetCableDelays', ...
                            board.handle, int32(port_index), 0, 0, 0);
                rhd2000.report_error(retVal);

                obj.UseManual = logical(use_manual_int);
            end
        end
    end
        
    methods (Hidden = true)
        function save(obj, board)
            retVal = calllib('RHD2000m', 'SetCableDelays', ...
                            board.handle, int32(obj.port_index), ...
                            obj.UseManual, obj.ManualDelay, ...
                            obj.DelayInMeters);
            rhd2000.report_error(retVal);            
        end
    end
    
end

