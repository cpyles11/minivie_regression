classdef AnalogOutputs
    %ANALOGOUTPUTS Settings related to analog outputs on the evaluation board.
    %
    % The RHD2000 Evaluation Board has 8 analog outputs. These are sometimes 
    % referred to as DAC outputs, because a digital signal is converted to 
    % analog via a digital-to-analog converter (DAC) before producing the 
    % analog output.
    %
    % Each analog output comes from a digital source, either one of the 
    % amplifiers or the DAC Manual value. The source of each analog output 
    % can be configured individually with the Sources property, and the 
    % value of DAC Manual can be set via rhd2000.Board.DacManual.
    %
    % Before the digital input value is converted to analog, the signal 
    % passes through an optional highpass filter, configured via the 
    % HighpassFilterEnabled and HighpassFilterFrequency properties. If the 
    % digital input value changes rapidly, you may want to settle that 
    % filter using the DSPSettle property.
    %
    % After the highpass filter, the signal is amplified via the 
    % DACGain value.
    %
    % The signals from analog outputs 1 and 2 are routed to the Audio Line 
    % Out as well, and noise slicing may be used (see the NoiseSuppress for 
    % more information). Audio noise slicing only affects analog outputs 
    % 1 and 2.
    %
    % Note: Configuration of the digital high-pass filter, noise slicing, 
    % and digital gain parameters affect all analog outputs identically;
    % they are not per-output.
    %
    % Example:
    %       params = board.get_configuration_parameters();
    %
    %       % Disable DSP Settle
    %       params.Board.AnalogOutputs.DspSettle = false;
    %
    %       % Turn digital highpass filter on, with cutoff of 250 Hz
    %       params.Board.AnalogOutputs.HighpassFilterEnabled = true;
    %       params.Board.AnalogOutputs.HighpassFilterFrequency = 250;
    %
    %       % Set noise suppress to 100 microVolts (channels 1 & 2 only)
    %       params.Board.AnalogOutputs.NoiseSuppress = 100e-6;
    %
    %       % Set the DAC gain to 2^3 * 515 V/V
    %       params.Board.AnalogOutputs.DacGain = 3;
    %
    %       % Disable analog output 1.  See Sources for detailed settings.
    %       params.Board.AnalogOutputs.Sources{1}.Enabled = false;
    %
    %       board.set_configuration_parameters(params);
    %
    %
    % See <a href="matlab:web(fullfile(fileparts(fileparts(which('rhd2000.configuration.AnalogOutputs'))),'Documentation','html','AnalogOutputs.html'))">Analog Outputs</a>
    %
    % See <a href="matlab:web(fullfile(fileparts(fileparts(which('rhd2000.configuration.AnalogOutputs'))),'Documentation','html','ConfigureAnalogOutputs.html'))">Analog Output Details</a>
    %
    % See also rhd2000.configuration.Configuration, rhd2000.Board.DacManual, 
    % rhd2000.configuration.AnalogOutputSource.
    
    properties
        %DSPSETTLE DSP Settle turns on or off DSP settle function.
        %
        % The DSP settle function in the FPGA. DSP settling occurs when the 
        % RHD2000 evaluation board is running and a CONVERT command is executed. 
        %
        % See also HighpassFilterFrequency, HighpassFilterEnabled.
        DspSettle

        %HIGHPASSFILTERENABLED Enables or disables digital filters.
        %
        % Enables or disables the optional digital first-order high-pass 
        % filters implemented in the FPGA on all eight DAC output channels. 
        % These filters may be used to remove low-frequency local field 
        % potential (LFP) signals from neural signals to facilitate spike 
        % detection while still recording the complete wideband data. This 
        % is useful when using the low-latency FPGA thresholds to detect 
        % spikes and produce digital pulses on the TTL outputs, for example.
        %
        % See also HighpassFilterFrequency, DspSettle.
        HighpassFilterEnabled

        %HIGHPASSFILTERFREQUENCY Cutoff frequency for digital filters.
        %
        % Cutoff frequency (in Hz) for optional digital first-order 
        % high-pass filters implemented in the FPGA on all eight 
        % DAC/comparator channels.
        %
        % See also HighpassFilterEnabled, DspSettle.
        HighpassFilterFrequency

        %NOISESUPPRESS Threshold for audio noise slicing.
        %
        % The signals from analog outputs 1 and 2 are routed to the Audio Line 
        % Out as well, and noise suppression may occur via the NoiseSuppress 
        % parameter. Audio noise suppression only affects analog outputs 1 and 2.
        %
        % Any data points of the waveform that fall within the slice range are 
        % set to zero, and signals extending beyond this range are brought in 
        % towards zero. The result is a dramatic improvement in the audibility 
        % of action potentials. Users are encouraged to experiment with this 
        % feature in neural recording experiments.
        %
        % Note: Both the Audio Line Out and the analog output 1 and 2 outputs 
        % are post-noise suppression.  
        %
        % The maximum value for this parameter is around 400 microVolts, 
        % and the smallest granularity that makes a difference is around 
        % 3 microVolts.
        NoiseSuppress

        %DACGAIN Scales the digital signals to all eight AD5662 DACs.
        %
        % Valid values 0-7.
        %
        % Scales the signal by 2^DacGain * 515 V/V.  I.e.:
        %       DacGain = 0 scales by 1 * 515 V/V
        %       DacGain = 7 scales by 128 * 515 V/V
        %
        % Roughly speaking, a gain of 1 x 515 V/V scales a full-range input 
        % signal (� 5 mV) from an amplifier to approximately � 3.3V at the 
        % DAC output. For smaller input signals, larger gains may be 
        % appropriate. 
        DacGain

        %SOURCES Digital input sources for the eight analog outputs.
        % 
        % See also rhd2000.configuration.AnalogOutputSource.
        Sources
    end
    
    methods
        function obj = AnalogOutputs(board)
        %Call rhd2000.Board/get_configuration_parameters instead.
        %
        % See also rhd2000.Board.get_configuration_parameters, 
            if (nargin == 1)
                [retVal, obj.DspSettle, obj.HighpassFilterEnabled, ...
                    obj.HighpassFilterFrequency, obj.NoiseSuppress, ...
                    obj.DacGain] = ...
                        calllib('RHD2000m', 'GetAnalogOutputCommon', ...
                                board.handle, 0, 0, 0, 0, 0);
                rhd2000.report_error(retVal);
                
                obj.Sources = cell(8, 1);
                for dac=1:8
                    obj.Sources{dac} = rhd2000.configuration.AnalogOutputSource(board, dac - 1);
                end
            end
        end
    end
       
    methods (Hidden = true)
        function save(obj, board)
            retVal = calllib('RHD2000m', ...
                             'ConfigureAnalogOutputCommon', ...
                             board.handle, obj.DspSettle, ...
                             obj.HighpassFilterEnabled, ...
                             obj.HighpassFilterFrequency, ...
                             obj.NoiseSuppress, obj.DacGain);
            rhd2000.report_error(retVal);

            for dac=1:8
                obj.Sources{dac}.save(board);
            end
        end
    end

    
end

