classdef Configuration
    %CONFIGURATION Settings for the board, chips, and driver.
    %
    % To change settings, call get_configuration_parameters, change the 
    % parameters in the returned configuration object, then call 
    % set_configuration_parameters to change the settings.
    %
    % Example:
    %       params = board.get_configuration_parameters();
    %
    %       % Set bandwidth cutoffs
    %       params.Chip.Bandwidth.DesiredLower = 1;
    %       params.Chip.Bandwidth.DesiredUpper = 3000;
    %
    %       board.set_configuration_parameters(params);
    %
    % See also rhd2000.Board.get_configuration_parameters, 
    % rhd2000.Board.set_configuration_parameters.
    
    properties
        %DRIVER Settings related to the driver.
        %
        % Contains settings not specific to the evaluation board or 
        % individual chips: cable delays and read buffer size.
        %
        % See also rhd2000.configuration.Driver.
        Driver

        %CHIP Settings related to individual chips.
        %
        % Contains settings specific to individual chips: bandwidth,
        % power, fast settling, and auxililiary digital output
        % configuration.
        %
        % See also rhd2000.configuration.Chip.
        Chip

        %BOARD Settings related to the RHD2000 evaluation board.
        %
        % Contains settings specific to the evaluation board: configuration
        % of analog and digital outputs on the board.
        %
        % See also rhd2000.configuration.Board.
        Board
    end
    
    methods
        function obj = Configuration(board)
        %Call rhd2000.Board/get_configuration_parameters instead.
        %
        % See also rhd2000.Board.get_configuration_parameters, 
            if (nargin == 1)
                obj.Driver = rhd2000.configuration.Driver(board);
                obj.Chip = rhd2000.configuration.Chip(board);
                obj.Board = rhd2000.configuration.Board(board);
            end
        end
    end
        
    methods (Hidden = true)
        function save(obj, board)
            obj.Driver.save(board);
            obj.Chip.save(board);
            obj.Board.save(board);
        end
    end

    
end

