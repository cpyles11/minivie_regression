classdef Power
    %POWER Power settings for the chips.
    %
    % Various parts of the RHD2000 series chip can be turned off when 
    % not in use to save power. See the data sheet for more information.
    %
    % Note: Power settings apply to all RHD2000 series chips attached to all ports.  
    %
    % Example:
    %       params = board.get_configuration_parameters();
    %
    %       % Full power down
    %       params.Chip.Power.AmplifierVRef = false;
    %       params.Chip.Power.ADCComparator = false;
    %       params.Chip.Power.VddSense = false;
    %       params.Chip.Power.Auxin1 = false;
    %       params.Chip.Power.Auxin2 = false;
    %       params.Chip.Power.Auxin3 = false;
    %       params.Chip.Power.Amplifiers = zeros(64,1);
    %
    %       board.set_configuration_parameters(params);
    %
    % See also rhd2000.configuration.Configuration.
    
    properties
        %AMPLIFIERVREF Amplifier Voltage Reference.
        %
        % Values:
        %      true    (default) to power up voltage references used by the 
        %              biopotential amplifiers.
        %      false   to reduce power supply current consumption by 180 micro Amps 
        %              when the amplifiers will not be used for an extended
        %              period of time.
        % After setting this bit to true, at least 100 micro seconds must elapse 
        % before ADC samples are valid, or before ADC calibration is 
        % executed. 
        AmplifierVRef
        
        %ADCCOMPARATOR Configures the bias current of the ADC comparator.
        %
        % Values:
        %      true    (default) for normal operation and ADC calibration.
        %      false   to reduce power supply current consumption by 
        %              80 micro Amps when the ADC will not be used for an 
        %              extended period of time.
        ADCComparator
        
        %VDDSENSE  Supply Voltage Sensor.
        %
        % Values: 
        %      true     (default) enables the on-chip supply voltage sensor.
        %      false    Supply voltage is not sampled, reduce current 
        %               consumption by 10 micro Amps.
        VddSense
        
        %AUXIN1 Lets auxin1 be used as an Auxiliary Analog Input. 
        %
        % This causes a buffer to be activated and draws a little power.
        Auxin1
        
        %AUXIN2 Lets auxin2 be used as an Auxiliary Analog Input. 
        %
        % This causes a buffer to be activated and draws a little power.
        Auxin2
        
        %AUXIN3 Lets auxin3 be used as an Auxiliary Analog Input. 
        %
        % This causes a buffer to be activated and draws a little power.
        Auxin3
        
        %AMPLIFIERS Powers up or down the selected amplifiers on a chip
        %
        % This must be an array of size 64. (If your chip has fewer than 
        % 64 channels, higher elements in the array will be ignored.) 
        Amplifiers
    end
    
    methods
        function obj = Power(board)
        %Call rhd2000.Board/get_configuration_parameters instead.
        %
        % See also rhd2000.Board.get_configuration_parameters, 
            if (nargin == 1)
                obj.Amplifiers = zeros(64,1);
                [retVal, obj.AmplifierVRef, obj.ADCComparator, ...
                 obj.VddSense, obj.Auxin1, obj.Auxin2, obj.Auxin3, ...
                 obj.Amplifiers ] = ...
                    calllib('RHD2000m', 'GetPowerSettings', ...
                        board.handle, 0, 0, 0, 0, 0, 0, obj.Amplifiers);
                rhd2000.report_error(retVal);                    
            end
        end
    end
    
    methods (Hidden = true)
        function save(obj, board)
            if length(obj.Amplifiers) ~= 64
                error('The ''amplifiers'' parameter must be of length 64');
            end

            [retVal, ~] = calllib('RHD2000m', 'SetPowerSettings', ...
                                    board.handle, obj.AmplifierVRef, ...
                                    obj.ADCComparator, obj.VddSense, ...
                                    obj.Auxin1, obj.Auxin2, obj.Auxin3, ...
                                    obj.Amplifiers);
            rhd2000.report_error(retVal);
        end
    end   
end

