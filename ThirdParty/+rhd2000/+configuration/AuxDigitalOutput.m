classdef AuxDigitalOutput
    %AUXDIGITALOUTPUT Configure realtime control for a chip's auxiliary digital output.
    %
    % Each RHD2000 series chip has an auxiliary digital output pin auxout.
    %
    % The RHD2000 Matlab toolbox lets you configure this pin to follow one 
    % of the RHD2000 evaluation board's digital inputs. So by connecting a 
    % digital signal to the digital input, that signal is routed to the 
    % individual chips attached to ports A, B, C, and D.
    %
    % Tip: Even with realtime control enabled, auxiliary digital outputs 
    % are only updated when the RHD2000 evaluation board is running, 
    % either via run_fixed or run_continuously.
    %
    % Note: This setting applies to all RHD2000 series chips attached 
    % to a single port.
    %
    % Example:
    %       params = board.get_configuration_parameters();
    %
    %       % Configure real-time auxiliary digital output for chip(s) on
    %       % Port A to come from digital input '3'
    %       params.Chip.AuxDigitalOutputs{1}.Enabled = true;
    %       params.Chip.AuxDigitalOutputs{1}.Channel = 3;
    %
    %       board.set_configuration_parameters(params);
    %
    % See <a href="matlab:web(fullfile(fileparts(fileparts(which('rhd2000.configuration.AuxDigitalOutput'))),'Documentation','html','AuxiliaryDigitalOutputs.html'))">Auxiliary Digital Outputs</a>
    %
    % See also rhd2000.configuration.Configuration.
    
    properties
        %ENABLED Enables/disables real-time control.
        %
        % Note: disabled pins output 0 V.
        %
        % See also Channel.
        Enabled
        
        %CHANNEL Which digital input to use.
        %
        % Channel configures which of the 16 digital inputs on the 
        % RHD2000 evaluation board should be routed to this auxiliary 
        % digital output on a chip.
        %
        % Note that Channel is 0-indexed, i.e., valid values are 0-15.
        %
        % See also Enabled.
        Channel
    end
    
    properties (Access = private, Hidden = true)
        port_index
    end
    
    
    methods
        function obj = AuxDigitalOutput(board, port_index)
        %Call rhd2000.Board/get_configuration_parameters instead.
        %
        % See also rhd2000.Board.get_configuration_parameters, 
            if (nargin == 2)
                obj.port_index = port_index;
                [retVal, obj.Enabled, obj.Channel] = ...
                    calllib('RHD2000m', ...
                            'GetAuxDigitalOutputConfiguration', ...
                            board.handle, int32(port_index), 0, 0);
                rhd2000.report_error(retVal);
            end
        end
    end
    
    methods (Hidden = true)
        function save(obj, board)
            retVal = calllib('RHD2000m', ...
                             'SetAuxDigitalOutputConfiguration', ...
                             board.handle, int32(obj.port_index), ...
                             obj.Enabled, obj.Channel);
            rhd2000.report_error(retVal);            
        end
    end
    
end

