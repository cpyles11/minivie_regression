classdef AnalogOutputSource
    %ANALOGOUTPUTSOURCE Digital input source for one analog output.
    %
    % Configures which input source (if any) is routed to an analog output 
    % on the RHD2000 evaluation board.
    %
    % Each analog output can be configured to output data received from 
    % either one of the amplifier channels or from a global "DAC Manual" 
    % value.
    %
    % Input source      How to configure 
    % ------------      ----------------
    % Amplifier input   Set DataSource to 0-7 (Port A, MISO 1 through 
    %                   Port D, MISO 2), set Channel 
    % Manual            Set DataSource to 8 (DAC Manual); Channel is not used 
    %
    % Tip: analog outputs are only updated when the evaluation board is 
    % running, either via run_fixed or run_continuously.
    %
    % See also rhd2000.configuration.AnalogOutputs,
    % rhd2000.Board.run_fixed, rhd2000.Board.run_continuously.
    
    properties
        %ENABLED Enables or disables an analog output.
        %
        % Note: Disabled channels output 0 V.  
        Enabled
        
        %DATASOURCE Selects which input is routed to this analog output.
        %
        % Valid values are 0-8.
        %
        % 0 = Port A, MISO 1
        % 1 = Port A, MISO 2
        % 2 = Port B, MISO 1
        % 3 = Port B, MISO 2
        % 4 = Port C, MISO 1
        % 5 = Port C, MISO 2
        % 6 = Port D, MISO 1
        % 7 = Port D, MISO 2
        % 8 = DAC Manual value
        %
        % See <a href="matlab:web(fullfile(fileparts(fileparts(which('rhd2000.configuration.AnalogOutputSource'))),'Documentation','html','DataSources.html'))">Data Sources</a>
        %
        % See also Channel.
        DataSource
        
        %CHANNEL Selects amplifier channel to use.
        %
        % When Data Source is set to one of the data sources (i.e., not 
        % DAC Manual), this value picks which amplifier channel from that 
        % data source is used to control the analog output. 
        %
        % Allowed values depend on chip:
        %       RHD2216 0-15
        %       RHD2132 0-31 
        %       RHD2164 0-63 
        %
        % Note: This must be an amplifier, not one of the auxiliary analog 
        % inputs.  
        %
        % See also DataSource.
        Channel
    end
    
    properties (Access = private, Hidden = true)
        index
    end
    
    
    methods
        function obj = AnalogOutputSource(board, index)
        %Call rhd2000.Board/get_configuration_parameters instead.
        %
        % See also rhd2000.Board.get_configuration_parameters, 
            if (nargin == 2)
                obj.index = index;
                [retVal, obj.Enabled, obj.DataSource, obj.Channel] = ...
                    calllib('RHD2000m', 'GetAnalogOutputSource', ...
                            board.handle, index, 0, 0, 0);

                rhd2000.report_error(retVal);
            end
        end
    end
       
    methods (Hidden = true)
        function save(obj, board)
            retVal = calllib('RHD2000m', 'ConfigureAnalogOutputSource', ...
                              board.handle, obj.index, obj.Enabled, ...
                              obj.DataSource, obj.Channel);
            rhd2000.report_error(retVal);            
        end
    end
    
end

