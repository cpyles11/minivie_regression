function obj = fetch_from_api(obj)
%FETCH_FROM_API Gets the signal group from the API
%
% Used internally; you shouldn't need to call this directly.

% Get the name and number of channels
obj.Name = blanks(200);
[retVal, obj.Name, num_channels] = ...
    calllib('RHD2000m', 'GetSignalGroupInfo', obj.board.handle, ...
            obj.sg_index_0, length(obj.Name), obj.Name, 0);
rhd2000.report_error(retVal);

% Create the individual channel objects
if num_channels ~= 0
    channels = cell(num_channels, 1);
    for channel_index=1:num_channels
        channels{channel_index} = ...
            rhd2000.savefile.SignalChannel(obj.board, obj.sg_index_0, ...
                                    channel_index - 1);
    end
else
    channels = [];
end
obj.Channels_Internal = channels;

end
