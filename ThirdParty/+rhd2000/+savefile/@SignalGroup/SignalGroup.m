classdef SignalGroup < rhd2000.SetGet_light
    %SIGNALGROUP Configuration of one signal group in the save file.
    %
    % The save file is organized as seven Signal Groups:
    %      Port A
    %      Port B
    %      Port C
    %      Port D
    %      Evaluation board analog inputs (ADCs)
    %      Evaluation board digital inputs
    %      Evaluation board digital outputs
    %
    % This class supports is used to configure what information is included
    % in the save file for the given signal group.
    %
    % See properties for more information.
    %
    % See also rhd2000.savefile.SaveFile, rhd2000.savefile.SignalChannel.
    
    properties (SetAccess = private)
        %NAME The signal group's name.
        %
        % Example: 'Port A'
        %
        % The signal group names are specified by the toolbox, and are not
        % modifiable.
        Name
    end
    
    properties (Dependent = true)
        %CHANNELS Individual data channels within the signal group.
        %
        % A given signal group consists of multiple data channels.
        %
        % Signal groups 'Port A' - 'Port D' consist of:
        %      Amplifier channels
        %      Auxiliary input channels
        %      Supply voltage channels
        %
        % The evaluation board signal groups consist of channels for
        % analog inputs, digital inputs, or digital outputs, respectively.
        %
        % See also rhd2000.savefile.SignalChannel.
        Channels
    end
    
    properties (Access = private, Hidden = true)
        Channels_Internal
        board
        sg_index_0
    end
    
    methods
        function obj = SignalGroup(board, sg_index_0)
            if (nargin == 2)
                obj.board = board;
                obj.sg_index_0 = sg_index_0;
                obj.fetch_from_api();
            end
        end
        
        function value = get.Channels(obj)
            value = obj.Channels_Internal;
        end
        
        function obj = set.Channels(obj, value)
            if length(value) ~= length(obj.Channels_Internal)
                error(['Illegal operation: you can''t add or delete channels; ' ...
                    'did you want to disable channels?']);
            end

            for cindex = 1:length(value)
                if ~isa(value{cindex}, 'rhd2000.savefile.SignalChannel')
                    error('Invalid type');
                end
            end
            obj.Channels_Internal = value;
        end
    end
    
    methods (Access = private, Hidden = true)
        obj = fetch_from_api(obj)
    end    
end

