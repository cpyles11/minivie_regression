function open(obj, save_format, path)
%OPEN Opens the save file
%
% board.SaveFile.open(format, path) opens a save file of the given
% format at the given path.
%      format should be of type rhd2000.savefile.Format.
%
% Note: you should do any save file configuration before you open the
% file; when you open the disk file, a header will be written that contains
% any options you have configured.
%
% You should close the file when you are done with it.
%
% Example:
%      board.SaveFile.Note1 = 'This is a test';
%      board.SaveFile.open(rhd2000.savefile.Format.intan, 'C:\test.rhd');
%
% See also rhd2000.savefile.Format, close.

[retVal, ~] = calllib('RHD2000m', 'OpenSaveFile', ...
                        obj.board.handle, int32(save_format), path);

rhd2000.report_error(retVal);
    
end
