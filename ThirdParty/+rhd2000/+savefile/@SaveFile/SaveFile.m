classdef SaveFile < rhd2000.SetGet_light
    %SAVEFILE Controls saving data to disk
    %
    % This class controls configuration and opening/closing of save files.
    % In general, configuration should be done before the file is opened;
    % when the file is opened a header is written that depends on the
    % configuration parameters.
    %
    % Example:
    %      board.SaveFile.Note1 = 'Save file configuration example';
    %
    %      % Turn off the last digital output (i.e., don't save)
    %      board.SaveFile.SignalGroups{7}.Channels{16}.Enabled = false;
    %
    %      % Rename the new last one
    %      board.SaveFile.SignalGroups{7}.Channels{15}.Name = ...
    %           'New last digital output';
    %
    %      board.SaveFile.open(rhd2000.savefile.Format.intan, 'C:\test.rhd');
    %
    %      % Write some data to the file
    %      datablock.save();
    %
    %      board.SaveFile.close();
    %
    % See methods and properties for more information.
    %
    % See also rhd2000.datablock.DataBlock.save, SignalGroup.
    
    properties (Dependent = true)
        %NOTE1 Note that shows up in the save file header.
        % Example:
        %      board.SaveFile.Note1 = 'My note';
        %
        % See also Note2, Note3.
        Note1

        %NOTE2 Note that shows up in the save file header.
        % Example:
        %      board.SaveFile.Note2 = 'My note';
        %
        % See also Note1, Note3.
        Note2

        %NOTE3 Note that shows up in the save file header.
        % Example:
        %      board.SaveFile.Note3 = 'My note';
        %
        % See also Note1, Note2.
        Note3
        
        %SIGNALGROUPS Configuration of the signals in the save file.
        %
        % The save file is organized as seven Signal Groups:
        %      Port A
        %      Port B
        %      Port C
        %      Port D
        %      Evaluation board analog inputs (ADCs)
        %      Evaluation board digital inputs
        %      Evaluation board digital outputs
        %
        % This variable allows configuration of the groups, including
        % names of individual channels within each group and whether or
        % not the channel is included in the output.
        %
        % See also rhd2000.savefile.SignalGroup.
        SignalGroups
    end
    
    properties (Access = private, Hidden = true)
        board
        Note1_internal
        Note2_internal
        Note3_internal
        SignalGroups_internal
    end
    
    methods
        function obj = SaveFile(board)
            if nargin > 0
                obj.board = board;
                obj.fetch_from_api();
            end
        end
        
        % Defined in other files
        open(obj, save_format, path)
        close(obj)
        
        % Getters and setters
        function value = get.Note1(obj)
            value = obj.Note1_internal;
        end
        
        function value = get.Note2(obj)
            value = obj.Note2_internal;
        end
        
        function value = get.Note3(obj)
            value = obj.Note3_internal;
        end
        
        function obj = set.Note1(obj, value)
            if ~strcmp(obj.Note1_internal, value)
                obj.Note1_internal = value;
                obj.update_notes();
            end
        end
        
        function obj = set.Note2(obj, value)
            if ~strcmp(obj.Note2_internal, value)
                obj.Note2_internal = value;
                obj.update_notes();
            end
        end
        
        function obj = set.Note3(obj, value)
            if ~strcmp(obj.Note3_internal, value)
                obj.Note3_internal = value;
                obj.update_notes();
            end
        end
        
        function value = get.SignalGroups(obj)
            value = obj.SignalGroups_internal;
        end
        
        function obj = set.SignalGroups(obj, value)
            if length(value) ~= length(obj.SignalGroups_internal)
                error('Illegal operation: you can''t add or delete signal groups');
            end

            for sgindex = 1:length(value)
                if ~isa(value{sgindex}, 'rhd2000.savefile.SignalGroup')
                    error('Invalid type');
                end
            end

            obj.SignalGroups_internal = value;
        end
        
    end

    methods (Access = private, Hidden = true)
        obj = fetch_from_api(obj)
        
        function update_notes(obj)
            [retVal, ~, ~, ~] = ...
                calllib('RHD2000m', 'ConfigureSaveFile', ...
                    obj.board.handle, 0, ...
                    obj.Note1_internal, obj.Note2_internal, ...
                    obj.Note3_internal);
            rhd2000.report_error(retVal);
        end      
    end
end

