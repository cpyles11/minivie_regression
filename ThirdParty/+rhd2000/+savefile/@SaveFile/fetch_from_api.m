function obj = fetch_from_api(obj)
%FETCH_FROM_API Gets the save file configuration from the API
%
% Used internally; you shouldn't need to call this directly.

% Get Notes
obj.Note1_internal = blanks(200);
obj.Note2_internal = blanks(200);
obj.Note3_internal = blanks(200);
[retVal, obj.Note1_internal, obj.Note2_internal, obj.Note3_internal] = ...
    calllib('RHD2000m', 'GetSaveFileNotes', obj.board.handle, ...
            length(obj.Note1_internal), obj.Note1_internal, ...
            obj.Note2_internal, obj.Note3_internal);
rhd2000.report_error(retVal);

% Get number of signal groups
[retVal, num_groups] = ...
    calllib('RHD2000m', 'GetNumSignalGroups', obj.board.handle, 0);
rhd2000.report_error(retVal);

obj.SignalGroups_internal = cell(num_groups, 1);
for sg_index = 1:num_groups
    obj.SignalGroups_internal{sg_index} = ...
        rhd2000.savefile.SignalGroup(obj.board, sg_index - 1);
end

end
