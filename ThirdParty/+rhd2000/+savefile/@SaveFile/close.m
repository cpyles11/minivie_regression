function close(obj)
%CLOSE Closes the save file
%
% board.SaveFile.close() writes any unsaved data to the file and closes the 
% save file.
%
% Example:
%      board.SaveFile.Note1 = 'This is a test';
%      board.SaveFile.open(rhd2000.savefile.Format.intan, 'C:\test.rhd');
%      board.SaveFile.close();
%
% See also open.

% Ignore return value, because it's a cleanup function
calllib('RHD2000m', 'CloseSaveFile', obj.board.handle);

end
