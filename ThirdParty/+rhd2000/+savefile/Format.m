classdef Format < uint32
    %FORMAT Enumeration of save file formats
    %
    % The RHD2000 Matlab Toolbox contains support for saving in three 
    % standard data formats:
    %      Traditional Intan File Format 
    %      "One File Per Signal Type" Format 
    %      "One File Per Channel" Format
    %
    % See also rhd2000.savefile.SaveFile.open.
    
    enumeration
        intan(0)                % Traditional Intan File Format 
        file_per_signal_type(1) % "One File Per Signal Type" Format
        file_per_channel(2)     % "One File Per Channel" Format
    end
    
end

