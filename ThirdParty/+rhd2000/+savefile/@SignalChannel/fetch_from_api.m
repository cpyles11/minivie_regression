function obj = fetch_from_api(obj, channel_index_0)
%FETCH_FROM_API Gets the signal channel from the API
%
% Used internally; you shouldn't need to call this directly.

obj.Name_Internal = blanks(200);
[retVal, obj.Name_Internal, obj.Enabled_Internal] = ...
    calllib('RHD2000m', 'GetChannelInfo', ...
            obj.board.handle, obj.sg_index_0, channel_index_0, ...
            length(obj.Name_Internal), obj.Name_Internal, 0);

rhd2000.report_error(retVal);

end
