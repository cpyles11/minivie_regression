classdef SignalChannel < rhd2000.SetGet_light
    %SIGNALCHANNEL Configuration of one signal channel in the save file.
    %
    % Individual channels exist for:
    %      Amplifiers
    %      Auxiliary inputs
    %      Supply voltages
    %      Analog inputs
    %      Digital inputs
    %      Digital outputs
    %
    % Channels can be renamed for inclusion in save files, and individual
    % channels can be enabled/disable. See properties for more information.
    %
    % See also rhd2000.savefile.SignalGroup.

    properties (Dependent = true)
        %NAME The channel's name.
        %
        % Can be renamed for convenience in the save file
        %
        % Example:
        %      board.SaveFile.SignalGroups{1}.Channels{1}.Name = ...
        %                        'Port A, Amplifier 1';
        Name

        %ENABLED Whether to include this channel in the save file
        %
        % Note that this only affects whether or not to save the channel;
        % data for the channel are always acquired, independent of this
        % setting.
        %
        % Example:
        %      board.SaveFile.SignalGroups{1}.Channels{1}.Enabled = true;
        Enabled
    end
    
    properties (Access = private, Hidden = true)
        Name_Internal
        Enabled_Internal
        board
        sg_index_0
    end
    
    methods
        function obj = SignalChannel(board, sg_index_0, channel_index_0)
            if (nargin == 3)
                obj.board = board;
                obj.sg_index_0 = sg_index_0;
                obj.fetch_from_api(channel_index_0);
            end
        end
        
        function value = get.Name(obj)
            value = obj.Name_Internal;
        end
        
        function obj = set.Name(obj, value)
            if ~strcmp(value, obj.Name_Internal)
                [retVal, ~, ~] = calllib('RHD2000m', 'RenameChannel', ...
                                obj.board.handle, obj.sg_index_0, ...
                                obj.Name_Internal, value);
                rhd2000.report_error(retVal);
                           
            end
            obj.Name_Internal = value;
        end
        
        function value = get.Enabled(obj)
            value = obj.Enabled_Internal;
        end
        
        function obj = set.Enabled(obj, value)
            if value ~= obj.Enabled_Internal
                retVal = calllib('RHD2000m', 'SetSaveDataEnabled', ...
                                  obj.board.handle, obj.sg_index_0, ...
                                  obj.Name_Internal, value);
                rhd2000.report_error(retVal);
            end            
            obj.Enabled_Internal = value;
        end
        
    end
    
    methods (Access = private, Hidden = true)
        obj = fetch_from_api(obj, channel_index_0)
    end
    
end

