classdef Port < uint32
    %PORT Enumeration of the four ports on the board (A-D)
    %   Detailed explanation goes here
    
   enumeration
       A(0)
       B(1)
       C(2)
       D(3)
   end
end

