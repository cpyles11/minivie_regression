classdef Chip < uint32
    %Chip Enumeration of RHD2000-series chips
    %
    % Used for indicating which type of chip is attached to which
    % datasource, and how many amplifier channels it has.
    %
    % See also rhd2000.Board.Chips.
    
    methods
        %NUM_CHANNELS Number of amplifier channels on the chip
        %
        % Example:
        %      n = chip.num_channels();
        function n = num_channels(obj)
            switch (obj)
                case rhd2000.Chip.rhd2132
                    n = 32;
                case rhd2000.Chip.rhd2216
                    n = 16;
                case rhd2000.Chip.rhd2164
                    n = 64;
                otherwise
                    n = 0;
            end
        end
    end
    
    enumeration
        none(0)    % No chip
        rhd2132(1) % RHD2132 chip with 32 unipolar inputs
        rhd2216(2) % RHD2216 chip with 16 differential inputs
        rhd2164(4) % RHD2164 chip with 64 unipolar inputs
    end
    
end

