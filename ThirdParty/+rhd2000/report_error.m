function report_error( error_code )
%REPORT_ERROR Errors if error_code is non-zero.
%
% This function is called internally in the lowlevel functions; you
% probably don't need to call it explicitly.
%
% The lowlevel functions of the RHD2000 Matlab Toolbox all return error
% codes whenever error conditions occur.  (Error code 0 means 'no error'.)
% REPORT_ERROR checks whether or not an error occurred (i.e., error_code 
% is non-zero).  
%
% If no error occurred, REPORT_ERROR does nothing, i.e.
%     report_error(0)
% is a no-op.
%
% If an error occurred, REPORT_ERROR calls 'error' with an appropriate
% error message.  For example:
%     report_error(-2147009597) 
% calls 
%     error('The requested system device cannot be found.')
%
% See also error_message.

if (error_code ~= 0)
    % Convert error code to text
    tmp = blanks(300);
    [~, error_string] = calllib('RHD2000m', 'ErrorMessage', ...
                                int32(error_code), length(tmp), tmp);
    
    error(error_string);
end

end

