classdef DataBlock < rhd2000.SetGet_light
    %DATABLOCK A synchronized block of data samples.
    %
    % The RHD2000 Matlab Toolbox provides a way to read synchronized data 
    % from the RHD2000 evaluation board and all RHD2000 series chips 
    % attached to it.
    %
    % Data is read as a series of data blocks, each of which contains 
    % 60 samples of various kinds of data. After reading the data 
    % (rhd2000.Board.read_next_data_block), you access the various 
    % properties of this class to extract the data from the data block. 
    %
    % The class is optimized so that only properties that you use are
    % converted to Matlab format.  For example, if you don't access the
    % ROM values, no time is spent converting.
    %
    % See properties and members for more information.
    %
    % See also rhd2000.Board.read_next_data_block.
    
    properties (SetAccess = private)
        %CHIPS Chip data from the current data block.
        %
        % Chips is a cell array of size 8, one entry for each datasource
        % (Port A, MISO 1 ... Port D, MISO 2).  Non-existent chips are
        % indicated by nulls; existing chips are indicated by Chip objects.
        %
        % Example:
        %     port_a_miso_1_chip = datablock.Chips{1};
        %     port_b_miso_1_chip = datablock.Chips{3};
        %
        % See <a href="matlab:web(fullfile(fileparts(fileparts(fileparts(which('rhd2000.datablock.DataBlock')))),'Documentation','html','DataSources.html'))">Data Sources</a>
        %
        % See also rhd2000.datablock.Chip.
        Chips
    end
    
    properties (Dependent = true, SetAccess = private)
        %TIMESTAMPS Timestamps from the current data block.
        %
        % A 1x60 array of timestamps from the current data block (60 
        % samples per data block).  Timestamps are increasing integers 
        % (e.g., 1, 2, 3, 4), incremented one per sample.  Note that 
        % timestamps are 32-bit integers, so they wrap around at some 
        % point; this may need to be corrected for when doing very long 
        % (multi-day) experiments.
        %
        % Example:
        %     timestamps = datablock.Timestamps;
        Timestamps

        %BOARD Evalution board data from the current data block.
        %
        % See also rhd2000.datablock.Board.
        Board
    end
    
    properties (Access = private, Hidden = true)
        Timestamps_Internal
        Board_Internal
        board_handle
    end
    
    properties (Hidden = true)
        handle
    end
    
    properties (Hidden = true, SetAccess = private)
        HasData
    end
    
    methods
        function obj = DataBlock(board)
        %Constructor; don't call directly, call rhd2000.Board.read_next_data_block
            obj.HasData = false;
            if nargin > 0
                obj.handle = 0;
                obj.board_handle = board; % needed for ref-counting
                
                obj.Chips = cell(8, 1);
                for datasource = 1:8
                    chip = board.Chips(datasource);
                    if (chip ~= rhd2000.Chip.none)
                        obj.Chips{datasource} = ...
                            rhd2000.datablock.Chip(obj, ...
                                datasource - 1, chip.num_channels);
                    end
                end
            end
        end
        
        function delete(obj)
        %DELETE Frees the data block.
            % Ignore return value, because it's a cleanup function
            calllib('RHD2000m', 'FreeDataBlock', obj.handle);
        end
        
        save(obj, timestamp_offset)
        read_next(obj, board)

        function value = get.Timestamps(obj)
            if isempty(obj.Timestamps_Internal)
                obj.Timestamps_Internal = zeros(1,60);
            end
            [retVal, obj.Timestamps_Internal] = ...
                calllib('RHD2000m', 'GetDatablockTimestamps', ...
                        obj.handle, obj.Timestamps_Internal);
            rhd2000.report_error(retVal);
            
            value = obj.Timestamps_Internal;
        end
        
        function value = get.Board(obj)
            if isempty(obj.Board_Internal)
                obj.Board_Internal = rhd2000.datablock.Board(obj);
            end
            value = obj.Board_Internal;
        end
    end
    
end

