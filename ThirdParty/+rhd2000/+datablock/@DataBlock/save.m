function save(obj, timestamp_offset)
%SAVE Saves the data block to disk.
%
% datablock.save(timestamp_offset) saves with offset timestamps.
% Specifically, timestamp_offset is added to all timestamps in the data 
% block prior to writing to file. For example, if an event of interest 
% happened at timestamp 12, you could set this value to -12, and that 
% would change the timestamps so that the event of interest is at 
% timestamp 0.
%
% datablock.save() saves with no timestamp offset.
%
% Note that the save file is the one for the board that created this data
% block; there is no mechanism to write to a different save file.  Also
% note that the save file must be open prior to saving the data block to
% it.
%
% Example:
%      board.SaveFile.open(rhd2000.savefile.Format.intan, 'C:\\test.rhd');
%      board.run_fixed(1)
%      datablock = board.read_next_data_block();
%      datablock.save();
%      board.SaveFile.close();
%
% See also rhd2000.savefile.SaveFile.

if nargin == 1
    timestamp_offset = 0;
end

retVal = calllib('RHD2000m', 'WriteDatablockToSaveFile', ...
                 obj.handle, timestamp_offset);
rhd2000.report_error(retVal);

end

