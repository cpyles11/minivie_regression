function read_next(obj, board)
%READ_NEXT Read next data block
%
% If you're just trying to read, see read_next_data_block in the 'See also'
% at the bottom.
% 
% datablock.read_next(board) reads the next datablock from the board
% (in-place).  This provides an efficient way to read, reusing the
% datablock object again and again.
%
% The board parameter should be the same board that was used to create the
% data block.  Strange things may happen otherwise.
%
% rhd2000.Board's read_next_data_block function uses this internally; that
% function is a bit safer (it checks for out of data conditions and that
% kind of thing) - the current function is useful if efficiency is the
% utmost concern, and if you've checked the other conditions in
% read_next_data_block yourself.
%
% Example:
%    datablock = rhd2000.datablock.DataBlock(board);
%
%    board.run_continuously();
%    for x=1:100
%        datablock.read_next(board);
%    end
%    board.stop();
%
% See also rhd2000.Board.read_next_data_block.



% Clear the previous data block (if any); it's okay to call
% FreeDataBlock on 0; it just doesn't do anything.

% Ignore return value, because it's a cleanup function
calllib('RHD2000m', 'FreeDataBlock', obj.handle);

[retVal, obj.handle] = ...
    calllib('RHD2000m', 'ReadNextDataBlock', board.handle, 0);
rhd2000.report_error(retVal);

obj.HasData = (obj.handle ~= 0);

end
