classdef Chip  < rhd2000.SetGet_light
    %CHIP Data from one RHD2000 series chip from the current data block.
    %
    % The class is optimized so that only properties that you use are
    % converted to Matlab format.  For example, if you don't access the
    % ROM values, no time is spent converting.
    %
    % See properties and members for more information.
    %
    % See <a href="matlab:web(fullfile(fileparts(fileparts(fileparts(which('rhd2000.datablock.Chip')))),'Documentation','html','Chips.html'))">RHD2000 Series Chip</a>
    
    properties (Dependent = true, SetAccess = private)
        %AMPLIFIERS All amplifier inputs for the current chip and data block.
        %
        % Stored as an Nx60 array.
        %       N rows: one for each amplifier input.  16 for RHD2216
        %               chips, 32 for RHD2132 chips, 64 for RHD2164 chips
        %       60 columns: one for each sample in the data block.
        % Each entry is a voltage, in Volts.
        %
        % Example:
        %      amplifiers = datablock.Chips{1}.Amplifiers;
        Amplifiers

        %AUXINPUTS All auxiliary inputs for the current chip and data block.
        %
        % Stored as a 3x60 array.
        %       3 rows: one for each auxiliary input (auxin1, auxin2,
        %               auxin3)
        %       60 columns: one for each sample in the data block.
        % Each entry is a voltage, in Volts.
        %
        % Example:
        %      auxins = datablock.Chips{1}.AuxInputs;
        AuxInputs

        %TEMPERATURE Temperature for the current chip and data block.
        %
        % Temperature is the value of the RHD2000 series chip's temperature 
        % sensor, in C. Each chip returns a single temperature reading per 
        % data block.
        %
        % Example:
        %      if datablock.Chips{1}.Temperature > 50
        %           error 'Chip #1 is very hot.';
        %      end
        Temperature

        %SUPPLYVOLTAGE Supply Voltage for the current chip and data block.
        %
        % SupplyVoltage is the value of the RHD2000 series chip's supply 
        % voltage sensor, in Volts. Each chip returns a single voltage 
        % reading per data block.
        %
        % Example:
        %      if datablock.Chips{1}.SupplyVoltage < 3.0
        %           error 'Chip #1''s supply voltage is very low.';
        %      end
        SupplyVoltage

        %ROM ROM registers for the current chip and data block.
        %
        % Example:
        %      rom = datablock.Chips{1}.ROM;
        %
        % The returned rom structure consists of:
        %      company              contains the characters INTAN in ASCII. The 
        %                           contents of these registers can be read to 
        %                           verify the fidelity of the SPI interface. 
        %      chipName             contains the null-terminated chip name (e.g. 
        %                           'RHD2132\0') in ASCII.
        %      miso_ab_marker       returns 00110101 (decimal 53) on MISO A and 
        %                           00111010 (decimal 58) on MISO B. These distinct 
        %                           bytes can be checked by the SPI master device 
        %                           to confirm signal integrity on the SPI bus 
        %                           (e.g., to adjust internal sampling times to 
        %                           compensate for cable propagation delay). 
        %      die_revision         encodes a die revision number which is set by 
        %                           Intan Technologies to encode various versions 
        %                           of a chip. 
        %      unipolar             is true if the amplifiers have unipolar inputs 
        %                           and a common reference, like the RHD2132 chip 
        %                           or RHD2164 chip. It is false if the on-chip 
        %                           biopotential amplifiers have independent 
        %                           differential (bipolar) inputs like the 
        %                           RHD2216 chip. 
        %      number_of_amplifiers encodes the total number of biopotential 
        %                           amplifiers on the chip (e.g., 64). 
        %      chip_id              encodes a unique Intan Technologies ID number 
        %                           indicating the type of chip. These correspond
        %                           to the enumeration values of rhd2000.Chip.        
        ROM
    end
    
    properties (Access = private, Hidden = true)
        datablock
        datasource
        num_amplifiers
        Amplifiers_Internal
        AuxInputs_Internal
    end
    
    methods
        function obj = Chip(datablock, datasource, num_amplifiers)
            if nargin > 0
                obj.datablock = datablock;
                obj.datasource = datasource;
                obj.num_amplifiers = num_amplifiers;
            end
        end
        
        function value = get.Amplifiers(obj)
            if isempty(obj.Amplifiers_Internal)
                obj.Amplifiers_Internal = zeros(obj.num_amplifiers, 60);
            end

            [retVal, obj.Amplifiers_Internal] = ...
                calllib('RHD2000m', ...
                   'GetDatablockAllAmplifierChannelsColumnMajor', ...
                   obj.datablock.handle, obj.datasource, obj.Amplifiers_Internal);

            rhd2000.report_error(retVal);

            value = obj.Amplifiers_Internal;
        end
        
        function value = get.AuxInputs(obj)
            if isempty(obj.AuxInputs_Internal)
                obj.AuxInputs_Internal = zeros(3, 15);
            end

            [retVal, obj.AuxInputs_Internal] = calllib('RHD2000m', ...
                    'GetDatablockAllAuxInputsColumnMajor', ...
                    obj.datablock.handle, obj.datasource, obj.AuxInputs_Internal);

            rhd2000.report_error(retVal);
                
            value = obj.AuxInputs_Internal;
        end
        
        function value = get.Temperature(obj)
            [retVal, value] = ...
                calllib('RHD2000m', 'GetDatablockTemperature', ...
                        obj.datablock.handle, obj.datasource, 0);

            rhd2000.report_error(retVal);
        end
        
        function value = get.SupplyVoltage(obj)
            [retVal, value] = ...
                calllib('RHD2000m', 'GetDatablockSupplyVoltage', ...
                        obj.datablock.handle, obj.datasource, 0);

            rhd2000.report_error(retVal);
        end
        
        function value = get.ROM(obj)
            value = obj.get_datablock_rom();
        end
        
        % In other files
        values = get_one_amplifier(obj, amplifier)
        values = get_one_aux_input(obj, auxin)
    end
    
    methods (Access = private, Hidden = true)
        rom = get_datablock_rom(obj)
    end
end

