function values = get_one_aux_input(obj, auxin)
%GET_ONE_AUX_INPUT One auxiliary analog input from the current data block.
%
% result = datablock.Chips{index}.get_one_aux_input(auxin) 
% returns one data block (60 samples) worth of analog input data for a 
% given auxiliary analog input.
%     auxin            selects one of the auxiliary analog inputs 
%                      1 => auxin1, 2 => auxin2, or 3 => auxin3.
%     result           is an array of size 60.  Each entry is a 
%                      voltage, in Volts.
%
% Compare with rhd2000.datablock.DataBlock class's Chips{n}.AuxInputs 
% property.  That property returns the values of all three auxiliary analog
% inputs on a given chip simultaneously. It is possible that there would be 
% some performance improvement calling the current function, if you only 
% need the value of one auxiliary analog input.
%
% Example:
%      datasource = 1; % Port A, MISO 1
%      auxin = 1; % Auxin1
%      result = datablock.Chips{datasource}.get_one_aux_input(auxin);
%
% See also rhd2000.datablock.Chip.AuxInputs.

values = zeros(1, 60);
[retVal, values] = calllib('RHD2000m', 'GetDatablockAuxInput', ...
                            obj.datablock.handle, obj.datasource, ...
                            auxin, values);

rhd2000.report_error(retVal);

end

