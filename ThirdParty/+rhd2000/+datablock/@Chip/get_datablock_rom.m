function rom = get_datablock_rom(obj)
%GET_DATABLOCK_ROM ROM registers from the current data block.
%
% Caled internally
%
% obj.get_datablock_rom() 
% fills in the contents of the ROM registers on the chip at the given
% datasource from the current data block.
%
% See also rhd2000.datablock.ROM.

rom.company = blanks(8);
rom.chipName = blanks(8);
[retVal, rom.company, rom.chipName, rom.miso_ab_marker, ...
 rom.die_revision, rom.unipolar, rom.number_of_amplifiers, ...
 rom.chip_id] = calllib('RHD2000m', 'GetDatablockROM', ...
                        obj.datablock.handle, obj.datasource, ...
                        rom.company, rom.chipName, 0, 0, 0, 0, 0);

rhd2000.report_error(retVal);

end

