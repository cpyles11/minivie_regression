function values = get_one_amplifier( obj, channel )
%GET_ONE_AMPLIFIER One amplifier channel from the current chip and data block.
%
% result = datablock.Chips{index}.get_one_amplifier(amplifier) 
% returns one data block (60 samples) worth of amplifier data for a given 
% channel.
%     amplifier        selects which amplifier channel to read. Allowed 
%                      values depend on chip:
%                           RHD2216: 0-15
%                           RHD2132: 0-31
%                           RHD2164: 0-63
%     result           is an array of size 60.  Each entry is a voltage,
%                      in Volts.
%
% Compare with rhd2000.datablock.DataBlock class's Chips{n}.Amplifiers 
% property.  That property returns the values of all amplifiers on a given 
% chip simultaneously. It is possible that there would be some performance 
% improvement calling the current function, if you only need the value of 
% one amplifier.  The Amplifiers property will be significantly more
% efficient if you need to get all amplifiers, though.
%
% Example:
%      datasource = 1; % Port A, MISO 1
%      amplifier = 15;
%      result = datablock.Chips{datasource}.get_one_amplifier(amplifier);
%
% See also rhd2000.datablock.Chip.Amplifiers.

values = zeros(1, 60);
[retVal, values] = calllib('RHD2000m', 'GetDatablockAmplifierChannel', ...
                            obj.datablock.handle, obj.datasource, ...
                            channel, values);

rhd2000.report_error(retVal);


end

