function value = get_one_adc(obj, adc)
%GET_ONE_ADC One ADC input from the current data block.
%
% result = datablock.Board.get_one_adc(adc) returns
% one data block (60 samples) worth of a given Analog to Digital Converter 
% on the RHD2000 Evaluation Board.
%     adc              is the 0-based index of the ADC.  Valid values 0-7.
%     result           is an array of size 60.  Each entry is a voltage,
%                      in Volts.
%
% Compare to rhd2000.datablock.DataBlock class's Board.ADCs property.  That
% property gets the values of all ADCs simultaneously, while this function
% gets a single ADC's values.  It is possible that there would be some 
% slight performance improvement calling the current function, if you only 
% need the value of one ADC, but if you need all eight, the other method 
% will almost certainly be faster.
%
% Example:
%      adc3 = datablock.Board.get_one_adc(2);
%
% See also rhd2000.datablock.Board.ADCs.

value = zeros(1, 60);
[retVal, value] = calllib('RHD2000m', 'GetDatablockADCs', ...
                            obj.datablock.handle, adc, value);

rhd2000.report_error(retVal);

end
