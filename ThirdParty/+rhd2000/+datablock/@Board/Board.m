classdef Board  < rhd2000.SetGet_light
    %BOARD Evalution board data from the current data block.
    %
    % The class is optimized so that only properties that you use are
    % converted to Matlab format.  For example, if you don't access the
    % ADC values, no time is spent converting.
    %
    % See properties and members for more information.
    %
    % See <a href="matlab:web(fullfile(fileparts(fileparts(which('rhd2000.Board'))),'Documentation','html','EvaluationBoard.html'))">RHD2000 Evaluation Board</a>
    
    properties (Dependent = true, SetAccess = private)
        %ADCS All ADC inputs from the current data block.
        %
        % Stored as an 8x60 array.
        %       8 rows: one for each ADC input
        %       60 columns: one for each sample in the data block.
        % Each entry is a voltage, in Volts.
        %
        % Example:
        %      adcs = datablock.Board.ADCs;
        ADCs
        
        %DIGITALINPUTS All digital inputs from the current data block.
        %
        % Stored as a 16x60 array.
        %       16 rows: one for each digital input
        %       60 columns: one for each sample in the data block.
        % Each entry is a 0 or 1.
        %
        % Example:
        %      digital_in = datablock.Board.DigitalInputs;
        %
        % See <a href="matlab:web(fullfile(fileparts(fileparts(which('rhd2000.Board'))),'Documentation','html','DigitalInputs.html'))">Digital Inputs</a>
        DigitalInputs
        
        %DIGITALOUTPUTS All digital outputs from the current data block.
        %
        % Stored as a 16x60 array.
        %       16 rows: one for each digital output
        %       60 columns: one for each sample in the data block.
        % Each entry is a 0 or 1.
        %
        % Example:
        %      digital_out = datablock.Board.DigitalOutputs;
        DigitalOutputs
    end
    
    properties (Access = private, Hidden = true)
        datablock
        ADCs_Internal
        DigitalInputs_Internal
        DigitalOutputs_Internal
    end
    
    methods
        function obj = Board(datablock)
            if nargin > 0
                obj.datablock = datablock;
            end
        end
        
        function value = get.ADCs(obj)
            if isempty(obj.ADCs_Internal)
                obj.ADCs_Internal = zeros(8,60);
            end
            [retVal, obj.ADCs_Internal] = calllib('RHD2000m', ...
                'GetDatablockAllADCsColumnMajor', ...
                obj.datablock.handle, obj.ADCs_Internal);

            rhd2000.report_error(retVal);

            value = obj.ADCs_Internal;
        end
        
        function value = get.DigitalInputs(obj)
            if isempty(obj.DigitalInputs_Internal)
                obj.DigitalInputs_Internal = zeros(16,60);
            end
            
            tmp = zeros(1,60);
            [retVal, tmp] = calllib('RHD2000m', ...
                'GetDatablockDigitalIn', obj.datablock.handle, tmp);
            rhd2000.report_error(retVal);

            for digital_in=1:16
                obj.DigitalInputs_Internal(digital_in, :) = bitget(tmp, digital_in);
            end
            value = obj.DigitalInputs_Internal;
        end
        
        function value = get.DigitalOutputs(obj)
            if isempty(obj.DigitalOutputs_Internal)
                obj.DigitalOutputs_Internal = zeros(16,60);
            end
            
            tmp = zeros(1,60);
            [retVal, tmp] = calllib('RHD2000m', ...
                'GetDatablockDigitalOut', obj.datablock.handle, tmp);
            rhd2000.report_error(retVal);

            for digital_out=1:16
                obj.DigitalOutputs_Internal(digital_out, :) = bitget(tmp, digital_out);
            end                
            value = obj.DigitalOutputs_Internal;
        end
        
        % Defined in other files
        value = get_one_adc(obj, adc)
    end
    
end

