classdef SamplingRate < uint32
    %SAMPLINGRATE Enumeration of the allowed sampling rates.
    %
    % See also frequency, rhd2000.Board.SamplingRate.
    
    methods
        function hertz = frequency(obj)
        %FREQUENCY Returns the sampling frequency (in Hz) as a double.
        %
        % result_in_hertz = sampling_rate.frequency() returns the
        % frequency as a double in Hz.
        %
        % Example:
        %      board.SamplingRate.frequency()
            switch obj
                case rhd2000.SamplingRate.rate1000
                    hertz = 1000;
                case rhd2000.SamplingRate.rate1250
                    hertz = 1250;
                case rhd2000.SamplingRate.rate1500
                    hertz = 1500;
                case rhd2000.SamplingRate.rate2000
                    hertz = 2000;
                case rhd2000.SamplingRate.rate2500
                    hertz = 2500;
                case rhd2000.SamplingRate.rate3000
                    hertz = 3000;
                case rhd2000.SamplingRate.rate3333
                    hertz = 3333;
                case rhd2000.SamplingRate.rate4000
                    hertz = 4000;
                case rhd2000.SamplingRate.rate5000
                    hertz = 5000;
                case rhd2000.SamplingRate.rate6250
                    hertz = 6250;
                case rhd2000.SamplingRate.rate8000
                    hertz = 8000;
                case rhd2000.SamplingRate.rate10000
                    hertz = 10000;
                case rhd2000.SamplingRate.rate12500
                    hertz = 12500;
                case rhd2000.SamplingRate.rate15000
                    hertz = 15000;
                case rhd2000.SamplingRate.rate20000
                    hertz = 20000;
                case rhd2000.SamplingRate.rate25000
                    hertz = 25000;
                case rhd2000.SamplingRate.rate30000
                    hertz = 30000;
            end
        end
    end
    
    enumeration
        rate1000(0)   % 1,000 Hz
        rate1250(1)   % 1,250 Hz
        rate1500(2)   % 1,500 Hz
        rate2000(3)   % 2,000 Hz
        rate2500(4)   % 2,500 Hz
        rate3000(5)   % 3,000 Hz
        rate3333(6)   % 3,333 Hz
        rate4000(7)   % 4,000 Hz
        rate5000(8)   % 5,000 Hz
        rate6250(9)   % 6,250 Hz
        rate8000(10)  % 8,000 Hz
        rate10000(11) % 10,000 Hz
        rate12500(12) % 12,500 Hz
        rate15000(13) % 15,000 Hz
        rate20000(14) % 20,000 Hz
        rate25000(15) % 25,000 Hz
        rate30000(16) % 30,000 Hz
    end
    
end

