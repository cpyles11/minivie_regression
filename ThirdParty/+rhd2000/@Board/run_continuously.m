function run_continuously(board)
%RUN_CONTINUOUSLY Starts the board running, and runs it in continuous mode.
%
% board.run_continuously() runs the board in continuous mode.
%
% Caution: The board will run indefinitely in this mode. Be sure that 
% whatever logic you implement always calls board.stop() (or board.close())
% to stop the board running, even if an error occurred.  
%
% Caution: Care should be taken in this mode to handle the incoming data 
% promptly (via read_next_data_block). The board gets into a weird state 
% if its FIFO fills up.
%
% Tip: You can (and should) check whether you are reading data fast enough 
% by using FIFOLag and FIFOPercentageFull. If you have a lot of 
% channels (e.g., if you're using 4 RHD2164 boards) and you're running at 
% a high sampling rate (e.g., 30 kS/s) and you're doing significant 
% processing and saving, you may have trouble keeping up with the incoming 
% data with the default settings. In that case, you should increase the 
% input buffer size:
%      call board.get_configuration_parameters()
%      change the Driver.NumBlocksToRead property (e.g., double it)
%      call board.set_configuration_parameters()  
%
% Example:
%      driver = rhd2000.Driver;
%      board = driver.create_board();
%      board.run_continuously();
%      ... % process data blocks as they occur
%      board.stop();
%
% See also stop, close, read_next_data_block, FIFOLag, FIFOPercentageFull,
% get_configuration_parameters, set_configuration_parameters, 
% rhd2000.configuration.Driver.NumBlocksToRead.

retVal = calllib('RHD2000m', 'RunContinuously', board.handle);

rhd2000.report_error(retVal);

end

