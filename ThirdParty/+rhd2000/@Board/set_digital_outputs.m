function board = set_digital_outputs(board, value)
%SET_DIGITAL_OUTPUTS Set digital outputs
%
% board = set_digital_outputs(board, value) sets the digital outputs.
% Value should be an array of length 16.
%
% Used internally; you shouldn't be calling this directly.
%
% See also rhd2000.Board.DigitalOutputs.

if length(value) ~= 16
    error('value must be an array of length 16');
end

sz = size(value);
if sz(1) == 16 && sz(2) == 1
    value = value';
end

if board.handle ~= 0
    if board.Comparators_Enabled
        % Ignore 1:8, only use 9:16
        short_list = value(9:16);
        
        % Write to board
        [retVal, ~] = calllib('RHD2000m', 'Write8DigitalOutputs', ...
                                board.handle, short_list);
        rhd2000.report_error(retVal);
        
        % Adjust in-memory data structure
        board.DigitalOutputs_Internal = [NaN(1,8) short_list];            
    else
        % Write to board
        [retVal, ~] = calllib('RHD2000m', 'Write16DigitalOutputs', ...
                                board.handle, value);

        rhd2000.report_error(retVal);
        
        % Adjust in-memory data structure
        board.DigitalOutputs_Internal = value;            
    end
else
    board.DigitalOutputs_Internal = value;            
end

end

