function reset(board)
%RESET Resets an RHD2000 Evaluation Board.
%
% Example:
%      driver = rhd2000.Driver;
%      board = driver.create_board();
%      board.reset();
%
% See also rhd2000.Driver.reset.

% Ignore return value, because it's a cleanup function
calllib('RHD2000m', 'ResetBoard', board.handle);

end
