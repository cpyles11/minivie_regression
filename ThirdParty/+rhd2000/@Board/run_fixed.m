function run_fixed(board, number_of_datablocks)
%RUN_FIXED Runs the board for a fixed number of time steps.
%
% board.run_fixed(number_of_datablocks) runs the board for 
% num_datablocks * 60 time steps.  (Each data block contains 60 samples.) 
% After calling this function, you could call read_next_data_block exactly 
% number_of_datablocks times.
%
% Note: You don't need to call stop after running this function; it stops 
% automatically when the given number of time steps are finished.
%
% Caution: As long as number_of_datablocks isn't too big, this function 
% automatically sets the number of data blocks to read to 
% number_of_datablocks, so that a single call to read_next_data_block will 
% fetch all the data generated from the board into computer memory (you 
% will, of course, need to call read_next_data_block multiple times to 
% return that data to Matlab). You should reset the value (either manually 
% via get_configuration_parameters or automatically by setting SamplingRate), 
% if you're planning to call run_continuously after calling this function. 
% The limit on the queue size (but not on reading) is 2048 data blocks. 
%
%
% Example:
%      driver = rhd2000.Driver;
%      board = driver.create_board();
%      board.run_fixed(1)
%      datablock = board.read_next_data_block();
%
% See also run_continuously, stop, read_next_data_block, 
% get_configuration_parameters, SamplingRate.

retVal = calllib('RHD2000m', 'RunFixed', board.handle, ...
                 number_of_datablocks);

rhd2000.report_error(retVal);

end

