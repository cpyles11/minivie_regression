function manual_fast_settle(board, enabled)
%MANUAL_FAST_SETTLE Causes the amplifiers to start/stop fast settling.
%
% board.manual_fast_settle(true) begins fast settling and continues until
% board.manual_fast_settle(false) is called
%
% Details:
% All RHD2000 series chips have a hardware 'fast settle' function that 
% rapidly resets the analog signal path of each amplifier channel to zero 
% to prevent (or recover from) saturation caused by large transient input 
% signals such as those due to nearby stimulation. Recovery from amplifier 
% saturation can be slow when the lower bandwidth is set to a low frequency 
% (e.g., 1 Hz).
%
% Fast settle can be configured in one of three ways:
%      Disabled      No fast settling; normal amplifier operation. 
%      Manual        Fast settling will begin immediately and will continue 
%                    until manual mode is exited. 
%      Real-time     Fast setting is linked to one of the digital inputs. 
%                    Fast settling will occur whenever the selected digital 
%                    input goes high and will continue until the selected 
%                    digital input goes low.
%
% board.manual_fast_settle(true) switches the chips to Manual mode.
%
% board.manual_fast_settle(false) switches the chips to Disabled mode.
%
% See 'See also' for configuring real-time mode.
%
% Example:
%      board.DigitalOutputs = ones(1,16); % stimulation
%      board.manual_fast_settle(true);
%      pause(1);
%      board.DigitalOutputs = zeros(1,16); % stimulation ends
%      board.manual_fast_settle(false);
%
% See also rhd2000.configuration.Chip.FastSettle.

retVal = calllib('RHD2000m', 'SetFastSettle', board.handle, ...
                    enabled, false, 0);
rhd2000.report_error(retVal);

end        
