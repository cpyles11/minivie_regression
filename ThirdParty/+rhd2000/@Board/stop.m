function stop( board )
%STOP Stops the board from running.
%
% board.stop() stops the board from running.  Only boards that are running 
% continuously (see run_continuously) need to be stopped.
%
% Note: Does not flush the board's FIFO. This allows you to read the 
% remaining data from the FIFO if you want.  
%
% Note: A board is automatically stopped when it is closed.
%
% Example:
%      driver = rhd2000.Driver;
%      board = driver.create_board();
%      board.run_continuously();
%      ... % process data blocks as they occur
%      board.stop();
%
% See also run_continuously, close.

% Ignore return value, because it's a cleanup function
calllib('RHD2000m', 'Stop', board.handle);

end

