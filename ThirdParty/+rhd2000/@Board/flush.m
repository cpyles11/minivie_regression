function flush( board )
%FLUSH Flush the board's FIFO.
%
% board.flush() flushes the on-board FIFO buffer that contains
% data that has been acquired but not yet returned to the computer.
%
% This function can be called after you call board.stop(), to
% clear out any remaining data stored in the board's FIFO.
%
% A board's FIFO is automatically flushed when you close the board.
%
% Example:
%      driver = rhd2000.Driver;
%      board = driver.create_board();
%      board.run_continuously();
%      board.stop();
%      board.flush();
%
% See also stop, reset, close.

% Ignore return value, because it's a cleanup function
calllib('RHD2000m', 'Flush', board.handle);

end