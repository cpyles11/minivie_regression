function end_plating(board)
%END_PLATING Ends "plating mode" and configures the chips for acquisition.
%
% board.end_plating(channel) disables the elec_test pin of the RHD2000 series
% chips.
%
% Example:
%      driver = rhd2000.Driver;
%      board = driver.create_board();
%      board.begin_plating();
%      ... % deliver elec_test pulse
%      board.end_plating();
%
% See also begin_plating.

% The library function 'EndImpedanceMeasurement' does what we want; there's
% no separate EndPlating.
retVal = calllib('RHD2000m', 'EndImpedanceMeasurement', board.handle);
rhd2000.report_error(retVal);

end

