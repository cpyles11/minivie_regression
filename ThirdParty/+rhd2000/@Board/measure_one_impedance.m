function  [impedance, actual_frequency] = measure_one_impedance(board, frequency, datasource, channel)
%MEASURE_ONE_IMPEDANCE Measures electrode impedance for a given electrode
%
% [impedances, actual_frequency] = board.measure_impedances(desired_frequency, datasource, channel) 
% measures the impedance of the electrode at (datasource, channel) at a 
% frequency close to the desired_frequency frequency and returns both the 
% measured impedance and the actual frequency at which the measurement took place.
% Here channel is 0-based (e.g., 0-31, not 1-32).
%
% Note that this measurement may take a while, especially for low
% frequencies.  The LEDs on the RHD2000 Evaluation Board will indicate that
% the measurement is occurring.  At the end of the measurement, the LEDs
% are not reset, so if you chain calls to this function, it will continue
% to increment the LEDs.  You may want to clear the LEDs yourself after
% running this function.
%
% Tip: The impedance frequency should be between lower bandwidth x 1.5 and 
% upper bandwidth / 1.5. (Lower bandwidth may be set by either the analog 
% filters on the RHD2000 series chip or the DSP.) The impedance period 
% (1/frequency) must be at least 4 samples and at most 1024 samples. 
% This function will produce an error if desired frequency is not within 
% those bounds. The returned actual_frequency value is the closest
% frequency that was achievable with the current settings.
% 
% Example (with a board on Port A, MISO 1, measuring its channel 15:
%       driver = rhd2000.Driver;
%       board = driver.create_board();
%       [impedance, actual_frequency] = board.measure_one_impedance(1000, 0, 15)
%
% impedance = 
%    6.3275e+06 - 1.0999e+07i
%
% actual_frequency =
%    1000
%
% The returned impedance is a complex number.
%
% To get the magnitudes in ohms, use the abs command.  For example:
%       abs(impedance)
% To get the phase in degrees, use the angle command.  For example:
%       angle(impedance)*180/pi
%
% See <a href="matlab:web(fullfile(fileparts(fileparts(which('rhd2000.Board'))),'Documentation','html','MeasuringImpedance.html'))">Measuring Impedance</a>
%
% See also abs, angle.

% Set impedance frequency
retVal = calllib('RHD2000m', 'SetImpedanceFreq', board.handle, frequency);
rhd2000.report_error(retVal);

% Get actual_frequency
[retVal, ~, actual_frequency] = ...
    calllib('RHD2000m', 'GetImpedanceFreq', board.handle, 0, 0);
rhd2000.report_error(retVal);

% Measure impedances for all electrodes - takes time
[retVal, magnitude, phase] = calllib('RHD2000m', 'MeasureOneImpedance', ...
                 board.handle, datasource, channel, 0, 0);
rhd2000.report_error(retVal);

impedance = magnitude * exp(1i * phase/180*pi);

end

