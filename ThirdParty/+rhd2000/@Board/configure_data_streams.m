function board = configure_data_streams( board, allowed )
%CONFIGURE_DATA_STREAMS Manually configure internal data streams.
%
% Tip: Typically, data streams are configured automatically when 
% driver.create_board is called. The current function is only needed if 
% autoconfiguration fails.  
%
% The RHD2000 evaluation board can handle at most 256 channels worth of 
% data, divided into data streams of 32 channels each. If more chips are 
% connected than can be configured automatically, this function can be used 
% to choose which chips to use and which not to use manually.
%
% Note: RHD2216 chips count as 32 channels, not 16, for this calculation, 
% as 32 channels is the minimum granularity of data streams.
%
% board.configure_data_streams(allowed) configures the internal data
% streams using only the allowed data sources.
%      allowed    is an array of size 8, with logical elements for each 
%                 data source. Allowed data sources (i.e., true) will be 
%                 used if there is a chip attached; Ignored data sources 
%                 (i.e., false) will not be used, whether or not there is a 
%                 chip attached. 
%
%                 The eight elements of the array correspond to 
%                 'Port A, MISO 1,' 'Port A, MISO 2,' ... 'Port D, MISO 2,'
%                 respectively.
%
% Example:
%      driver = rhd2000.Driver;
%      board = driver.create_board();
%
%      % Configure data streams, but ignore the chip(s) on Port B
%      board.configure_data_streams([1 1 0 0 1 1 1 1]);
%
% See <a href="matlab:web(fullfile(fileparts(fileparts(which('rhd2000.Board'))),'Documentation','html','DataSources.html'))">Data Sources</a>
%
% See also rhd2000.Driver.create_board.

    
if (length(allowed) ~= 8)
    error('The ''allowed'' parameter must be of length 8.');
end

[retVal, ~] = calllib('RHD2000m', 'ConfigureDataStreams', ...
                        board.handle, allowed);

rhd2000.report_error(retVal);
    
end

