function begin_plating(board, channel)
%BEGIN_PLATING Configures the chips for plating
%
% board.begin_plating(channel) enables the elec_test pin of the RHD2000 series
% chips and sets the ZCheck DAC to a DC signal, so that plating
% currents/voltages from elec_test will pass the the appropriate pin.
%
% Example:
%      driver = rhd2000.Driver;
%      board = driver.create_board();
%      board.begin_plating();
%      ... % deliver elec_test pulse
%      board.end_plating();
%
% See also end_plating.

retVal = calllib('RHD2000m', 'BeginPlating', board.handle, channel);
rhd2000.report_error(retVal);

end

