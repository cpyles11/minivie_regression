function board = set_configuration_parameters(board, params)
%SET_CONFIGURATION_PARAMETERS Sets the board configuration.
%
% board.set_configuration_parameters(params) sets the board's configuration
% parameters to the values contained in params, which should be of type 
% rhd2000.configuration.Configuration.
%
% To change settings, call get_configuration_parameters, change the 
% parameters in the returned configuration object, then call this function 
% to change the settings.
%
% Example:
%       params = board.get_configuration_parameters();
%
%       % Set bandwidth cutoffs
%       params.Chip.Bandwidth.DesiredLower = 1;
%       params.Chip.Bandwidth.DesiredUpper = 3000;
%
%       board.set_configuration_parameters(params);
%
% See also get_configuration_parameters,
% rhd2000.configuration.Configuration.


params.save(board);
if board.Comparators_Enabled ~= ...
        params.Board.DigitalOutputs.ComparatorsEnabled
    if board.Comparators_Enabled
        % Currently have 8 Digital Outputs; need 16
        board.DigitalOutputs_Internal = ...
            [ zeros(1,8) board.DigitalOutputs_Internal(9:16)];
    else
        % Currently have 16 Digital Outputs; need 8
        board.DigitalOutputs_Internal = ...
            [ NaN(1,8) board.DigitalOutputs_Internal(9:16)];
    end
    board.Comparators_Enabled = ...
        params.Board.DigitalOutputs.ComparatorsEnabled;
end

end
