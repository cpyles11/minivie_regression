function board = get_chip_types(board, force)
%GET_CHIP_TYPES Lists chip types (e.g., RHD2132) attached to board.
%
% board.get_chip_types() sets board.Chips to a list of chips attached
% to the board.  It uses the results of the last scan of chips.
%
% board.get_chip_types(true) rescans the ports and regenerates the list
% of chips attached to the board.  It sets board.Chips to that list.
%
% Details:
% This function scans SPI Ports A-D to find all connected RHD2000 series 
% chips. It reads the chip ID from on-chip ROM to determine the chip type 
% on each port. 
%
% This process is repeated at all possible MISO delays in the FPGA, and
% the cable length on each port is inferred from this. These optimal cable 
% lengths are stored.  (See rhd2000.configuration.CableDelay.)
%
% Note: The scans are done at 30 kHz, for maximal temporal resolution in 
% cable delays.  
%
% Tip: This function is called internally when a board is created and by rescan.
%
% See also Chips, rhd2000.configuration.CableDelay, rescan.

if nargin == 1
    force = false;
end

tmp = zeros(8,1);
[retVal, tmp] = calllib('RHD2000m', 'GetChipTypes', board.handle, ...
                        force, tmp);

rhd2000.report_error(retVal);

board.Chips = rhd2000.Chip(tmp);

end

