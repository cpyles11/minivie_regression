function datablock = read_next_data_block(board)
%READ_NEXT_DATA_BLOCK Reads the next data block from the board.
%
% datablock = board.read_next_data_block() reads the next data block from 
% the RHD2000 evaluation board and updates the values of FIFOLag and 
% FIFOPercentageFull.
%
% Note: Depending on configuration, more than one data block may be read 
% at a time from the RHD2000 evaluation board into computer memory, and 
% then a single data block will be returned by this function. The RHD2000 
% Matlab Toolbox allows configuration of a queue of data blocks, for more 
% efficient reading (of multiple data blocks at once). See 
% rhd2000.configuration.Driver.NumBlocksToRead for more information.  
%
% Tip: if you are running continuously and you call stop, there may still 
% be some data on the board. You can continue to call this function, and 
% it will continue reading as long as data is available. When no more data 
% is available, it will return a null data block.
%
% See <a href="matlab:web(fullfile(fileparts(fileparts(which('rhd2000.Board'))),'Documentation','html','Reading.html'))">Reading Data</a>
%
% There is another, more efficient way to read, which may be important if
% you're at a high sampling rate (e.g., 30 kHz) and have a lot of channels
% (e.g., 256).  See read_next in the 'See also' section for details.
%
% See also rhd2000.datablock.DataBlock,
% rhd2000.configuration.Driver.NumBlocksToRead,
% rhd2000.datablock.DataBlock.read_next.

    datablock = rhd2000.datablock.DataBlock(board);
    datablock.read_next(board);
    if ~datablock.HasData
        datablock = [];
    end
end

