classdef Board < rhd2000.SetGet_light
    %BOARD Class that represents a single RHD2000 Evaluation Board.
    %
    % You should not create this class directly, but rather use the
    % driver's create_board method.
    %
    % Example:
    %       driver = rhd2000.Driver;
    %       board = driver.create_board();
    %
    % See properties and methods for more information.
    %
    % See <a href="matlab:web(fullfile(fileparts(fileparts(which('rhd2000.Board'))),'Documentation','html','EvaluationBoard.html'))">RHD2000 Evaluation Board</a>
    
    properties
        %HANDLE Board handle
        %
        % You should be able to access all functionality without ever using
        % this handle, but it is provided for convenience when calling 
        % low-level functions; those functions mostly take board_handle as 
        % an input parameter.
        %
        % Example:
        %    rhd2000.lowlevel.measure_all_impedances(board.handle);
        handle
        
        %EVALBOARDMODE Evaluation Board Mode
        %
        % This value is 0 for standard RHD2000 evaluation boards, but
        % may be some other value for different products.
        EvalBoardMode
    end
    
    properties (Dependent = true)
        %DACMANUAL DAC Manual value
        %
        % DacManual is the manual output voltage (in V) for analog outputs 
        % configured to use the "DAC Manual" source. 
        %
        % Individual analog outputs can be configured to read from an 
        % amplifier input channel or from this value. All analog outputs 
        % configured to use "DAC Manual" as their input source will output 
        % the value controlled by this variable.
        %
        % The valid range is -3.3V..+3.3V. 
        %
        % See also set_configuration_parameters.
        DacManual

        %DIGITALOUTPUTS Controls the digital outputs on the board
        %
        % Example:
        %      board.DigitalOutputs = [0 0 0 0 1 1 1 1 0 1 0 1 0 0 1 1];
        %
        % Depending on how the board is configured, either:
        %      1) All 16 digital inputs are user-controlled
        %      2) Digital inputs 0-7 are controlled by low-latency threshold
        %         comparators, and digital inputs 8-15 are user-controlled
        %
        % In case 1, the array will be of length 16, with 0 or 1 in each
        % position.  Setting board.DigitalOutputs will change all 16.
        %
        % In case 2, the array will be of length 16, with NaN in the first
        % 8 positions (i.e., the ones not user-controlled), and 0 or 1 in
        % the remaining 8 positions.  Setting board.DigitalOutputs requires
        % an array of length 16; the first 8 will be ignored in this case.
        %
        % Note that case 2 is the default.
        %
        % The digital outputs are updated immediately, whether or not the 
        % RHD2000 Evaluation Board is running.
        %
        % See also set_configuration_parameters.
        DigitalOutputs

        %LEDS Controls the LEDs on the RHD2000 Evaluation Board.
        %
        % Example:
        %   board.LEDs = [0 1 0 1 0 1 0 1];
        %
        % There are 8 LEDs, so the array should be of size 8.
        %
        % Note: The LEDs are used as a progress indicator when run_continuously 
        % is called (until stop). The progress indicator values will overwrite 
        % anything set with the current function.
        %
        % See also run_continuously, stop.
        LEDs

        %SAMPLINGRATE The current board sampling rate.
        %
        % Example:
        %      board.SamplingRate = rhd2000.SamplingRate.rate20000; 
        %
        % As the sampling rate affects several other parameters, it is
        % recommended that you set it before setting other configuration
        % parameters.
        %
        % See also rhd2000.SamplingRate, set_configuration_parameters.
        SamplingRate
    end
    
    properties (SetAccess = private)
        %CHIPS Array of attached chips
        %
        % When a board object is created, this list is filled in with the
        % chip types attached to each of the 8 datasources (Port A, MISO 1;
        % Port A, MISO 2; Port B, MISO 1; . . . Port D, MISO 2).  The ports
        % can be rescanned with the rescan method.
        %
        % See also rhd2000.Chip, rescan.
        Chips

        %SAVEFILE Controls saving data to disk
        %
        % See <a href="matlab:web(fullfile(fileparts(fileparts(which('rhd2000.Board'))),'Documentation','html','SavingData.html'))">Saving Data</a>
        %
        % See also rhd2000.savefile.SaveFile.
        SaveFile
    end
        
    properties (Dependent = true, SetAccess = private)
        %DIGITALINPUTS The current value of the digital inputs on the board.
        %
        % board.DigitalInputs is an array of size 16.  
        %
        % Example:
        %      result = board.DigitalInputs
        %
        % Note: This property provides an immediate read of the digital 
        % inputs.  So if you read it twice, you might get different values
        % if the digital inputs have changed.
        %
        % Another way to read the digital inputs is with the
        % read_next_data_block function, which provides the digital inputs 
        % synchronized with other inputs (e.g., amplifiers) as well as the 
        % board's digital outputs and analog outputs.  
        %
        % See also read_next_data_block.
        DigitalInputs
        
        %FIFOLAG The lag of the FPGA's FIFO queue.
        %
        % Example:
        %     lag = board.FIFOLag;
        %
        % FIFOLag is the most recently read FIFO lag, in ms. This can be 
        % interpreted as amount of data (in milliseconds worth) that is in 
        % the board's FIFO and has not yet been transferred to the computer. 
        %
        % Note: This value is only updated when data is read from the RHD2000 
        % Evaluation Board. Specifically, when read_next_data_block is called and 
        % the computer-side in-memory queue is full, so more data is read from the 
        % RHD2000 Evaluation Board.  
        %
        % Caution: You should take care to ensure the FIFO doesn't fill up and 
        % overflow; it may require a board reset or even power cycling the 
        % RHD2000 Evaluation Board, and the software on the computer may lock up.  
        %
        % See also read_next_data_block, FIFOPercentageFull.
        FIFOLag
        
        %FIFOPERCENTAGEFULL The percentage full of the FPGA's FIO.
        %
        % Example:
        %      percentage = board.FIFOPercentageFull;
        %
        % FIFOPercentageFull is the most recently read FIFO percentage full.
        %
        % Note: This value is only updated when data is read from the RHD2000 
        % Evaluation Board. Specifically, when read_next_data_block is called and 
        % the computer-side in-memory queue is full, so more data is read from the 
        % RHD2000 Evaluation Board.  
        %
        % Caution: You should take care to ensure the FIFO doesn't fill up and 
        % overflow; it may require a board reset or even power cycling the 
        % RHD2000 Evaluation Board, and the software on the computer may lock up.  
        %
        % See also read_next_data_block, FIFOLag.
        FIFOPercentageFull
    end
    
    properties (Access = private, Hidden = true)
        LEDs_Internal
        DigitalOutputs_Internal
        SamplingRate_Internal
        DacManual_Internal
        Comparators_Enabled
        driver
    end
    
    methods
        function board = Board(driver, serial_number)
        %Constructor; use rhd2000.Driver.create_board instead.
            if nargin == 0
                board.handle = 0;
            else
                if nargin == 1
                    serial_number = [];
                end
                
                [retVal, ~, board.handle] = calllib('RHD2000m', 'Open', ...
                                                    serial_number, 0);
                rhd2000.report_error(retVal);

                [retVal, board.EvalBoardMode] = calllib('RHD2000m', ...
                                                'GetEvalBoardMode', ...
                                                board.handle, 0);
                rhd2000.report_error(retVal);
                
                board.driver = driver;

                board.get_chip_types();
                board.SaveFile = rhd2000.savefile.SaveFile(board);
            end
            board.LEDs_Internal = zeros(1,8);
            board.DacManual_Internal = 0;
            board.Comparators_Enabled = true;
            board.DigitalOutputs_Internal = [NaN(1,8) zeros(1,8)];
        end
        
        function delete(board)
        %DELETE Closes out communication with an RHD2000 Evaluation Board.
            if ~isempty(board.handle)
                % Ignore return value, because it's a cleanup function
                calllib('RHD2000m', 'Close', board.handle);
            end
        end
        
        % Getters and setters
        
        function board = set.LEDs(board, values)
            if (length(values) ~= 8)
                error('values must be of length 8');
            end

            if board.handle ~= 0
                retVal = calllib('RHD2000m', 'SetLEDs', ...
                                    board.handle, values);
                rhd2000.report_error(retVal);
            end
            board.LEDs_Internal = values;
        end
        
        function values = get.LEDs(board)
            values = board.LEDs_Internal;
        end
        
        function values = get.DigitalInputs(board)
            if board.handle ~= 0
                values = zeros(16, 1);
                [retVal, values] = calllib('RHD2000m', ...
                            'ReadDigitalInputs', board.handle, values);
                rhd2000.report_error(retVal);                
            end
        end
        
        function value = get.DacManual(board)
            value = board.DacManual_Internal;
        end

        function board = set.DacManual(board, value)
            if board.handle ~= 0
                retVal = calllib('RHD2000m', 'SetDACManual', ...
                                    board.handle, value);
                rhd2000.report_error(retVal);
            end
            board.DacManual_Internal = value;            
        end        
        
        function value = get.DigitalOutputs(board)
            value = board.DigitalOutputs_Internal;
        end

        function board = set.DigitalOutputs(board, value)
            board = set_digital_outputs(board, value);
        end

        function value = get.FIFOPercentageFull(board)
            [retVal, value] = calllib('RHD2000m', ...
                            'GetFIFOPercentageFull', board.handle, 0);
            rhd2000.report_error(retVal);
        end

        function value = get.FIFOLag(board)
            [retVal, value] = calllib('RHD2000m', 'GetLag', ...
                                        board.handle, 0);
            rhd2000.report_error(retVal);
        end
        
        function value = get.SamplingRate(board)
            if isempty(board.SamplingRate_Internal)
                [retVal, enumerated, ~] = calllib('RHD2000m', ...
                            'GetSamplingRate', board.handle, 0, 0);
                rhd2000.report_error(retVal);

                board.SamplingRate_Internal = ...
                        rhd2000.SamplingRate(enumerated);                
            end
            value = board.SamplingRate_Internal;
        end

        function board = set.SamplingRate(board, value)
            retVal = calllib('RHD2000m', 'SetSamplingRate', ...
                                board.handle, int32(value));
            rhd2000.report_error(retVal);
            
            board.SamplingRate_Internal = value;
        end
        
        % Other methods
        reset(board)
        flush(board)
        run_continuously(board)
        run_fixed(board, number_of_datablocks)
        stop(board)
        self_test(board)
        board = rescan(board)
        estimates = estimate_cable_lengths_in_meters(board)
        manual_fast_settle(board, enabled)
        [impedances, actual_frequency] = measure_impedances(board, frequency)
        [impedance, actual_frequency] = measure_one_impedance(board, frequency, datasource, channel)
        datablock = read_next_data_block(board)
        begin_plating(board, channel)
        end_plating(board)
        
        % Configuration
        params = get_configuration_parameters(board)
        board = set_configuration_parameters(board, params)
        board = configure_data_streams(board, allowed)
        
    end
    
    methods (Access = private, Hidden = true)
        board = set_digital_outputs(board, value)
        board = get_chip_types(board, force)
    end
end

