function params = get_configuration_parameters(board)
%GET_CONFIGURATION_PARAMETERS Gets current board configuration.
%
% params = board.get_configuration_parameters() returns a configuration 
% object that contains the RHD2000 evaluation board's current configuration
% settings.  
%
% To change settings, call this function, change the parameters in the
% object, then call set_configuration_parameters to change the settings.
%
% Example:
%       params = board.get_configuration_parameters();
%
%       % Set bandwidth cutoffs
%       params.Chip.Bandwidth.DesiredLower = 1;
%       params.Chip.Bandwidth.DesiredUpper = 3000;
%
%       board.set_configuration_parameters(params);
%
% See also set_configuration_parameters,
% rhd2000.configuration.Configuration.

    params = rhd2000.configuration.Configuration(board);
end
