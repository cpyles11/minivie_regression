function self_test( board )
%SELF_TEST Very basic self test on a board.
%
% board.self_test() runs the self test.  Currently the self test only 
% includes checking whether communication with the board is successful.
% Error if it is not successful.
%
% Example:
%      driver = rhd2000.Driver;
%      board = driver.create_board();
%      board.self_test();

retVal = calllib('RHD2000m', 'SelfTest', board.handle);

rhd2000.report_error(retVal);

end

