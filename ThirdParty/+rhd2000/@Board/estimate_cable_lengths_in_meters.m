function estimates = estimate_cable_lengths_in_meters(board)
%ESTIMATE_CABLE_LENGTHS_IN_METERS Converts between delay units and meters.
%
% Calculates, based on the current sampling rate, the conversion between 
% delay units and meters, for using in setting cable delays 
% (see rhd2000.configuration.CableDelay for background).
%
% When setting cable delays, you have the option of specifying the delays 
% in clock steps (the ManualDelay parameter). Clock steps are the minimum 
% granularity that the RHD2000 Evaluation Board can control, so changing a 
% value by 1 is the minimum effective change. ManualDelay must be between 
% 0 and 15 (inclusive).
%
% The estimate_cable_lengths_in_meters function calculates the meaning of 
% the different values of ManualDelay, i.e., what they would correspond to 
% in meters.
%
% Example:
% > board.estimate_cable_lengths_in_meters()
% ans =
%         0
%         0
%         0
%    1.3905
%    2.8761
%    4.3617
%    5.8472
%    7.3328
%    8.8184
%   10.3040
%   11.7896
%   13.2751
%   14.7607
%   16.2463
%   17.7319
%   19.2174
%
% That means that a ManualDelay value of 5 corresponds to a cable length 
% of 4.3617 meters, for example.
%
% See also configuration.CableDelay, Board, SamplingRate.

estimates = zeros(16, 1);
[retVal, estimates] = calllib('RHD2000m', ...
                              'EstimateCableLengthsInMeters', ...
                              board.handle, estimates);

rhd2000.report_error(retVal);

end

