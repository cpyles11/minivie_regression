function board = rescan(board)
%RESCAN Rescans chip types (e.g., RHD2132) attached to board.
%
% board.rescan() rescans the ports and regenerates the list
% of chips attached to the board.  It sets board.Chips to that list.
%
% Details:
% This function scans SPI Ports A-D to find all connected RHD2000 series 
% chips. It reads the chip ID from on-chip ROM to determine the chip type 
% on each port. 
%
% This process is repeated at all possible MISO delays in the FPGA, and
% the cable length on each port is inferred from this. These optimal cable 
% lengths are stored.  (See rhd2000.configuration.CableDelay.)
%
% Note: The scans are done at 30 kHz, for maximal temporal resolution in 
% cable delays.  
%
% See also Chips, rhd2000.configuration.CableDelay.

board = board.get_chip_types(true);

end

