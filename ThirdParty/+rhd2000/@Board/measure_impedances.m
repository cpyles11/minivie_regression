function [impedances, actual_frequency] = measure_impedances( board, frequency )
%MEASURE_IMPEDANCES Measures electrode impedances
%
% [impedances, actual_frequency] = board.measure_impedances(desired_frequency) 
% measures the impedances of all electrodes at a frequency close to the
% desired_frequency frequency and returns the measured impedances in an 
% array, plus the actual frequency at which the measurement took place.
%
% Note that this measurement may take a long time, especially for low
% frequencies.  The LEDs on the RHD2000 Evaluation Board will indicate that
% the measurement is occurring.
%
% Tip: The impedance frequency should be between lower bandwidth x 1.5 and 
% upper bandwidth / 1.5. (Lower bandwidth may be set by either the analog 
% filters on the RHD2000 series chip or the DSP.) The impedance period 
% (1/frequency) must be at least 4 samples and at most 1024 samples. 
% This function will produce an error if desired frequency is not within 
% those bounds. The returned actual_frequency value is the closest
% frequency that was achievable with the current settings.
% 
% The returned 'impedances' array contains one entry for each attached
% chip ordered by datasource (i.e., 'Port A, MISO 1' through 'Port D, MISO
% 2').  Each non-null entry is an array of impedances, stored as complex
% numbers.
%
% Example (with an RHD2164 on Port A, MISO 1, and an RHD2132 on Port B,
% MISO 1:
%       driver = rhd2000.Driver;
%       board = driver.create_board();
%       [impedances, actual_frequency] = board.measure_impedances(1000)
%
% impedances = 
%    [64x1 double]
%    []
%    [32x1 double]
%    []
%    []
%    []
%    []
%    []
%
% actual_frequency =
%    1000
%
% Note that the returned impedances (e.g., impedances{1}) are arrays of
% complex numbers.  
% To get the magnitudes in ohms, use the abs command.  For example:
%       abs(impedances{1})
% To get the phase in degrees, use the angle command.  For example:
%       angle(impedances{1})*180/pi
%
% See <a href="matlab:web(fullfile(fileparts(fileparts(which('rhd2000.Board'))),'Documentation','html','MeasuringImpedance.html'))">Measuring Impedance</a>
%
% See also abs, angle.

% Set impedance frequency
retVal = calllib('RHD2000m', 'SetImpedanceFreq', board.handle, frequency);
rhd2000.report_error(retVal);

% Get actual_frequency
[retVal, ~, actual_frequency] = ...
    calllib('RHD2000m', 'GetImpedanceFreq', board.handle, 0, 0);
rhd2000.report_error(retVal);

% Measure impedances for all electrodes - takes time
retVal = calllib('RHD2000m', 'MeasureAllImpedances', board.handle);
rhd2000.report_error(retVal);

% Now read the values back from the API
impedances = cell(8, 1);
for datasource = 1:8
    chip = board.Chips(datasource);
    if (chip ~= rhd2000.Chip.none)
        impedance_array = zeros(chip.num_channels, 1);
        for channel = 1:chip.num_channels
            [retVal, magnitude, phase] = ...
                calllib('RHD2000m', 'GetImpedance', ...
                        board.handle, datasource-1, channel-1, 0, 0);
            rhd2000.report_error(retVal);

            impedance_array(channel) = magnitude * exp(1i * phase/180*pi);
        end
       
        impedances{datasource} = impedance_array;
    end
end

end
