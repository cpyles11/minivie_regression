% rhd2000 is the main package of the RHD2000 Matlab Toolbox.
%
% For more information on the toolbox, see <a href="matlab:web(fullfile(fileparts(fileparts(which('rhd2000.Driver'))),'Documentation','html','Driver.html'))">RHD2000 Matlab Toolbox</a>
% 
% This directory contains:
%   * Two important classes:
%      Driver          - Class to wrap the RHD2000 Matlab Toolbox.
%      Board           - A single rhd2000 Evaluation Board.
%
%   * Several enums used by various classes in the toolbox:
%      Chip            - Enumeration of rhd2000-series chips
%      Port            - Enumeration of the four ports on the board (A-D)
%      SamplingRate    - Enumeration of the allowed sampling rates.
%
%   * Some classes and functions used internally:
%      SetGet_light    - Wrapper of matlab.mixin.SetGet with Hidden methods
%      handle_light    - Wrapper of handle with Hidden methods
%      report_error    - Errors if error_code is non-zero.
%
% In general, the 'doc' documentation is better than the 'help'
% documentation for classes in the toolbox.
%
% To get started, see <a href="matlab:doc rhd2000.Driver">doc rhd2000.Driver</a>.
