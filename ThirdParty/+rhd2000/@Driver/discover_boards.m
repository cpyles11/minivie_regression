function [ list_of_boards ] = discover_boards(~)
%DISCOVER_BOARDS Lists connected RHD2000 Evaluation Boards.
%
% list_of_boards = driver.discover_boards();
% returns the serial numbers of all RHD2000 Evaluation Boards attached 
% to the computer.
%
% The serial numbers can be used in create_board, when opening a specific
% board by serial number.
% 
% Caution: RHD2000 Evaluation Boards that are open with another program,
% are not included in this list.
%
% Example:
%      driver = rhd2000.Driver;
%      list_of_boards = driver.discover_boards();
%      board = driver.create_board(list_of_boards(1));
%
% See also create_board.

[retVal, numChars] = calllib('RHD2000m', 'DiscoverBoards1', 0);
rhd2000.report_error(retVal);

tmp=blanks(numChars);
[retVal, single_board_string] = calllib('RHD2000m', 'DiscoverBoards2', ...
                                        tmp, length(tmp));
rhd2000.report_error(retVal);

% Now split at newlines (and the string ends in a newline, so ignore that)
list = regexp(single_board_string, '\s+', 'split');
list_of_boards = list(1:end-1);

end

