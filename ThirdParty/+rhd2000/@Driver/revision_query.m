function revision = revision_query(~)
%REVISION_QUERY Revision of the RHD2000 Matlab Toolbox.
%
% revision = driver.revision_query()
% returns a structure with major, minor, and submajor version numbers.
%
% A revision is sometimes reported as major.minor.submajor, for example
% the 1.2.3 revision has major = 1, minor = 2, submajor = 3.
%
% It may be useful to check the revision if your Matlab code depends on
% having a specific version or higher of the toolbox.
%
% Example:
%     driver = rhd2000.Driver;
%     revision = driver.revision_query();

[retVal, revision.major, revision.minor, revision.submajor] = ...
    calllib('RHD2000m', 'RevisionQuery', 0, 0, 0);

rhd2000.report_error(retVal);

end

