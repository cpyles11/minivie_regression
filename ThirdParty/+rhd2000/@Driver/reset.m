function reset(~)
%RESET Resets the driver and any and all boards.
%
% Example:
%      driver = rhd2000.Driver;
%      driver.reset();

% Ignore return value, because it's a cleanup function
calllib('RHD2000m', 'Reset');

end

