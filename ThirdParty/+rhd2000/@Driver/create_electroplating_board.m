function [ board ] = create_electroplating_board( varargin )
%CREATE_ELECTROPLATING_BOARD Establishes communication with an RHD2000 electroplating board.
%
% board = driver.create_electroplating_board() talks to the default board
%
% board = driver.create_electroplating_board('ABC123') talks to the board with serial 
% number ABC123.  Serial numbers can be found by discover_boards()
%
% Example:
%     driver = rhd2000.Driver;
%     electroplating_board = driver.create_electroplating_board();
%
% See also rhd2000.Board, discover_boards.

board_tmp = rhd2000.Board(varargin);

if board_tmp.EvalBoardMode ~= 2
    error('Incorrect board type connected.');
end

board = board_tmp;

end

