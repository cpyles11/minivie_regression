classdef Driver < rhd2000.handle_light
%DRIVER Class to wrap the RHD2000 Matlab Driver.
%
% The RHD2000 Matlab toolbox consists of a DLL written in C++ and several
% .m files to access that DLL.  This class loads the C++ DLL into
% memory, if it isn't already.  When the last reference to the class goes
% out of scope, it unloads the library.
%
% Note that boards created via the create_board method contain an internal
% reference to the driver, so you can clear the driver variable once you've
% created a board.  In that case, the library will be unloaded when the 
% board goes out of scope.
%
% Example:
%      driver = rhd2000.Driver;
%      board = driver.create_board();
%      ... % other operations
%      clear board driver
%
% For more information, see
%      <a href="matlab:doc rhd2000.Driver">doc rhd2000.Driver</a>
%      <a href="matlab:web(fullfile(fileparts(fileparts(which('rhd2000.Driver'))),'Documentation','html','Driver.html'))">RHD2000 Matlab Toolbox</a>

    properties
    end
    
    methods
        function obj = Driver()
        %DRIVER Creates a driver object and loads the C++ DLL.
            if not(libisloaded('RHD2000m'))
                if strcmp(computer('arch'), 'win64')
                    dll = '+rhd2000\RHD2000m64.dll';
                else
                    dll = '+rhd2000\RHD2000m32.dll';
                end
                loadlibrary(dll, '+rhd2000\RHD2000Toolbox.h', 'alias', 'RHD2000m');
            end
        end
        
        function delete(~)
        %DELETE Unloads the C++ DLL.
            if libisloaded('RHD2000m')
                unloadlibrary('RHD2000m');
            end
        end

        % methods defined in other files
        board_list = discover_boards(driver)
        board = create_board(driver, serial_number)
        board = create_electroplating_board(driver, serial_number)
        revision = revision_query(driver)
        reset(driver)
    end
    
end

