
%% Step 0: Setup VIE software
MiniVIE.configurePath();

%% Step 1: Setup Input Device
emgSignalSource = Inputs.IntanInput.getInstance;

%% Step 2: Add input filters
% NOTE: IF THESE ARE CHANGED ALSO NEED TO CHANGE FILTER SETTINGS IN
% Utilities/feature_extract_slide.m 
% TODO: Align this so they use the same filters
lowPassFilter = Inputs.LowPass(450,8,1000);
highPassFilter = Inputs.HighPass(15,8,1000);
emgSignalSource.addfilter(lowPassFilter);  % set filter with custom parameters
emgSignalSource.addfilter(highPassFilter);

%Set the number of samples to return when getting data:
emgSignalSource.Verbose = 0;
emgSignalSource.NumSamples = 2000;
windowTimeSize = 0.2; % 200 ms

emgSignalSource.initialize();
emgSignalSource.start();

%% Step 3: Setup Signal Viewer

hSignalViewer = GUIs.guiSignalViewer(emgSignalSource);
hSignalViewer.SelectedChannels = [1,4,6];
hSignalViewer.ShowFilteredData = 1;
hSignalViewer.NumTimeDomainSamples = 2000;
hSignalViewer.NumFeatureSamples = 250;
hSignalViewer.NumFrequencySamples = 400;
hSignalViewer.FeatureWindowSize = 1000*windowTimeSize;
hSignalViewer.ZcThreshold=0.15
hSignalViewer.SscThreshold=0.15

